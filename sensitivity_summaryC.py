#!/usr/bin/env python

import os
import pickle
import numpy as np

from sensitivityTestC import NREPS, NVARS, PARAM_RANGES
print("Number of repetitions:, ", NREPS)
print("Number of variables assessed: ", NVARS)
print(PARAM_RANGES)


def createSummaryArray(resultData):
    """
    Create a structured array with the right size/columns
    for params and outputs/reps
    """
    summaryDtype = []
    # first, parameters that were varied
    for varIdx in range(NVARS):
        varName, subIdx, minVal, maxVal = PARAM_RANGES[varIdx]
        if subIdx is None:
            # single value - assume float
            summaryDtype.append((varName, np.float64))
        else:
            # tuple - assume float
            varName = varName + '[' + str(subIdx) + ']'
            summaryDtype.append((varName, np.float64))

    # next the output ones
    areaList = None
    maxAreaName = 0
    nAreas = 0
    for output in resultData:

        summaryDtype.append((output, np.float64))
        
        if areaList is None:
            # while we are here build a list of the areas
            # - assume all the same for each result
            areaList = []
            for areaName in resultData[output]:
                areaList.append(areaName)
        
                # find the number of areas, and the longest areaName
                # assume all the measures have the same keys
                if len(areaName) >  maxAreaName:
                    maxAreaName = len(areaName)

                nAreas += 1

    # areaName
    summaryDtype.append(('areaName', 'U%d' % maxAreaName))
    #print(summaryDtype)
    
    # now create the array
    summaryArray = np.zeros(NREPS * nAreas, dtype=summaryDtype)
    
    return summaryArray, areaList

def writeCSV(summaryArray, outputDataPath):

    names = []
    fmt = []
    for name in summaryArray.dtype.fields:
        names.append(name)
        
        dtype, offset = summaryArray.dtype.fields[name]
        if dtype.type is np.float64:
            fmt.append('%6f')
        else:
            # string
            fmt.append('%s')
        
    header = ','.join(names)
    summaryFName = os.path.join(outputDataPath, 'summary.csv')
    np.savetxt(summaryFName, summaryArray, fmt=fmt, header=header, delimiter=',',
            comments='')

def doSummary():
    inputDataPath = os.path.join(os.getenv('KIWIPROJDIR', default='.'), 'kiwi_data')
    outputDataPath = os.path.join(os.getenv('KIWIPROJDIR', default='.'), 'SensitivityResultsC')

    preProcessingPickleName = os.path.join(inputDataPath, 'kiwipreprocessing.pkl')
    landscapePickleName = os.path.join(inputDataPath, 'landscape.pkl')
   
    fileobj = open(preProcessingPickleName, 'rb')
    preProcessingData = pickle.load(fileobj)
    fileobj.close()

    fileobj = open(landscapePickleName, 'rb')
    landscapeData = pickle.load(fileobj)
    fileobj.close()
    
    # don't know size etc until we get first rep
    summaryArray = None
    areaList = None
 
    idx = 0
    for rep in range(NREPS):
#        paramsPickleName = os.path.join(inputDataPath, 'kiwiparams_%02d.pkl' % rep)
        paramsPickleName = os.path.join(outputDataPath, 'kiwiparams_%02d.pkl' % rep)
        resultName = os.path.join(outputDataPath, 'kiwiresults_%02d.pkl' % rep)
        
        fileobj = open(paramsPickleName, 'rb')
        paramsData = pickle.load(fileobj)
        fileobj.close()
        
        fileobj = open(resultName, 'rb')
        resultData = pickle.load(fileobj)
        fileobj.close()
        
        print(rep)
        
        if summaryArray is None:
            summaryArray, areaList = createSummaryArray(resultData)
    
        # now the output values
        for areaName in areaList:
            for output in resultData:
                #print(output, areaName, resultData[output][areaName])
                summaryArray[idx][output] = resultData[output][areaName]

                # parameters - same for this whole rep so there might be a faster way to do this
                for varIdx in range(NVARS):
                    varName, subIdx, minVal, maxVal = PARAM_RANGES[varIdx]
                    if subIdx is None:
                        # single value 
                        summaryArray[idx][varName] = getattr(paramsData, varName)
                    else:
                        # tuple
                        tup = getattr(paramsData, varName)
                        varName = varName + '[' + str(subIdx) + ']'
                        summaryArray[idx][varName] = tup[subIdx]
                    
                # areaname
                summaryArray[idx]['areaName'] = areaName

            idx += 1
        
        #nAreas = len(areaList)
        #print(summaryArray[rep * nAreas:(rep+1) * nAreas])

    writeCSV(summaryArray, outputDataPath)
        
#        print('meanPropMast', resultData['meanPropMast'])
#        print('meanPropControlled', resultData['meanPropControlled'])
#        print('meanStoatDensities', resultData['meanStoatDensities'])
#        print('zeroStoatProportions', resultData['zeroStoatProportions'])
#        print('meanRodentDensities', resultData['meanRodentDensities'])
        #print('kiwiGrowthRatios', resultData['kiwiGrowthRatios'])

        #for areaName in resultData['kiwiGrowthRatios']:
        #    print(areaName, resultData['kiwiGrowthRatios'][areaName])
        
#        print('elevationRefuge', landscapeData['elevationRefuge'])
#        print('Beech', landscapeData['Beech'])
#        print('CoastalBeech', landscapeData['CoastalBeech'])
       
    
if __name__ == '__main__':
    doSummary()
