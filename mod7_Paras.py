#!/usr/bin/env python

import os
import numpy as np
from kiwimodel import params
from kiwimodel import preProcessing
from kiwimodel import diagnosticSim

pars = params.KiwiParams()

# set paths to scripts and data
pars.inputDataPath = os.path.join(os.getenv('KIWIPROJDIR', default='.'), 'kiwi_data')
pars.outputDataPath = os.path.join(os.getenv('KIWIPROJDIR', default='.'), 'KiwiProjResults', 'mod7_Results')
if not os.path.isdir(pars.outputDataPath):
    os.mkdir(pars.outputDataPath)

### SET DATA AND PATHS TO DIRECTORIES
pars.setExtentShapeFile(os.path.join(pars.inputDataPath, 'FiordlandConArea.shp'))
pars.setKClasses(os.path.join(pars.inputDataPath, 'rodentK.img'))
### Area trapped in recent times.
pars.setIslands(os.path.join(pars.inputDataPath, 'islandSecRes.tif'))
pars.setDEM(os.path.join(pars.inputDataPath, 'dem.tif'))
pars.setResolutions((200.0, 1000.0, 1000.0))
pars.setControlFile(os.path.join(pars.inputDataPath, 'control1.csv'))
pars.setControlPathPrefix(pars.inputDataPath)

### SET YEARS AND BURN IN YEARS
pars.setBurnin(50)
pars.setYears(np.arange(20))
### SET ITERATIONS
pars.setIterations(200)
print('iterations:', pars.iter)

# Control parameters
# proportion of zone in mast required for reactive control
#pars.setReactiveMode(0.33)

### Masting parameters
pars.setMastRho(16000.0)
#pars.setMastWindowSize(130)
pars.setMastCellParams(0.001, 1000.0)
#pars.setMastProportionParams(0.4, 0.2)
pars.setMastSpatialSD(2.1)
pars.setMastPrEvent(1.0 / 5.3)          #5.3)

## rodent parameters
pars.setPRodentPresence(0.95)
pars.setRodentInitialMultiplier(.80)
pars.setRodentProbEatBait(0.8)
pars.setRodentGrowthRate(1.2)

pars.setPrpGrowRateControl(0.2)


## rodent tracking tunnel parameters
#pars.setThreshold_TT(0.20)
#pars.setG0_TT(0.02)
#pars.setSigma_TT(22.0)
#pars.setNights_TT(4)
#pars.setNTunnels(120)



## stoat parameters
pars.setPStoatPresence(0.75)
pars.setStoatInitialMultiplier(.85)
pars.setIslandK(1.3)
pars.setStoatGrowthRate(1.2)
pars.setStoatKAsymptotes(1.0, 8.0)
pars.setPEncToxic(0.006)  #.004        # operates at stoat scale
pars.setPEatEncToxic(0.9)
pars.setStoatPopSD(0.22)

### Kiwi parameters
pars.setKiwiK(20)
pars.setPKiwiPresence(0.68)
pars.setKiwiInitialMultiplier(0.15)
pars.setKiwiPsi(0.03)   #.0075
pars.setMinPredationSurv(0.85)
pars.setCompetitionEffect(0.000)    #0.004    
pars.setKiwiGrowthRate(.10)
pars.setKiwiPopSD(.12)

data = preProcessing.KiwiData(pars)
data.pickleSelf(os.path.join(pars.outputDataPath, 'preProcData.pkl'))



## DIAGNOSTIC SIMULATION; plot and save graph
#simDiag = diagnosticSim.OnePixelSim(pars)



