#!/usr/bin/env python

import os
import numpy as np
from scipy import stats
import pylab as P
#import starlings
#import params
from scipy.special import gamma
from scipy.special import gammaln
#from starlings import dwrpcauchy
from scipy.stats.mstats import mquantiles
from pyDOE import lhs
from numba import jit
import datetime
from scipy.stats import gengamma


def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D


def getBetaFX(mg0, g0Sd):
    a = mg0 * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
    b = (1.0 - mg0) * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
    return (a, b)


def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))

def reScale(lowEnd, hiEnd, dat):
    minx = np.min(dat)
    maxx = np.max(dat)
    numerator = (hiEnd - lowEnd) * (dat - minx)
    denominator = maxx - minx
    newx = (numerator / denominator) + lowEnd
    return newx


def gamma_pdf(xx, shape, scale):
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))
    return gampdf

def dwrpcauchy(th, mu, rho):
    """
    wrapped cauchy pdf: direction is mu, and focus is rho.
    mu is real, and rho > 0
    Wikipedia pdf equation
    """
#    e_num = np.exp(-2*rho)
#    e_denom = 2 * np.exp(-rho)
#    sinh_rho = (1 - e_num) / e_denom
#    cosh_rho = (1 + e_num) / e_denom
    sinh_rho = np.sinh(rho)
    cosh_rho = np.cosh(rho)
    cos_mu_th = np.cos(th - mu)
    dwrpc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
    return dwrpc

def genGammPDF(x, a, d, p):
    ## FROM WIKIPEDIA
    num = p / a**d
    dem = gamma(d/p)
    num2 = (x**(d-1)) * (np.exp(-(x / a)**p))
    ggPDF = num * num2 / dem
    return(ggPDF)

class G0Test(object):
    def __init__(self):

        self.gg = np.arange(.01,.99, .001)
#        self.dpdf = stats.beta.pdf(self.gg, self.params.g0_alpha, self.params.g0_beta)
#        P.plot(self.gg, self.dpdf)
#        P.show()
#        P.cla()
        (a, b) = getBetaFX(0.09, .04)

#        mode = .75
#        a = 3


        ##HH
#        a = 1   #2.5             # 1.4 # 1.1
#        b = 1   #2.5             # 84.38889
        ##cats
#        a = 0.92
#        b = 22.1

#        b = ((a-1.0)/mode) - a + 2.0


        print("a and b", a, b)
        print('mean beta', (a/(a+b)), 'sd =', np.sqrt(a*b / (a+b)**2 / (a+b+1)))
        betaMode = (a-1) / (b + a - 2)
        print('mode', betaMode) 
#        randbeta = np.random.beta(a,b,100000)
#        print('nabove', len(randbeta[randbeta>.04]), 'nbelow', len(randbeta[randbeta < .04]))
        self.dpdf = stats.beta.pdf(self.gg, a, b)   #3.9, 191.)  # 1.0, 15.667)         # .095, 37.05)
        P.plot(self.gg, self.dpdf)
#        P.ylim(0., 15.0)
        P.xlim(0.001, .999)        
        P.xlabel("Prior prob. of eradication")
        P.ylabel("Confidence (density)")
#        P.hist(randbeta)
        P.savefig('veryLowPrior.png', format='png')
        P.show()
#        P.cla()

def g0SigmaTest():
    g0 = .1
    sig = 180
    n = 75000
    ## sig test
    dx = np.random.normal(0, sig, n)
    dy = np.random.normal(0, sig, n)
    dist = np.sqrt(dx**2 + dy**2)
    quants = mquantiles(dist, prob=[0.95, 0.99, 99999])
    print('quants', quants, 'dx quants', mquantiles(dx, prob=[0.95, 0.975, 0.99]))
    print('quotient quants / sig', quants / sig)
    print('area quants', (np.pi * quants**2 / 10000))

    ## start with area in hr area in ha
    hr = 50 #ha
    m2 = hr * 10000
    rad = np.sqrt(m2 / np.pi)
    print('sig95', rad/2.45, 'sig99', rad/3.05)

    g0 = .13
    sig = 90.0
    penc = [.96, .83, .57]
    pint = [.55, .30, .50]
    d = 20.0
    pc0 = g0 * np.exp(-(d**2 / 2.0/ sig**2))
    pc1_3 = (1 - (1-penc[0]*pint[0]) * 
                (1-penc[1]*pint[1]) *
                (1-penc[2]*pint[2]))
    ## NO BIAS USUAL CALC
    pcapt = 1 - (1 - pc0)**4
    print('PoA calc without bias = ', pcapt)

    ## ACCOUNT FOR DEPENDENCE
    pcaptBias = 1 - (1 - pc0) * (1 - pc1_3)
    print('pcapt with bias = ', pcaptBias)




class BetaPlot(object):
    def __init__(self):

        self.gg = np.arange(.001,.99, .001)
        P.figure(figsize=(11,9))
        P.subplot(2,2,1)
        mode = .008
        a = 1.2 # 1.4 # 1.1
        b = ((a-1.0)/mode) - a + 2.0
        dpdf = stats.beta.pdf(self.gg, a, b)   #3.9, 191.)  # 1.0, 15.667)         # .095, 37.05)
        P.plot(self.gg, dpdf, color='k', linewidth = 3)
        P.xlim(0.001, .99)        
        P.ylabel("Relative confidence (density)")
        P.title("Very low Prior")
        P.subplot(2,2,2)
        mode = .2
        a = 3.5 # 1.4 # 1.1
        b = ((a-1.0)/mode) - a + 2.0
        dpdf = stats.beta.pdf(self.gg, a, b)   #3.9, 191.)  # 1.0, 15.667)         # .095, 37.05)
        P.plot(self.gg, dpdf, color='k', linewidth = 3)
        P.xlim(0.001, .99)        
        P.title("Low Prior")
        P.subplot(2,2,3)
        mode = .5
        a = 4. # 1.4 # 1.1
        b = ((a-1.0)/mode) - a + 2.0
        dpdf = stats.beta.pdf(self.gg, a, b)   #3.9, 191.)  # 1.0, 15.667)         # .095, 37.05)
        P.plot(self.gg, dpdf, color='k', linewidth = 3)
        P.xlim(0.001, .99)        
        P.ylabel("Relative confidence (density)")
        P.xlabel("Prior Probability of eradication.")
        P.title("Coin-toss prior")
        P.subplot(2,2,4)
        a = 1.0
        b = 1.0
        dpdf = stats.beta.pdf(self.gg, a, b)   #3.9, 191.)  # 1.0, 15.667)         # .095, 37.05)
        P.plot(self.gg, dpdf, color='k', linewidth = 3)
        P.xlim(0.001, .99)        
        P.xlabel("Prior Probability of eradication")
        P.title("No idea prior")
        P.savefig('nutriaPrior.png', format='png')
        P.show()
#        P.cla()


def findBetaPert(min, mode, max, shape=4):
    """
    Find a and b of a Beta distribution given the information for a Pert distribution 
    (min, mode, max)
    see http://rgm2.lab.nig.ac.jp/RGM2/func.php?rd_id=mc2d:pert 
    """
    vals = np.arange(.01, .99, .01)
    mu = (min + max + shape * mode)/(shape + 2.0);
    if mu == mode:
        shape1 = 1.0 + shape / 2.0
    else:
        shape1 = (mu - min) * (2.0 * mode - min - max)/((mode-mu)*(max - min))
    shape2 = shape1 * (max - mu)/(mu - min)
    betapdf = stats.beta.pdf(vals, shape1, shape2)
    P.figure()
    P.plot(vals, betapdf)
    P.show()
    rangeBP = max - min
    randBeta = (np.random.beta(shape1, shape2, 1000) * rangeBP) + min
     
    print('shp1, shp2', shape1, shape2, 'min, mean, max', np.min(randBeta),
        np.mean(randBeta), np.max(randBeta))



def betaBinomFX():
    mn = 0.09
    sd = 0.08
    th = np.linspace(0.001, .999, 2000)
    ## PRIORS
    (a, b) = getBetaFX(mn, sd)
#    a = 1
#    b = 1

    priorPDF = stats.beta.pdf(th, a, b)
    P.figure(figsize =(11,9))
#    ax0 = P.gca()
    P.subplot(2,2,1)
    P.plot(th, priorPDF, color = 'k', linestyle = 'dashed', 
        linewidth = 4, label = 'Prior')
    P.axvline(x = 0.95, ls = '--', color = 'k')
    P.legend(loc = 'upper right')
    P.xlabel('Probability of absence', fontsize = 14)
    P.ylabel('Density', fontsize = 14)
#    lns0 = ax0.plot(th, priorPDF, color = 'k', linestyle = 'dashed', 
#        linewidth = 2, label = 'Prior')
#    lns = lns0.copy()
#    labs = [l.get_label() for l in lns]
#    ax0.legend(lns, labs, loc = 'upper right')
#    ax0.set_xlabel('Probability of absence', fontsize = 14)
#    ax0.set_ylabel('Density', fontsize = 14)
    yMax = np.max(priorPDF) + 1.0
#    yMax = 24.0

    P.ylim(-0.5, yMax)

    P.subplot(2,2,2)
##    ax1 = P.gca()
    priorPDF2 = stats.beta.pdf(th, a, b)
    N = 5
    y = 5
    postAlpha = a + y
    postBeta = N - y + b
    postPDF2 = stats.beta.pdf(th, postAlpha, postBeta)
    P.plot(th, priorPDF2, color = 'k', linestyle = 'dashed', 
        linewidth = 4, label = 'Prior')
    P.plot(th, postPDF2, color = 'r', linewidth = 4, label = 'Posterior')
    P.axvline(x = 0.95, ls = '--', color = 'k')
    P.legend(loc = 'upper right')
#    lns = lns0 + lns1
#    labs = [l.get_label() for l in lns]
#    ax1.legend(lns, labs, loc = 'upper right')
    P.xlabel('Probability of absence', fontsize = 14)
    P.ylabel('Density', fontsize = 14)
    P.ylim(-0.5, yMax)


    
#    ax2 = P.gca()
    P.subplot(2,2,3)
    priorPDF3 = postPDF2.copy()
    a = postAlpha
    b = postBeta
    N = 15
    y = 15
    postAlpha = a + y
    postBeta = N - y + b
    postPDF3 = stats.beta.pdf(th, postAlpha, postBeta)
    P.plot(th, priorPDF3, color = 'r', linestyle = 'dashed', 
        linewidth = 4, label = 'Prior')
    P.plot(th, postPDF3, color = 'b', linewidth = 4, label = 'Posterior')
    P.axvline(x = 0.95, ls = '--', color = 'k')
#    lns = lns2 + lns3
#    labs = [l.get_label() for l in lns]
#    ax2.legend(lns, labs, loc = 'upper right')
    P.legend(loc = 'upper left')
    P.xlabel('Probability of absence', fontsize = 14)
    P.ylabel('Density', fontsize = 14)
    P.ylim(-0.5, yMax)

#    ax3 = P.gca()
    P.subplot(2,2,4)
    priorPDF4 = postPDF3.copy()
    a = postAlpha
    b = postBeta
    N = 150
    y = 150
    postAlpha = a + y
    postBeta = N - y + b
    postPDF4 = stats.beta.pdf(th, postAlpha, postBeta)
    P.plot(th, priorPDF4, color = 'b', linestyle = 'dashed', 
        linewidth = 4, label = 'Prior')
    P.plot(th, postPDF4, color = 'y', linewidth = 4, label = 'Posterior')
    P.axvline(x = 0.95, ls = '--', color = 'k')
#    lns = lns4 + lns5
#    labs = [l.get_label() for l in lns]
#    ax3.legend(lns, labs, loc = 'upper right')
    P.legend(loc = 'upper left')

    yMax = 24.0
    P.ylim(-0.5, yMax)
    P.xlabel('Probability of absence', fontsize = 14)
    P.ylabel('Density', fontsize = 14)
    print('max post', np.max(postPDF4), 'mean', (postAlpha / (postAlpha + postBeta)))
    print('yMax', yMax)
    
    P.tight_layout()
    plotFname = 'betaBinPriorPost.png'
    P.savefig(plotFname, format='png', dpi=300)
    P.show()

def pIntro():
    nWk = 20
    weeks = np.arange(nWk, dtype = int)
    SSe = 0.25
    prior = np.repeat(0.94, nWk)
    pIntro = 0.03
    posterior = np.zeros(nWk)
    priorPosterior = []
    ppWk = []
    for i in range(nWk):
        posterior[i] = prior[i] / (1.0 - SSe*(1.0 - prior[i]))
        priorPosterior.append([prior[i], posterior[i]])
        ppWk.append([i, i])
        if i < (nWk - 1):
            prior[i + 1] = posterior[i] * (1.0 - pIntro)
    priorPosterior = np.concatenate(priorPosterior)
    ppWk = np.concatenate(ppWk)
    P.figure(figsize=(14,7))
    P.subplot(1,2,1)
    P.plot(weeks, posterior, color = 'k', linewidth = 4, label = 'Posterior')
    P.axhline(y = 0.95, color = 'k', linestyle = 'dashed')
    P.plot(weeks, prior, color = 'red', linewidth = 3, label = 'Prior')
    P.plot(ppWk, priorPosterior, color = 'b', label = 'Updating PoA')
    P.ylim(0.85, 1.0)
    P.ylabel('Probability of absence', fontsize = 14)
    P.xlabel('Weeks', fontsize = 14)
    P.legend(loc = 'upper left')

    P.subplot(1,2,2)
    SSe = 0.39
    prior = np.repeat(0.94, nWk)
    posterior = np.zeros(nWk)
    priorPosterior = []
    ppWk = []
    for i in range(nWk):
        posterior[i] = prior[i] / (1.0 - SSe*(1.0 - prior[i]))
        priorPosterior.append([prior[i], posterior[i]])
        ppWk.append([i, i])
        if i < (nWk - 1):
            prior[i + 1] = posterior[i] * (1.0 - pIntro)
    priorPosterior = np.concatenate(priorPosterior)
    ppWk = np.concatenate(ppWk)
    P.plot(weeks, posterior, color = 'k', linewidth = 4, label = 'Posterior')
    P.axhline(y = 0.95, color = 'k', linestyle = 'dashed')
    P.plot(weeks, prior, color = 'red', linewidth = 3, label = 'Prior')
    P.plot(ppWk, priorPosterior, color = 'b', label = 'Updating PoA')
    P.ylim(0.85, 1.0)
    P.ylabel('Probability of absence', fontsize = 14)
    P.xlabel('Weeks', fontsize = 14)
    P.legend(loc = 'upper left')
    P.tight_layout()
    plotFname = 'pIntroUpdating.png'
    P.savefig(plotFname, format='png', dpi=300)


    P.show()

class gammaTest(object):
    def __init__(self):

#        self.params = params
#        print(self.params.g0_alpha)
#        self.rr = np.arange(.005, 1.5, .001)
        self.rr = np.arange(1, 26)
        mode = 9
#        sh = 9.3333    # 4.33 # 3.0    # 2.8        # 2.8750    # .001
        sc = 2.0    #.8 # .2 #1.0   # 0.8    # 1000.    #.1    # 1.0/ 5.4  #2.25   #1.6667  #3.    
        sh = (mode / sc) + 1.0
        print('sh =', sh, 'sc =', sc)
#        print('mode', (sh - 1.0) * sc)
#        sc = 3.0     # 1.5     # .66667    
#        rate = 1.0/sc    #0.5  #40.0     #1.1      #.5 #4  #1.      #2
        self.dpdf = gamma_pdf(self.rr, sh, (sc))           # use shape and scale: sc = 1/rate !!!
        randg = np.random.gamma(sh, sc, size = 100000)          # use shape and scale
        print('mean randg', np.mean(randg))
        print("mean", sh*sc, "sd", np.sqrt(sh*sc**2), "mode", (sh-1.)*sc)
#        print("parameter calc mean", sh * sc)
#        print("max LL r", self.rr[self.dpdf==np.max(self.dpdf)], ((sh-1)*sc))
#        print("max pdf", self.dpdf[self.dpdf==np.max(self.dpdf)])
        
        P.figure(figsize = (11,9))
        P.subplot(2,2,1)
        P.plot(self.rr, 10.5 * self.dpdf, linewidth = 3, color = 'k')
        P.xlabel('Weeks since mast', fontsize = 14)
        P.ylabel('Mast effect on reproduction rate', fontsize = 14)

        P.subplot(2,2,2)
        sc = 4.0    #.8 # .2 #1.0   # 0.8    # 1000.    #.1    # 1.0/ 5.4  #2.25   #1.6667 
        sh = (mode / sc) + 1.0
        print('sh =', sh, 'sc =', sc)
        self.dpdf = gamma_pdf(self.rr, sh, (sc))           # use shape and scale: sc = 1/ra
        P.plot(self.rr, 15.5 * self.dpdf, linewidth = 3, color = 'k')
        P.xlabel('Weeks since mast', fontsize = 14)
        P.ylabel('Mast effect on reproduction rate', fontsize = 14)

        P.subplot(2,2,3)
        sc = 6.0    #.8 # .2 #1.0   # 0.8    # 1000.    #.1    # 1.0/ 5.4  #2.25   #1.6667 
        sh = (mode / sc) + 1.0
        print('sh =', sh, 'sc =', sc)
        self.dpdf = gamma_pdf(self.rr, sh, (sc))           # use shape and scale: sc = 1/ra
        P.plot(self.rr, 18.5 * self.dpdf, linewidth = 3, color = 'k')
        P.xlabel('Weeks since mast', fontsize = 14)
        P.ylabel('Mast effect on reproduction rate', fontsize = 14)

        P.subplot(2,2,4)
        sc = 8.0    #.8 # .2 #1.0   # 0.8    # 1000.    #.1    # 1.0/ 5.4  #2.25   #1.6667 
        sh = (mode / sc) + 1.0
        print('sh =', sh, 'sc =', sc)
        self.dpdf = gamma_pdf(self.rr, sh, (sc))           # use shape and scale: sc = 1/ra
        P.plot(self.rr, 8.0 * self.dpdf, linewidth = 3, color = 'k')
        P.xlabel('Weeks since mast', fontsize = 14)
        P.ylabel('Mast effect on reproduction rate', fontsize = 14)

#        P.ylim(0, 0.5)
        fname = os.path.join('/home/dean/workfolder/projects/jo_rats/Results/mastTime.png')
        P.savefig(fname, format='png', dpi = 100)
        P.show()
#        P.cla()
        gammaMode()

def gammaMode():
    n = 50000
    modeG = np.random.uniform(10, 18, n)
#    sh = np.random.uniform(0.1, 4.0, size = 3000)
    sc = np.random.uniform(4.0, 10, size = n)
    sh = (modeG / sc) + 1.0
    qu = mquantiles(sh, prob=[.05, .5, .95])
    print('shape rand Quants', qu)


def invGamma():
    a = np.arange(.01, 10.0, .01)
    shp = 3.0
    scl = 1 / 3.0
    rGamma = np.random.gamma(shp, scl, 5000)
    ig = 1.0/ rGamma
    print('shp', shp, 'scl', scl, 'mean', np.mean(ig), 'sd', np.std(ig))
    invGamPDF = gamma_pdf(a, shp, scl)
    P.figure()
    P.hist(ig, bins=50, range=(.01, 20.0))
#    P.plot(a, invGamPDF)
    P.show()


class wrpCTest(object):
    def __init__(self):
        day = np.arange(365)
        nDays = len(day)
        d_i = datetime.date(2021,1,1)
        dt = []
        dt.append(d_i)
        for i in range(1, nDays):
            d_i = d_i + datetime.timedelta(days = 1)
            dt.append(d_i)
        daysRad = day/np.max(day) * 2.0 * np.pi

#        aa = days/364 * 2. 
        aa = np.arange(0, (np.pi*2), .05)
        mu = np.pi    # 10/365 * 2 * np.pi    #-1.5 # 1.4    # np.pi*2
        rho = 1.0   #4.0   # .44   # 3.   #.7
#        dwrpc = (dwrpcauchy(daysRad, mu, rho) * -0.15) + .04
        dwrpc = dwrpcauchy(daysRad, mu, rho)
        st_dwrpc = (dwrpc/np.max(dwrpc)) 
#        print(dwrpc)
#        print('st_dwrpc', st_dwrpc)
        P.figure(figsize = (9,6))
        P.plot(dt, st_dwrpc, linewidth = 3, color = 'k')
        P.xlabel('Days of year', fontsize = 14)
        P.ylabel('$\omega_{ijt}$ upland', fontsize = 14)
        P.axhline(y=0, ls = '--', color = 'k')
        plotFname = 'wrapCauchy.png'
        P.savefig(plotFname, format='png', dpi = 400)
#        P.plot(aa, st_dwrpc, color = 'r')
#        P.ylim(0, 1.0)
        P.show()
        P.cla()

class normalBeta(object):
    def __init__(self):
        M = .4
        V = .01
        dd = logit(np.arange(.001, 1, .001))
        ddpdf = stats.norm.pdf(dd, logit(M), .5)
        l_mu = np.random.normal(logit(M), .05, 2000)
        mu = inv_logit(l_mu)
#        print(np.std(mu))
        P.figure(0)
        P.subplot(1,2,1)
        P.hist(mu)
        P.xlim(0,1.0)
        P.subplot(1,2,2)
        P.plot(dd, ddpdf)
#        P.ylim(0, 2.0)
#        P.show()
#        P.cla()
        

class normalFX(object):
    def __init__(self):
        M = 178
        S = np.sqrt(200)
        dd = np.arange(30, 550)
        ddpdf = stats.norm.pdf(dd, M, S)
        P.figure(0)
        P.plot(dd, ddpdf)
#        P.ylim(0, 2.0)
        P.show()

        


class Poisson(object):
    def __init__(self):
        dat = 91
        datGammaLn = gammaln(dat + 1)
        ll = 178.002847241 

        lpmf = self.ln_poissonPMF(dat, datGammaLn, ll)

        pois = stats.poisson.logpmf(dat, ll)
#        print('lpmf', lpmf, 'pois', pois)

    def ln_poissonPMF(self, dat, datGammaLn, ll):
        """
        calc log pmf for poission
        """
        ln_ppmf = ((dat * np.log(ll)) - ll) - datGammaLn
        return(ln_ppmf)



class halfNorm(object):
    def __init__(self):
        dat = np.sqrt(50)
#        dat = np.arange(400.0)
        sig = 90.0
        HNpdf = stats.halfnorm.pdf(dat,loc=0.0, scale = sig)
        
        print('hnpdf', np.sum(HNpdf))
        P.figure()
        P.plot(dat, HNpdf)
        P.show()


class multiVNorm(object):
    def __init__(self):
        dat = np.array([5., 5.])
#        dat = np.arange(400.0)
        dat = np.zeros((10, 2))
        sig = 90.0
        sigMat = np.expand_dims(sig, 1)
        ndat = len(dat)
        ndat = 6
        covMat = np.identity(ndat) * sigMat
        meanMat = np.zeros(ndat)
#        MVpdf = stats.multivariate_normal.logpdf(dat, mean = meanMat, cov = covMat)
        rMV1 = np.random.multivariate_normal(meanMat, covMat, size = 1)
        rMV2 = np.random.multivariate_normal(meanMat, covMat, size = 1)
        distmv = np.sqrt(rMV1**2 + rMV2**2)
        print('distmv', np.mean(distmv), np.std(distmv), np.max(distmv))  
        print('covMat', covMat, 'rMV1', rMV1)
#        print('mvpdf', np.sum(MVpdf))
#        P.figure()
#        P.plot(dat, MVpdf)
#        P.hist(rMV1)
#        P.show()

class TruncNormFX(object):
    def __init__(self):
        lo = -5.0
        hi = 5.0
        mu, sigma = 4.98, 0.05
        arr = np.linspace(4.90, 5.0, 1000)
        X = stats.truncnorm((lo - mu) / sigma, (hi - mu) / sigma, 
            loc=mu, scale=sigma)
        Xstar = stats.truncnorm((lo - arr) / sigma, 
            (hi - arr) / sigma, loc=arr, scale=sigma)
        diff = arr - mu
        pnew_old = X.pdf(arr)
        pold_new = Xstar.pdf(mu)
#        print('stat truncnorm', X)
        rand = X.rvs(10000)
        nabove = np.sum(rand > mu)
        nbelow = np.sum(rand <= mu)
        absDiff = np.abs(diff)
        mindiff = np.min(absDiff)
        print('nabove', nabove, 'nbelow', nbelow, 
            'IR at mu', (pnew_old/pold_new)[absDiff == mindiff])
        P.figure()
        P.plot(diff, pnew_old/pold_new)
#        ax = P.hist(rand)
        P.show()
        
        
class TruncNormIR(object):
    def __init__(self):
        n = 1000
        lo = -5.0
        hi = 5.0
        sigma = 0.05
        Y = np.linspace(3.0, 5.0, n)
        nowTNorm = stats.truncnorm((lo - Y) / sigma, (hi - Y) / sigma, 
            loc=Y, scale=sigma)
        Ynew = nowTNorm.rvs(n)
        newTNorm = stats.truncnorm((lo - Ynew) / sigma, 
            (hi - Ynew) / sigma, loc=Ynew, scale=sigma)
        pnew_old = nowTNorm.pdf(Ynew)
        pold_new = newTNorm.pdf(Y)
        IR = pnew_old / pold_new
        P.figure()
        P.plot(Y, IR)
#        ax = P.hist(rand)
        P.show()
        


class circHR(object):
    def __init__(self):
        normMean = 0.0
        normSig = 90.0
        n = 20000
        xnorm = np.random.normal(normMean, normSig, n)
        ynorm = np.random.normal(normMean, normSig, n)

        dat 
        LLxy

        normDist = np.sqrt(xnorm**2 + ynorm**2)
        normSD = np.std(normDist)
        print('normSD', normSD)
        normQuant = mquantiles(normDist, prob=[0.95, 0.99])

        print('norm 95%', normQuant, normQuant/normSig)


        distHN = stats.halfnorm.rvs(loc = 0., scale = normSig, size =n)
        HNSD = np.std(distHN)
        print('HNSD', HNSD)
        HNQuant = mquantiles(distHN, prob=[0.95, 0.99])

        print('HN 95%', HNQuant, HNQuant/normSig)


class WeibullPCapt(object):
    def __init__(self):
        g0 = 0.13
        sig = 90.0
        H = 3.0
        d = 40.
        pe = 0.9
        pIntEnc = .05

        K = np.arange(.01, 5.0, .01)
        Pcij = g0 * np.exp(-(d**2) / 2. / (sig**2))


        print('PCurrentOne', Pcij)

        P4 = 1.0 - (1.0 - Pcij)**4
        print('P4', P4)

        th = 1.0 - (1.0 - Pcij) * (np.exp(-(Pcij * H)**K))
        print('th k= 1', th[K==1.])

        thEI = 1.0 - (1.0 - Pcij) * (1.0 - (pe * pIntEnc))**H
        print('ThEI', thEI)


        P.figure()
        P.plot(K, th)
        P.xlabel('K')
        P.ylabel('Theta')
        P.axhline(y=P4, ls = '--')
        P.axhline(y=Pcij, color = 'k')
        plotFname = 'weibullCaptureDecay.png'
        P.savefig(plotFname, format='png')
        P.show()


class Rescale(object):
    def __init__(self):
        ## trial at rescaling and convert to (0,1) with inv_logit

        stoatLogitSc = [-4.0, 7.0]
        sKRange = [0.5, 8.0]
        rodentDensity = np.arange(.2, 26., .2)
        rDensityRange = [0.2, 26.0]

        ## Run two functions
        self.rescaleProportion(rodentDensity, stoatLogitSc, sKRange)

        self.getStoatK(12., rDensityRange, stoatLogitSc, sKRange)
        
        # run generic function
        newdat = self.rescaleFX(rDensityRange, stoatLogitSc, 12.)
        print('Generic rescale fx', inv_logit(newdat))



    def rescaleFX(self, oldRange, newRange, dat):
        """
        generic function for rescaling data to new value in specified range
        """
        numer = (newRange[1] - newRange[0]) * (dat - oldRange[0])
        denom = oldRange[1] - oldRange[0]
        newDat = (numer / denom) + newRange[0]
        print('newDat', newDat)
        return(newDat)

        

    def rescaleProportion(self, rodentDensity, stoatLogitSc, sKRange):
        """
        rescale variable and take inv_logit
        """
        minRodent = np.min(rodentDensity)
        maxRodent = np.max(rodentDensity)
        (minLogitSc, maxLogitSc) = stoatLogitSc
        numer = (maxLogitSc - minLogitSc) * (rodentDensity - minRodent)
        denom = maxRodent - minRodent
        sLogitDat = (numer / denom) + minLogitSc
        prpStoatK = inv_logit(sLogitDat)
        stoatK = sKRange[0] + (prpStoatK * (sKRange[1] - sKRange[0]))

        rdiff = np.abs(rodentDensity - 8.0)
        rmask = (rdiff == np.min(rdiff))
        horizLine = stoatK[rmask]

        P.Figure()
        P.plot(rodentDensity, stoatK, color ='k')
        P.xlabel('Rodent density $(individuals * km^{-2})$')
        P.ylabel('Stoat K $(individuals * km^{-2})$')
        P.vlines(x = 8.0, ymin=0.0, ymax = horizLine, 
            linestyles = 'dashed', colors='k')
        P.hline(y = horizLine, xmin=0.0, xmax = 8.0, 
            linestyles = 'dashed', colors='k')
        plotFname = 'stoatK.png'
        P.savefig(plotFname, format='png', dpi=400)
        P.show()        
        return(stoatK)

    def getStoatK(self, rodentDensity, rDensityRange, stoatLogitSc, sKRange):
        """
        # get stoat K as function of rodent density.
        """
        (minRodent, maxRodent) = rDensityRange
        (minLogitSc, maxLogitSc) = stoatLogitSc
        ## 
        numer = (maxLogitSc - minLogitSc) * (rodentDensity - minRodent)
        denom = maxRodent - minRodent
        sLogitDat = (numer / denom) + minLogitSc
        prpStoatK = inv_logit(sLogitDat)
        stoatK = sKRange[0] + (prpStoatK * (sKRange[1] - sKRange[0]))
        print('stoatK[rod = 12.', stoatK[rodentDensity == 12.], 'prpStoatK', prpStoatK)
        return(stoatK)

class encounterFX(object):
    def __init__(self):
        """
        Trial at relationship between number to toxic rodents and the number
        of stoats that get poisoned
        """
        T_rodent = np.arange(20.*100.)
        pEatEnc = 0.9
        encRate = 0.004
        pEnc = 1.0 - np.exp(-encRate * T_rodent)
        pTStoat = pEnc * pEatEnc
        P.figure()
        P.plot(T_rodent, pTStoat, color = 'k')
        P.xlabel('Toxic Rodents $(T^{[R]}_{k,t})$')
        P.ylabel('$Pr(eat|enc)Pr(enc)$')
        plotFname = 'pToxicStoat.png'
        P.savefig(plotFname, format='png', dpi=400)
        P.show()
        print('max ptstoat', np.max(pTStoat), pTStoat[T_rodent == 300])
#        P.figure()
#        P.plot(nS, r)
#        P.show()

class Emigration(object):
    def __init__(self):
        """
        Explore function and parameters for emigration
        """
        KRatio = np.arange(0.01, 2.0, 0.01)
#        gammaPara = (-4.0, 3.0)
#        PrEmig = inv_logit(gammaPara[0] + gammaPara[1] * KRatio)
        PrEmig = 1.0 - np.exp(-0.60 * KRatio)  
        horizLine = PrEmig[KRatio == 1.0]
        vertLine = 1.0
        print('prob at ratio = 1', horizLine)
        P.figure(figsize=(7,6))
        P.plot(KRatio, PrEmig, color = 'k')
        P.vlines(x = 1.0, ymin=0.0, ymax = horizLine, linestyles = 'dashed', 
            colors='k')
        P.hlines(y = horizLine, xmin=0.0, xmax = 1.0, linestyles = 'dashed', 
            colors='k')

        P.xlabel('Ratio $R_{i,t}$ : $K^{[R]}_{i,t}$')
        P.ylabel('$pEm^{[R]}_{i,t}$', rotation = 0, labelpad = 20)
        plotFname = 'pEmigration.png'
        P.savefig(plotFname, format='png', dpi=600)
        P.show()

        #### show prob of emigrating from i to j with distance and R
        R = np.array([.10, .40, .70, 1.1])
        tau = 1.4       # 1.4
        rr = np.exp(-R*tau)
        dij = np.arange(2200.)
        delt = 1200.0
        ax1 = P.gca()
#        ax1.figure(figsize=(8,6))
        relPr = rr[0] * np.exp(-(dij) / delt)
        lns0 = ax1.plot(dij, relPr, label = 'prop. K = ' + str(R[0]))
        relPr = rr[1] * np.exp(-(dij) / delt)
        lns1 = ax1.plot(dij, relPr, label = 'prop. K = ' + str(R[1]))
        relPr = rr[2] * np.exp(-(dij) / delt)
        lns2 = ax1.plot(dij, relPr, label = 'prop. K = ' + str(R[2]))
        relPr = rr[3] * np.exp(-(dij) / delt)
        lns3 = ax1.plot(dij, relPr, label = 'prop. K = ' + str(R[3]))
        lns = lns0 + lns1 + lns2 + lns3 
        labs = [l.get_label() for l in lns]
        ax1.legend(lns, labs, loc = 'upper right')
        ax1.set_xlabel('Distance cell $i$ to cell $j$')
        ax1.set_ylabel('$RelProbEm^{[R]}_{i,j,t}$')
        plotFname = 'relPrEm.png'
        P.savefig(plotFname, format='png', dpi=600)

        P.show()

class EmigrationSurvive(object):
    def __init__(self):
        """
        Explore function and parameters for emigration
        """
#        gammaPara = (-4.0, 3.0)
        seed = 450.0
#        R = np.array([5, 13, 21, 29])
        RContinuous = np.arange(40)
        RHa = RContinuous / 4.0
        gamma = 0.1
        tau = .001
        delt = 0.8
        PrEmig = (1.0 - np.exp(-RContinuous*gamma)) * np.exp(-seed*tau)

        rDiff = np.abs(RHa - 5.0)
#        rDiff = np.abs(RContinuous - 20.0)
        minRDiff = np.min(rDiff)
        maskR = rDiff == minRDiff
        emK = PrEmig[maskR]
        rK = RHa[maskR]
#        rK = RContinuous[maskR]
        P.figure(figsize=(7,6))
        P.plot(RHa, PrEmig, color = 'k')
        P.vlines(x = rK, ymin=0.0, ymax = emK, linestyles = 'dashed', 
            colors='k')
        P.hlines(y = emK, xmin=0.0, xmax = rK, linestyles = 'dashed', 
            colors='k')
    
        print('pEm20', PrEmig[maskR])

        P.xlabel('$R_{i,t}$ ($ha^{-1}$)')
        P.ylabel('$pEm^{[R]}_{i,t}$', rotation = 0, labelpad = 20)
        plotFname = 'pEmigrationSurvive.png'
        P.savefig(plotFname, format='png', dpi=600)
        P.show()

        #### show prob of emigrating from i to j with distance and R
        S = 450 #np.array([7500, 900, 300, 75])
        R = np.array([1, 3, 5, 7, 9])
        dij = np.arange(2000.)/1000.
        ax1 = P.gca()
#        ax1.figure(figsize=(8,6))
        relPr = np.exp(-R[3]*4*gamma) * (1.0 - np.exp(-S*tau)) * np.exp(-dij * delt)
        minPr = relPr[-1]
        lns3 = ax1.plot(dij, relPr/minPr, label = str(R[3]) + ' Rats ' + '$ha^{-1}$')

        relPr = np.exp(-R[0]*4*gamma) * (1.0 - np.exp(-S*tau)) * np.exp(-dij * delt)
        lns0 = ax1.plot(dij, relPr/minPr, label = str(R[0]) + ' Rats ' + '$ha^{-1}$')
        relPr = np.exp(-R[1]*4*gamma) * (1.0 - np.exp(-S*tau)) * np.exp(-dij * delt)
        lns1 = ax1.plot(dij, relPr/minPr, label = str(R[1]) + ' Rats ' + '$ha^{-1}$')
        relPr = np.exp(-R[2]*4*gamma) * (1.0 - np.exp(-S*tau)) * np.exp(-dij * delt)
        lns2 = ax1.plot(dij, relPr/minPr, label = str(R[2]) + ' Rats ' + '$ha^{-1}$')
        lns = lns0 + lns1 + lns2 + lns3 
        labs = [l.get_label() for l in lns]
        ax1.legend(lns, labs, loc = 'upper right')
        ax1.set_xlabel('Distance cell $i$ to cell $j$ (km)')
        ax1.set_ylabel('$RelProbEm^{[R]}_{i,j,t}$')
        plotFname = 'relPrEm.png'
        P.savefig(plotFname, format='png', dpi=600)

        P.show()



class EmigrationStoat(object):
    def __init__(self):
        """
        Explore function and parameters for emigration
        """
        rodent = 5.0        ## 500.     # Beech forest
        stoat = np.array([1, 3, 5, 8])
        stoatCont = np.arange(1, 10, .1)
        gamma = .2            ##.75
        tau = 0.25
        delt = 0.4  #1625.0
#        PrEmig = 1.0 - np.exp(-stoatCont**2 / rodent**gamma)
        PrEmig = (1.0 - np.exp(-stoatCont*gamma)) * np.exp(-rodent*tau)

        sDiff = np.abs(stoatCont - 7.0)
        minSDiff = np.min(sDiff)
        maskS = sDiff == minSDiff
        emK = PrEmig[maskS]
        sK = stoatCont[maskS]
        P.figure(figsize=(7,6))
        P.plot(stoatCont, PrEmig, color = 'k')
        P.vlines(x = sK, ymin=0.0, ymax = emK, linestyles = 'dashed', 
            colors='k')
        P.hlines(y = emK, xmin=0.0, xmax = sK, linestyles = 'dashed', 
            colors='k')
    
        print('pEm stoat', PrEmig[maskS])

        P.xlabel('$S_{k,t}$ ($km^{-2}$)')
        P.ylabel('$pEm^{[S]}_{k,t}$', rotation = 0, labelpad = 20)
        plotFname = 'pEmigrationStoat.png'
        P.savefig(plotFname, format='png', dpi=600)
        P.show()

        #### show prob of emigrating from i to j with distance and R
        R = 5 #np.array([7500, 900, 300, 75])
        dij = np.arange(7000.)/1000
        ax1 = P.gca()
#        ax1.figure(figsize=(8,6))
#        relPr = np.exp(-stoat[0]**2 / rodent**tau) * np.exp(-(dij) / delt)
        Pr = np.zeros((4, len(dij)))
        for i in range(4):
            Pr[i] = (np.exp(-stoat[i]*gamma) * (1- np.exp(-rodent*tau)) * 
                    np.exp(-(dij) * delt))
        sumPr = np.sum(Pr, axis = 0)
        relPr = Pr / np.min(Pr)
        lns0 = ax1.plot(dij, relPr[0], label = str(stoat[0]) + ' Stoats ' + '$km^{-2}$')
#        relPr = np.exp(-stoat[1]**2 / rodent**tau) * np.exp(-(dij) / delt)
##        relPr = np.exp(-stoat[1]*gamma) * (1- np.exp(-rodent*tau)) * np.exp(-(dij) / delt)
        lns1 = ax1.plot(dij, relPr[1], label = str(stoat[1]) + ' Stoats ' + '$km^{-2}$')
#        relPr = np.exp(-stoat[2]**2 / rodent**tau) * np.exp(-(dij) / delt)
##        relPr = np.exp(-stoat[2]*gamma) * (1- np.exp(-rodent*tau)) * np.exp(-(dij) / delt)
        lns2 = ax1.plot(dij, relPr[2], label = str(stoat[2]) + ' Stoats ' + '$km^{-2}$')
#        relPr = np.exp(-stoat[3]**2 / rodent**tau) * np.exp(-(dij) / delt)
##        relPr = np.exp(-stoat[3]*gamma) * (1- np.exp(-rodent*tau)) * np.exp(-(dij) / delt)
        lns3 = ax1.plot(dij, relPr[3], label = str(stoat[3]) + ' Stoats ' + '$km^{-2}$')
        lns = lns0 + lns1 + lns2 + lns3 
        labs = [l.get_label() for l in lns]
        ax1.legend(lns, labs, loc = 'upper right')
        ax1.set_xlabel('Distance cell $k$ to cell $l$ (km)')
        ax1.set_ylabel('$RelProbEm^{[S]}_{k,l,t}$')
        plotFname = 'stoatRelPrEm.png'
        P.savefig(plotFname, format='png', dpi=600)

        P.show()
    

class EmigrationKiwi(object):
    def __init__(self):
        """
        Explore function and parameters for emigration
        """
        kiwi = np.array([2, 8, 16, 24])
        kiwiCont = np.arange(1, 25, .1)
        gamma = .1
        PrEmig = 1.0 - np.exp(-kiwiCont * gamma)

        sDiff = np.abs(kiwiCont - 15.0)
        minSDiff = np.min(sDiff)
        maskS = sDiff == minSDiff
        emK = PrEmig[maskS]
        sK = kiwiCont[maskS]
        P.figure(figsize=(7,6))
        P.plot(kiwiCont, PrEmig, color = 'k')
        P.vlines(x = sK, ymin=0.0, ymax = emK, linestyles = 'dashed', 
            colors='k')
        P.hlines(y = emK, xmin=0.0, xmax = sK, linestyles = 'dashed', 
            colors='k')
    
        print('pEm kiwi', PrEmig[maskS])

        P.xlabel('$S_{k,t}$ ($km^{-2}$)')
        P.ylabel('$pEm^{[S]}_{k,t}$', rotation = 0, labelpad = 20)
        plotFname = 'pEmigrationKiwi.png'
        P.savefig(plotFname, format='png', dpi=600)
        P.show()

        #### show prob of emigrating from i to j with distance and R
        dij = np.arange(5000.) / 1000
        delt = .6
        ax1 = P.gca()
#        ax1.figure(figsize=(8,6))
        relPr = np.exp(-kiwi[0]*gamma) * np.exp(-(dij) * delt)
        lns0 = ax1.plot(dij, relPr, label = str(kiwi[0]) + ' kiwi ' + '$km^{-2}$')
        relPr = np.exp(-kiwi[1]*gamma) * np.exp(-(dij) * delt)
        lns1 = ax1.plot(dij, relPr, label = str(kiwi[1]) + ' kiwi ' + '$km^{-2}$')
        relPr = np.exp(-kiwi[2]*gamma) * np.exp(-(dij) * delt)
        lns2 = ax1.plot(dij, relPr, label = str(kiwi[2]) + ' kiwi ' + '$km^{-2}$')
        relPr = np.exp(-kiwi[3]*gamma) * np.exp(-(dij) * delt)
        lns3 = ax1.plot(dij, relPr, label = str(kiwi[3]) + ' kiwi ' + '$km^{-2}$')
        lns = lns0 + lns1 + lns2 + lns3 
        labs = [l.get_label() for l in lns]
        ax1.legend(lns, labs, loc = 'upper right')
        ax1.set_xlabel('Distance cell $k$ to cell $l$ (km)')
        ax1.set_ylabel('$RelProbEm^{[S]}_{k,l,t}$')
        plotFname = 'kiwiRelPrEm.png'
        P.savefig(plotFname, format='png', dpi=600)

        P.show()
    



class FunctionalResp(object):
    def __init__(self):
        """
        Explore functional responses
        """

        self.minK = 0.25
        self.N = np.arange(0.0, 40., .1)
        self.alpha = .2   
        self.theta = 1.0       
        self.handle = 1.0   #.1428      

#        self.afx(6.75, 40000.)
#        self.hfx(6.75, 40000)

        self.functRespIII()

    def afx(self, c, ns):
        self.alpha = c / ns / (1.0 - (c*self.handle))
        print('alpha', self.alpha, 'handle', self.handle)

    def hfx(self, c, ns):
        self.handle = ((self.alpha * ns) - c) / c / self.alpha / ns
        print('alpha', self.alpha, 'h', self.handle)


    def functRespIII(self):
        """
        calc type III functional respons
        """
        numer = self.alpha * self.N**self.theta
        denom = 1.0 + self.alpha * self.handle * self.N**self.theta                      
        self.C = self.minK + numer / denom
        rdiff = np.abs(self.N - 8.0)
        rmask = (rdiff == np.min(rdiff))
        horizLine = self.C[rmask]
        rdiff2 = np.abs(self.N - 5.0)
        rmask2 = (rdiff2 == np.min(rdiff2))
        horizLine2 = self.C[rmask2]
        print('min and max C', np.min(self.C), np.max(self.C))
        P.figure()
        P.plot(self.N, self.C)
        P.xlabel('Rodent density $(individuals * ha^{-1})$')
        P.ylabel('Stoat K $(individuals * km^{-2})$')
        P.vlines(x = 8.0, ymin=0.0, ymax = horizLine, 
            linestyles = 'dashed', colors='k')
        P.hlines(y = horizLine, xmin=0.0, xmax = 8.0, 
            linestyles = 'dashed', colors='k')
        ### line x == 5.0
        P.vlines(x = 5.0, ymin=0.0, ymax = horizLine2, 
            linestyles = 'dashed', colors='k')
        P.hlines(y = horizLine2, xmin=0.0, xmax = 5.0, 
            linestyles = 'dashed', colors='k')
        plotFname = 'stoatK.png'
#        P.savefig(plotFname, format='png', dpi=400)
  
        P.show()
  
class genLogisticFX(object):
    def __init__(self):
        """
        Explore 'generalised logistic function' for sigmoid shape
        """
        self.a = 1.0    # lower asymptote
        self.k = 12.0     # upper asymptote 6 - 8
        self.b = 0.25   # 0.25     # growth rate .25, .3
        self.shiftR = 1.2
        self.v = .45    # affects near which asymptote we obs greatest growth rate
        self.q = 3.   #self.shiftR + 1.0     # relates to inflection point

        self.N = np.arange(0.0, 26.0, 0.1)

        self.genLogFX()
        self.plotGLF()


    def genLogFX(self):
        """
        calc generalised logistic function
        """
        numer = self.k - self.a
        denom = (1.0 + (self.q * np.exp(-self.b * (self.N - self.shiftR))))**(1.0 / self.v)
        self.C = self.a + (numer / denom)

    def plotGLF(self):
        rdiff = np.abs(self.N - 8.0)
        rmask = (rdiff == np.min(rdiff))
        horizLine = self.C[rmask]
        rdiff2 = np.abs(self.N - 5.0)
        rmask2 = (rdiff2 == np.min(rdiff2))
        horizLine2 = self.C[rmask2]
        rdiff3 = np.abs(self.N - 2.5)
        rmask3 = (rdiff3 == np.min(rdiff3))
        horizLine3 = self.C[rmask3]
        print('stoatK at R = 2.5:', horizLine3,
                'stoatK at R = 5.0:', horizLine2,
                'stoatK at R = 8.0:', horizLine)
        print('min and max C', np.min(self.C), np.max(self.C))
        P.figure()
        P.plot(self.N, self.C, color='k', linewidth = 3.0)
        P.xlabel('Rodent density $(individuals * ha^{-1})$')
        P.ylabel('Stoat K $(individuals * km^{-2})$')
        P.vlines(x = 8.0, ymin=0.0, ymax = horizLine, 
            linestyles = 'dashed', colors='k')
        P.hlines(y = horizLine, xmin=0.0, xmax = 8.0, 
            linestyles = 'dashed', colors='k')
        ### line x == 5.0
        P.vlines(x = 5.0, ymin=0.0, ymax = horizLine2, 
            linestyles = 'dashed', colors='k')
        P.hlines(y = horizLine2, xmin=0.0, xmax = 5.0, 
            linestyles = 'dashed', colors='k')
        ### line x == 2.5
        P.vlines(x = 2.5, ymin=0.0, ymax = horizLine3, 
            linestyles = 'dashed', colors='k')
        P.hlines(y = horizLine3, xmin=0.0, xmax = 2.5, 
            linestyles = 'dashed', colors='k')
        plotFname = 'stoatK.png'
        P.savefig(plotFname, format='png', dpi=400)
        P.show()


 
class KiwiGrowth(object):
    def __init__(self):
        """
        Explore fx for realised kiwi population growth
        """
        self.BRmax = 0.4
        self.BRmin = -0.04
        self.psi = 0.5
        self.S = np.arange(0.0, 8.1, 0.1)
        self.kiwiRealisedGrowth()
        self.plotRealR()
    

    def kiwiRealisedGrowth(self):
        self.realR = self.BRmin + (np.exp(-self.psi*self.S)*(self.BRmax - self.BRmin))
        
    def plotRealR(self):
        rdiff = np.abs(self.realR - 0.0)
        rmask = (rdiff == np.min(rdiff))
        horizLine = 0.0
        vertLine = self.S[rmask]
        print('stoat density with 0 kiwi growth', vertLine)
        P.figure()
        P.plot(self.S, self.realR,  color='k', linewidth = 3.0)
        P.xlabel('Stoat density $(individuals * km^{-2})$')
        P.ylabel('Kiwi realised pop. growth rate $(individuals * year^{-1})$')
        P.vlines(x = vertLine, ymin=self.BRmin, ymax = horizLine, 
            linestyles = 'dashed', colors='k')
        P.hlines(y = horizLine, xmin=0.0, xmax = vertLine, 
            linestyles = 'dashed', colors='k')

        P.savefig('kiwiRealGrowth.png', format='png', dpi=400)
        P.show()

        P.show()


class RickerGrowth(object):
    def __init__(self):
        self.r = 0.25       #0.02
        self.ps = .2
        self.k = 1360       # 20 / km-sq * 10 km-sq
        self.n0 = 77        # .25/km-sq * 10 km-sq
        self.nyears = 9
        self.years = np.arange(self.nyears, dtype = int)
        self.iter = 10000
        self.narray = np.zeros((self.nyears, self.iter))
        self.lastYearPop = np.zeros(self.iter)
        
        for i in range(self.iter):
            self.n = self.n0
            ## loop years
            for j in range(self.nyears):
                self.n = self.n * np.exp(self.r * (1.0 - (self.n / self.k)))
#                self.n = np.round(np.exp(np.random.normal(np.log(self.n + 1.0), self.ps)) - 1.0, 0)
                self.n = np.random.poisson(self.n)
                self.narray[j, i] = self.n
            self.lastYearPop[i] = self.n
#        self.narray = self.narray / self.iter

        totGrowthRate = (self.lastYearPop - self.n0) / self.n0
        print('mean total growth Rate', np.mean(totGrowthRate))
        print('mean last year', np.mean(self.lastYearPop), 'quants',
            mquantiles(self.lastYearPop, prob=[.05, .5, .95]))

        P.figure()
        P.subplot(2,1,1)
        meanN = np.mean(self.narray, axis = 1)
        quants = mquantiles(self.narray, prob = [.05, .95], axis = 1)
        P.plot(self.years, meanN, linewidth = 3, color='k')
        P.plot(self.years, quants[:,0],  color='k', linestyle='dashed')
        P.plot(self.years, quants[:,1],  color='k', linestyle='dashed')
        P.xlabel('years')
        P.ylabel('density')
        self.growthRate = (self.narray[1:] - self.narray[:-1]) / self.narray[:-1]
        P.subplot(2,1,2)
        meanGR = np.mean(self.growthRate, axis = 1)
        P.plot(self.years[1:], meanGR)
        P.xlabel('Years')
        P.ylabel('Growth Rate')
        P.show()


def AlleeEffect():
    r =  .375   #0.02
    k = 200       # 20 / km-sq * 10 km-sq
    n0 = 4        # .25/km-sq * 10 km-sq
    n = n0
    nyears = 30
    years = np.arange(nyears, dtype = int)
    narray = np.zeros(nyears)
    ## loop years
    for j in range(nyears):
        n = n * np.exp(r * (1.0 - (n / k)))
#        n = np.round(np.exp(np.random.normal(np.log(n + 1.0), ps)) - 1.0, 0)
#        n = np.random.poisson(n)
        narray[j] = n
    b0 = r
    b1 = -r/k
    N = np.arange(1, k+50)
    GR = b0 + b1*N

    T = 75
    alleeN = np.arange(1,T+1)
    nDat = len(alleeN)
    a0 = -0.1   #-0.05
    a1 = 0.0092       #.0078
    grAllee = np.zeros(nDat)
    grAllee[0] = a0
    gr = a0
    for i in range(1,nDat):
        gr = gr + a1 * ((T-alleeN[i])/T)
        grAllee[i] = gr 

    P.figure(figsize=(9,7))
    P.plot(N,GR, linewidth=4, color='k')
#    P.plot(alleeN, grAllee, linewidth=4, color='r')
    
    P.xlabel('Population density', fontsize = 18)
    P.ylabel('Growth rate', fontsize = 18)
    P.hlines(y = 0, xmin=0.0, xmax = N[-1], 
            linestyles = 'dashed', colors='k')
    P.savefig('AlleeGrowRate1.png', format='png', dpi=400)

    P.figure(figsize=(9,7))
    P.plot(N,GR, linewidth=4, color='k')
    P.plot(alleeN, grAllee, linewidth=4, color='r')
    P.xlabel('Population density', fontsize = 18)
    P.ylabel('Growth rate', fontsize = 18)
    P.hlines(y = 0, xmin=0.0, xmax = N[-1], 
            linestyles = 'dashed', colors='k')
    P.savefig('AlleeGrowRate2.png', format='png', dpi=400)

    P.figure(figsize=(9,7))
    P.plot(years, narray, linewidth=4, color='k')
    P.xlabel('Time', fontsize = 18)
    P.ylabel('Population density', fontsize = 18)
    P.hlines(y = k, xmin=0.0, xmax = nyears-5, 
            linestyles = 'dashed', colors='k')
    P.savefig('AlleeLogistic.png', format='png', dpi=400)
    P.show()

 
class PredationSurvival(object):
    def __init__(self):
        """
        Explore fx for realised kiwi survivorship with predation
        """
        self.stoat = np.arange(8.)
        self.kiwiSurv = .03
        self.r = 0.2    #0.1
        self.kiwiK = 20.
        self.kiwiN = 5.0
        self.s = np.exp(-self.kiwiSurv * self.stoat)
        self.s = np.where(self.s <.85, .85, self.s)
        predN = self.kiwiN * np.exp(self.r * ( 1.0 - (self.kiwiN / self.kiwiK))) * self.s
        print('example pop growth', predN)

        P.figure()
        P.subplot(2,1,1)
        P.plot(self.stoat, self.s, color = 'k', linewidth = 4)
        P.ylabel('Kiwi predation survivorship')
        
        P.subplot(2,1,2)
        growthrate = np.log(np.exp(self.r) * self.s)
        pArr = np.zeros((len(self.stoat), 3))
        pArr[:,0] = self.stoat
        pArr[:,1] = self.s
        pArr[:,2] = growthrate
        print('growthrate', pArr)
        P.plot(self.stoat, growthrate, color = 'k', linewidth = 4)
        P.xlabel('Stoat density $(individuals * km^{-2})$')
        P.ylabel('Kiwi growth rate')
        P.savefig('kiwiPredationSurvive.png', format='png', dpi=400)
###        P.show()

        #############################################
        #### Rodent competition effect no predation
        self.rod = np.arange(20)
        self.compEffect = 0.007
        self.c = np.exp(-self.compEffect * self.rod)
        self.c = np.where(self.c <.85, .85, self.c)
        predN = self.kiwiN * np.exp(self.r * ( 1.0 - (self.kiwiN / self.kiwiK))) * self.c
        print('example pop growth', predN)

        P.figure()
        P.subplot(2,1,1)
        P.plot(self.rod, self.c, color = 'k', linewidth = 4)
        P.ylabel('Rodent competition factor')
        
        P.subplot(2,1,2)
        growthrate = np.log(np.exp(self.r) * self.c)
#        pArr = np.zeros((len(self.rod), 3))
#        pArr[:,0] = self.stoat
#        pArr[:,1] = self.s
#        pArr[:,2] = growthrate
#        print('growthrate', pArr)
        P.plot(self.rod, growthrate, color = 'k', linewidth = 4)
        P.xlabel('Rodent density $(individuals * ha^{-1})$')
        P.ylabel('Kiwi growth rate')
        P.savefig('rodentCompetition.png', format='png', dpi=400)
#        P.show()


        #############################################
        #### Combined effect of Rodent competition and stoat predation
        self.rod = np.arange(20)
        self.stoat = 2.             # 2 stoats
        self.c = np.exp(-self.compEffect * self.rod)
        self.s = np.exp(-self.kiwiSurv * self.stoat)
        self.CS = self.c * self.s
        self.CS = np.where(self.CS <.85, .85, self.CS)
        predN = self.kiwiN * np.exp(self.r * ( 1.0 - (self.kiwiN / self.kiwiK))) * self.CS
        print('example pop growth', predN)

        P.figure()
        P.subplot(2,1,1)
        P.plot(self.rod, self.CS, color = 'k', linewidth = 4)
        P.ylabel('Rodent-stoat factor')
        
        P.subplot(2,1,2)
        growthrate = np.log(np.exp(self.r) * self.CS)
#        pArr = np.zeros((len(self.rod), 3))
#        pArr[:,0] = self.stoat
#        pArr[:,1] = self.s
#        pArr[:,2] = growthrate
#        print('growthrate', pArr)
        P.plot(self.rod, growthrate, color = 'k', linewidth = 4)
        P.xlabel('Rodent density $(individuals * ha^{-1})$')
        P.ylabel('Kiwi growth rate')
        P.savefig('rodentStoatEffect.png', format='png', dpi=400)
        P.show()




    

class CVTest():
    def __init__(self):

        n = 15
        pbar = np.repeat(.35, n)
    
        p = np.random.uniform(0.0,1.0, n)
        x = np.random.binomial(1, p, n)
        devPred = (np.sum(stats.binom.logpmf(x, 1, p)))
        devMean = (np.sum(stats.binom.logpmf(x, 1, pbar)))
#        r = (np.sum(devPred) / np.sum(devMean))
        r = np.exp(devPred) / np.exp(devMean)

        print('p =', p)
        print('x', x)
        print('pbar =', pbar)
        print('devPred', devPred)
        print('devMean', devMean)

        print('dev diff', devPred - devMean)
        print('r = ', r)


def binomTest():
    risk = [.8, .1, .075, .025]
#    risk = [.6, .3, .1]
    print('risk', risk)
#    w = np.array([6., 3., 1.])
#    w = np.array([1., 3., 6.])
    w = np.array([12, 6, 3, 1])
    print('test weighting', w)
#    prp = np.array([.4, .1, .01])
#    prp = np.array([1., 1., 1.])
    prp = np.array([0, .4, .1, .01])
    n = 20000
    pu = 1
    z = np.random.multinomial(pu, risk, n)
#    print('z', z)
    sez = 1 - (1.0 - 0.05 * prp)**w
    print('sez', sez)
    seWAve = np.sum(sez * risk) / np.sum(risk)
    se = 1 - (1 - seWAve)**pu
    print('seWAve', seWAve, 'se', se)
    d = 0
    for i in range(n):
        testResults = np.random.binomial(z[i], sez)
#        print('z', z[i], 'results', testResults)
        dtmp = np.sum(testResults)
        if dtmp > 0:
            d += 1
    seActual = d/n
    print('seActual', seActual)
  
    print('')
    print('')
    print('#################################')
    print('############### test 2 ##########')
    iter = 10000
    seu = 0.05 
    pu = 3
    w = np.repeat([20,8,3,1],4)
    n = len(w)
    samp = np.array([1,1,1,1, 1,1,0,0, 1,1,0,0, 1,0,0,0])
    sez = 1 - (1 - seu * samp)**w 
    print('sez', sez)
    seWAve = np.sum(sez * w) / np.sum(w)
    prp = np.sum(samp) / n
    se = 1 - (1 - seWAve)**pu
    print('seWAve', seWAve, 'se', se, 'prp', prp)
    d = 0
    a = np.arange(n)
    for i in range(iter):
        z_array = np.array([], dtype=int)
        risk = w / np.sum(w)
        for j in range(pu):

            det = np.random.multinomial(1, risk)
            zCell = a[det == 1]
            z_array = np.append(z_array, zCell)
            risk[zCell] = 0.0
            risk = risk / np.sum(risk)
#        print('zCell', zCell)
        sez_i = sez[z_array]
#        print('sez_i', sez_i)
        testResults = np.random.binomial(1, sez_i)
        dtmp = np.sum(testResults)
#        print('test results', testResults)
        if dtmp > 0:
            d += 1
    seActual = d/iter
    print('seActual', seActual)





def CDFTest():
    x = 40.0
    m0 = 30.
    s0 = 15.
    m1 = 50.
    s1 = 15.
    f0 = stats.norm.cdf(x, m0, s0)
    f1 = stats.norm.cdf(x, m1, s1)
    ## NEW FORMULATION FOR EQUATIONS 1-3
    p0 = (1.0 - f0)  
    p1 = f0 * (1.0 - f1)
    p2 = f0 * f1
    print('NEW IDEA: f0', f0, 'f1', f1, 'p0', p0, 'p1', p1, 'p2', p2, 'sum', p0+p1+p2)

    ## OLD FORMULATION FOR EQUATIONS 1-3
    p0 = (1.0 - f0) * (1.0 - f1)  
    p1 = f0 * (1.0 - f1)
    p2 = f0 * f1
    print('OLD APPROACH: f0', f0, 'f1', f1, 'p0', p0, 'p1', p1, 'p2', p2, 'sum', p0+p1+p2)



class SurvivorPrp(object):
    def __init__(self):
        n = 10000.
        nYears = 40
        s = .9048
        years = np.arange(1,41)
#        pop = np.zeros(nYears)
#        for i in range(nYears):
#            n = n * s
#            pop[i] = n
#        print('pop', pop[-20:])
#        age = years             # * 365
        sur = .10           #/ 365     # constant daily mortality rate (exponential model)
        prpSurv = np.exp(-years * sur)
#        prpSurv = np.exp(-(age+365) * sur)
        print('daily survivorship', sur, 'prpsurv', prpSurv[-10:], 'years', years[-6:])
#        years = (age).astype(int)
        P.figure(figsize=(11,9))
        P.subplot(1,2,1)
        P.plot(years, prpSurv, color = 'k', linewidth = 4.0)
        P.xlabel('Age (years)')
        P.ylabel('Mean age specific survival')
        
#        years = np.arange(1, 41)      #np.arange(366, (366 + (5*365)))

        psur = (np.exp(-sur))**years
#        psur = np.exp(-pd*years)
        print('prpSurv', psur[-10:], 'years', years[-6:])
        P.subplot(1,2,2)
        P.plot((years), psur, color = 'k', linewidth = 4.0)
        P.xlabel('Age (years)')
        P.ylabel('Mean age specific survival')

        

        FName = 'prpSurviving.png'
        P.savefig(FName, format='png')

        P.show()

        prpSurv = np.zeros(len(years))
        p = 1.0
        sur = .1
        for i in years:
            p = (np.exp(-sur * i))
#            prpSurv[i] = p
            print('age', i, 'prpSurv', p)
   



def LatHypCubeFX():
    v = 4
    n = 1
    lhd = lhs(v, samples=n, criterion = 'm')
    min = [-5, 10, 30, 50]
    max = [7, 25, 45, 100]
    print('lhd', lhd)
    rand = np.zeros((n, v))
    for i in range(v):
        rand[:, i] = reScale(min[i], max[i], lhd[:, i])
#        rand[:, i] = np.random.uniform(min[i], max[i], n)
    print('rand', rand)
    P.figure()
    P.plot(rand[:,0], rand[:,1])
    P.plot(rand[:,1], rand[:,2])
    P.plot(rand[:,2], rand[:,3])
    P.plot(rand[:,0], rand[:,3])
    P.plot(rand[:,1], rand[:,3])
    P.plot(rand[:,0], rand[:,2])
    P.show()
    
    
class SimKiwiDyn2(object):
    def __init__(self):

        self.N = np.array([.025, 1, 3, 6 , 10, 14, 16])
        self.K = 20.0
        self.nYears = 30
        self.recruitDecay = 14.5         #2.0
        self.perCapRec = 0.6           #.8
        self.B = np.arange(1, 25)
        self.pMaxRec = np.exp(-self.B**2 / self.recruitDecay**2)
        self.adultSurv = np.exp(-0.1)        # constant annual survival rate (exponential model)
        self.adultSurvDecay = 110.0          #.1
        ## PREDATOR
        self.psi = 2.0
        self.stoat = np.arange(4)
        

        self.findK()

        self.plotAgeSpecSurv()
        
        self.plotKiwiSurvRecruit()
        self.plotAdultSurv()
        self.plotProbRec()
        self.simPop()


    def findK(self):
        N = 20.0
        for i in range(1,50):
            NStar = N * self.adultSurv * (np.exp(-N**2 / self.adultSurvDecay**2))
            recruits = NStar * self.perCapRec * np.exp(-NStar**2 / self.recruitDecay**2)
            N = NStar + recruits
#            print('i', i, 'N', N)
        self.K = np.round(N, 2)
        print('K', self.K)

    def simPop(self):
        self.nCells = len(self.N)
        self.pop = np.zeros((self.nYears, self.nCells))
        self.meanN = np.mean(self.N)
        self.meanPop = np.zeros(self.nYears)
        for i in range(self.nYears):
            ## sim cells
            surv_i = self.adultSurv * (np.exp(-self.N**2 / self.adultSurvDecay**2))
            NStar = self.N * surv_i
            pMaxRec = np.exp(-NStar**2 / self.recruitDecay**2)
            recRate = self.perCapRec * pMaxRec
            nRecruit = NStar * recRate
            self.N = nRecruit + NStar
            self.pop[i] = self.N
#            self.pop[i] = np.random.poisson(self.N)
            if i > 0:
                print('i', i,  (self.pop[i,:3] - self.pop[(i-1), :3]) / self.pop[(i-1), :3], 
                    'surv_i', surv_i[:2])
            
            ## sim means
            meanSurv = self.adultSurv * (np.exp(-self.meanN**2 / self.adultSurvDecay**2))
            meanNStar = self.meanN * meanSurv
            pMaxRec = np.exp(-meanNStar**2 / self.recruitDecay**2)
            recRate = self.perCapRec * pMaxRec
            nRecruit = meanNStar * recRate
            self.meanN = nRecruit + meanNStar
            self.meanPop[i] = self.meanN
        self.plotSimPop()
        pChange = (self.pop[-1] - self.pop[0]) / self.pop[0]
        print('mean pop end', np.mean(self.pop[-1]), 'meanN', self.meanN,
            'pop end', self.pop[-1], 'pChange', pChange, 'Prp K', self.pop[-1] / self.K)


    def plotSimPop(self):
        P.figure()
        years = np.arange(1, self.nYears + 1)
        for i in range(self.nCells):
            P.plot(years, self.pop[:, i], color='k', linewidth = 2)

#        P.plot(years, self.meanPop, color = 'k', linewidth = 3)
        P.ylabel('Kiwi density (' + '$km^{-2}$)', fontsize = 14)
        P.xlabel('Years', fontsize = 14)
        FName = 'simKiwiDensityDepend.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()

    def plotKiwiSurvRecruit(self):
        P.figure(figsize=(16,9))
        P.subplot(1,2,1)
        n = np.arange(1, 25)
        pMaxSurv = (np.exp(-n**2 / self.adultSurvDecay**2))
        sur = self.adultSurv * pMaxSurv
        P.plot(n, sur, color = 'k', linewidth = 4)
        P.xlabel('Kiwi density (' + '$km^{-2}$)', fontsize = 14)
        P.ylabel('Annual prob. adult survival', fontsize = 14)
        P.title('(A)', loc = 'left', fontsize = 14)
        P.subplot(1,2,2)
        ax1 = P.gca()
        pMaxRec = (np.exp(-n**2 / self.recruitDecay**2) *
                np.exp(-self.stoat[0] * self.psi))
        lns0 = ax1.plot(n, pMaxRec * self.perCapRec, color = 'k', linewidth = 2,
            label = '0 stoat $km^{-2}$')
        pMaxRec = (np.exp(-n**2 / self.recruitDecay**2) * 
                np.exp(-self.stoat[1] * self.psi))
        lns1 = ax1.plot(n, pMaxRec * self.perCapRec, color = 'b', linewidth = 2,
            label = '1 stoat $km^{-2}$')
        pMaxRec = (np.exp(-n**2 / self.recruitDecay**2) * 
                np.exp(-self.stoat[2] * self.psi))
        lns2 = ax1.plot(n, pMaxRec * self.perCapRec, color = 'y', linewidth = 2,
            label = '2 stoat $km^{-2}$')
        pMaxRec = (np.exp(-n**2 / self.recruitDecay**2) * 
                np.exp(-self.stoat[3] * self.psi))
        lns3 = ax1.plot(n, pMaxRec * self.perCapRec, color = 'r', linewidth = 2,
            label = '3 stoat $km^{-2}$')

        lns = lns0 + lns1 + lns2 + lns3 
        labs = [l.get_label() for l in lns]
        ax1.legend(lns, labs, loc = 'upper right')
        ax1.set_xlabel('Kiwi density (' + '$km^{-2}$)', fontsize = 14)
        ax1.set_ylabel('Per-capita recruitment', fontsize = 14)
        P.title('(B)', loc = 'left', fontsize = 14)
        FName = 'kiwiAdultSurvivalRecruit.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()


    def plotAdultSurv(self):
        n = np.arange(1, 25)
        pMaxSurv = (np.exp(-n**2 / self.adultSurvDecay**2))
        sur = self.adultSurv * pMaxSurv
        P.figure()
        P.plot(n, sur)
        P.show()

    def plotProbRec(self):
        print('pMaxRec', self.pMaxRec)
        P.figure(figsize = (11,9))
        P.subplot(1,2,1)
        P.plot(self.B, self.pMaxRec * self.perCapRec)
        P.subplot(1,2,2)
        P.plot(self.B, self.pMaxRec)
        P.show()


    def plotAgeSpecSurv(self):
        print('self.adultSurv', self.adultSurv)
        years = np.arange(1, 41)
        psur = self.adultSurv**years
#        psur = np.exp(-pd*years)
        print('prpSurv', psur[-10:], 'years', years[-6:])
        P.figure(figsize = (9,7))
        P.plot((years), psur, color = 'k', linewidth = 4.0)
        P.xlabel('Age (years)', fontsize=14)
        P.ylabel('Mean age specific survival', fontsize=14)
        FName = 'kiwiAgeSpecificSurv.png'
        P.savefig(FName, format='png')
        P.show()




class SimRatDyn(object):
    def __init__(self):
        """
        rat dynamics in 4 ha block
        """
        self.N = np.array([4, 4, 12, 14, 18, 22, 30]) 
#        self.N = np.array([1,3,6,9,15, 20])
#        self.K = 1750.0    #np.array([2., 3., 4., 5., 7., 20.0])
        self.KArray = np.array([7500, 900, 450, 300, 150, 75], dtype = int)
        self.nYears = 7
        self.recruitDecay = 1.0    #1.0        #.97     
        self.perCapRec = 2.5           #.8
        self.adultSurv = np.exp(-0.5)        # constant annual survival rate (exp model)
        self.adultSurvDecay = 1.2  # 1.05    

        self.plotAdultSurv()

        self.plotProbRec()
        self.simPop()

        self.simRatSurv()
        self.simStochastic()
        self.plotPrpSurv()


    def simRatSurv(self):
        nYears = 6
        years = np.arange(1,nYears)
        prpSurv = np.exp(-years * self.adultSurv)
        P.figure(figsize=(11,9))
        P.plot(years, prpSurv, color = 'k', linewidth = 4.0)
        P.xlabel('Age (years)')
        P.ylabel('Mean age specific survival')
        FName = 'ratAgeSurv.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()




    def simPop(self):
        self.nCells = len(self.N)
        self.pop = np.zeros((self.nYears, self.nCells))
        self.meanN = np.mean(self.N)
        self.meanPop = np.zeros(self.nYears)
        N = self.N.copy()
        print('N', N)
        for i in range(self.nYears):
            ## sim cells
            surv_i = self.adultSurv * (np.exp(-N**2 / 
                self.KArray[2]**self.adultSurvDecay))
            NStar = N * surv_i
            pMaxRec = np.exp(-NStar**2 / self.KArray[2]**self.recruitDecay)
            recRate = self.perCapRec * pMaxRec
            nRecruit = NStar * recRate

###            N = NStar * np.exp(self.r * (1.0 - 
###                    (NStar**2 / self.KArray[2]**self.recruitDecay)))

            N = nRecruit + NStar
            self.pop[i] = N
#            self.pop[i] = np.random.poisson(self.N)
#            if i > 0:
#                print('i', i,  (self.pop[i,:3] - self.pop[(i-1), :3]) / self.pop[(i-1), :3], 
#                    'surv_i', surv_i[:2])
            print('i', i, 'N', N)           
            ## sim means
            meanSurv = self.adultSurv * (np.exp(-self.meanN**2 / 
                self.KArray[2]**self.adultSurvDecay))
            meanNStar = self.meanN * meanSurv
            pMaxRec = np.exp(-meanNStar**2 / self.KArray[2]**self.recruitDecay)
            recRate = self.perCapRec * pMaxRec
            nRecruit = meanNStar * recRate
            self.meanN = nRecruit + meanNStar
            self.meanPop[i] = self.meanN
        self.plotSimPop()
        pChange = (self.pop[-1] - self.pop[0]) / self.pop[0]
        print('last population', self.pop[-1])



    def plotSimPop(self):
        P.figure()
        years = np.arange(1, self.nYears + 1)
        for i in range(self.nCells):
            P.plot(years, self.pop[:, i], color = 'k', linewidth = 2)
#        P.plot(years, self.meanPop, color = 'k', linewidth = 3)
        P.xlabel('rat dynamics in 4 ha blockYears')
        P.ylabel('Rat density ' + '(4 $ha)^{-1}$')
        P.title('simRatPop')
        FName = 'simRatDynamics.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()  
                  
    def plotAdultSurv(self):
        n = np.arange(1, 90., 0.1)
        nHa = n / 4.0
        pMaxSurv = (np.exp(-n**2 / self.KArray[0]**self.adultSurvDecay))
        sur = self.adultSurv * pMaxSurv
        P.figure(figsize=(11,9))
        P.subplot(2,2,1)
        P.subplots_adjust(hspace = 0.4, wspace = 0.2)
        P.plot(nHa, sur, color = 'k', linewidth = 4)
        P.title('(A)', loc = 'left')
#        P.title('        self.simRatSurv()Seed Density = ' + str(self.KArray[0]) + ' $m^{-2}$')
        P.xlabel('Rat density ($ha^{-1}$)')
#        P.xlabel('Rat density')
        P.ylabel('Prop. adult surv')
                  
        n = np.arange(1, 25, 0.1)
        nHa = n / 4.0
        pMaxSurv = (np.exp(-n**2 / self.KArray[2]**self.adultSurvDecay))
        sur = self.adultSurv * pMaxSurv
        P.subplot(2,2,2)
        P.plot(nHa, sur, color = 'k', linewidth = 4)
        P.title('(B)', loc = 'left')
        P.xlabel('Rat density ($ha^{-1})$')
        P.ylabel('Prop. adult surv')

        n = np.arange(1, 12, 0.05)
        nHa = n / 4.0
        pMaxSurv = (np.exp(-n**2 / self.KArray[-1]**self.adultSurvDecay))
        sur = self.adultSurv * pMaxSurv
        P.subplot(2,2,3)
        P.plot(nHa, sur, color = 'k', linewidth = 4)
        P.title('(C)', loc = 'left')
        P.xlabel('Rat density ($ha^{-1})$')
        P.ylabel('Prop. adult surv')
                  
        P.subplot(2,2,4)
        years = np.arange(1,6, 0.01)
        prpSurv = self.adultSurv**years
        P.plot(years, prpSurv, color = 'k', linewidth = 4.0)
        P.title('(D)', loc = 'left')
        P.xlabel('Age (years)')
        P.ylabel('Mean age-specific survival')
                  
        FName = 'prpRatAdultSurviving.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()   
                  
    def plotProbRec(self):
        P.figure(figsize = (11,9))
        R = np.arange(1, 90.0, 0.1)
        R_Ha = R / 4.0
        self.pMaxRec = np.exp(-R**2 / self.KArray[0]**self.recruitDecay)
        P.subplot(2,2,1)
        P.subplots_adjust(hspace = 0.4, wspace = 0.2)
        P.plot(R_Ha, self.pMaxRec * self.perCapRec, color = 'k', linewidth = 4.0)
        P.xlabel('Rat Density ($ha^{-1}$)')
        P.ylabel('Per Cap Recruit')
        P.title('(A)', loc='left')

        P.subplot(2,2,2)
        R = np.arange(1, 35.0, 0.1)
        R_Ha = R / 4.0
        self.pMaxRec = np.exp(-R**2 / self.KArray[1]**self.recruitDecay)
        P.plot(R_Ha, self.pMaxRec * self.perCapRec, color = 'k', linewidth = 4.0)
        P.xlabel('Rat Density ($ha^{-1}$)')
        P.ylabel('Per Cap Recruit')
        P.title('(B)', loc='left')
                  
        P.subplot(2,2,3)
        R = np.arange(1, 22.0, 0.05)
        R_Ha = R / 4.0
        self.pMaxRec = np.exp(-R**2 / self.KArray[3]**self.recruitDecay)
        P.plot(R_Ha, self.pMaxRec * self.perCapRec, color = 'k', linewidth = 4.0)
        P.xlabel('Rat Density ($ha^{-1}$)')
        P.ylabel('Per Cap Recruit')
        P.title('(C)', loc='left')
                  
        P.subplot(2,2,4)
        R = np.arange(1, 12.0, 0.05)
        R_Ha = R / 4.0
        self.pMaxRec = np.exp(-R**2 / self.KArray[-1]**self.recruitDecay)
        P.plot(R_Ha, self.pMaxRec * self.perCapRec, color = 'k', linewidth = 4.0)
        P.xlabel('Rat Density ($ha^{-1}$)')
        P.ylabel('Per Cap Recruit')
        P.title('(D)', loc='left')
                  
        FName = 'probabilityRatRecruit.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()  
                
    def simStochastic(self):
        n = 10    
        seed = 450
        nYears = 200
        years = np.arange(nYears)
        nArray0 = np.zeros(nYears)
        P.figure(figsize=(13,11))
        for i in range(nYears):
            if i % 10 == 0:
                seed_i = 7500
#            elif i % 10 == 1:
#                seed_1 = 600
            else: 
                seed_i = seed 
            surv_i = self.adultSurv * (np.exp(-n**2 / 
                seed_i**self.adultSurvDecay))
            NStar = n * surv_i
            pMaxRec = np.exp(-NStar**2 / seed_i**self.recruitDecay)
            recRate = self.perCapRec * pMaxRec
            mu = NStar * (1 + recRate)
            n = np.random.poisson(mu)
            nArray0[i] = n
#            print('i', i, 'n', n, 'year', years[i], 'narr', nArray0[i])
        P.subplot(1,2,1)
        P.plot(years, nArray0, color = 'k', linewidth = 4)
        P.ylim(0,80)
        n = 10
        years1 = np.arange(nYears)
        nArray1 = np.zeros(nYears)
        for i in range(nYears):
            if i % 10 == 0:
                seed_i = 5000
#            elif i % 10 == 1:
#                seed_1 = 600
            else: 
                seed_i = seed 
            surv_i = self.adultSurv * (np.exp(-n**2 / 
                seed_i**self.adultSurvDecay))
            NStar = n * surv_i
            pMaxRec = np.exp(-NStar**2 / seed_i**self.recruitDecay)
            recRate = self.perCapRec * pMaxRec
            mu = NStar * (1 + recRate)
            n = np.random.poisson(mu)
            nArray1[i] = n
        P.subplot(1,2,2)
        P.plot(years1, nArray1, color = 'k', linewidth = 4)
        P.ylim(0,80)
        P.show()  
                  
    def plotPrpSurv(self):
        seed = np.arange(75, 7500)
        surv = self.adultSurv * (np.exp(-20**2 / 
                seed**self.adultSurvDecay))
        P.figure()
        P.plot(seed, surv)
        P.show()
                  
                  
                  
class SimStoatDyn(object):
    def __init__(self):
        """       
        stoat dynamics in 1 km2 block
        """       
        self.nYears = 10
        self.recruitDecay = 1.4  # 2.4      #1.0         #2.0
        self.perCapRec = 4.0    #5.4          #.8
        self.S = 1.0
        self.adultSurv = .5     #np.exp(-0.8)    # constant annual survival rate (exponential model)
        self.adultSurvDecay = 1.6   # 1.6    # 2.8        #.1
                  
        self.plotWrapFX()
        self.plotStoatEqPop()
        self.stoatSeries()
        self.plotSurvYear()
        self.simStochastic()
                  
                  
    def plotWrapFX(self):
        self.R = np.linspace(1, 25)
        self.Rat = 5.
        self.nStoats = np.arange(1,8, 0.1)
        self.getAdultSurvRecruit()
        self.plotStoats()
                  
    def getAdultSurvRecruit(self):
        ## FIX RAT = 5 PER HA
        pMaxSurv = np.exp(-self.nStoats / (self.Rat)**self.adultSurvDecay)   
        self.stoatSur_R5 = self.adultSurv * pMaxSurv
        nSurv = self.nStoats * self.stoatSur_R5
        self.pMaxRec = (np.exp(-self.nStoats / 
             (self.Rat)**self.recruitDecay))
        self.recRate_R5 = self.pMaxRec * self.perCapRec 
        ## FIX STOAT = 1 KM2
        pMaxSurv_S = (np.exp(-self.S / (self.R)**self.adultSurvDecay))  
        self.stoatSur_S = self.adultSurv * pMaxSurv_S
        self.pMaxRec_S = (np.exp(-self.S / 
            (self.R)**self.recruitDecay)) 
        self.recRate_S = self.pMaxRec_S * self.perCapRec 
                  
                  
    def plotStoats(self):
        P.figure(figsize=(16,10))
        P.subplot(2,2,1)
        P.subplots_adjust(hspace = 0.4, wspace = 0.2) 
        P.plot(self.nStoats, self.stoatSur_R5, color='k', linewidth = 4)
        P.xlabel('Stoat Density (' + '$km^{-2}$)', fontsize = 14)
        P.ylabel('Stoat adult survivorship', fontsize = 14)
        P.title('(A)', loc='left')
        P.subplot(2,2,2)
        P.plot(self.nStoats, self.recRate_R5, color='k', linewidth = 4)
        P.xlabel('Stoat Density (' + '$km^{-2}$)', fontsize = 14)
        P.ylabel('Per-capita recruitment', fontsize = 14)
        P.title('(B)', loc='left')
        P.subplot(2,2,3)
        P.plot(self.R, self.stoatSur_S, color = 'k', linewidth = 4)
        P.xlabel('Rat Density (' + '$ha^{-1}$)', fontsize = 14)
        P.ylabel('Stoat adult survivorship', fontsize = 14)
        P.xlim(0, 30)
        P.title('(C)', loc='left')
        P.subplot(2,2,4)
        P.plot(self.R, self.recRate_S, color = 'k', linewidth = 4)
        P.xlabel('Rat Density (' + '$ha^{-1}$)', fontsize = 14)
        P.ylabel('Per-capita recruitment', fontsize = 14)
        P.xlim(0,30)
        P.title('(C)', loc='left')
        FName = 'stoatDynamParas.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()


    def plotStoatEqPop(self):
        self.getNeq()        
        P.figure(figsize=(11,6))
        P.subplot(1,2,1)
        P.plot(self.R, self.nEq, color = 'k', linewidth = 3)
        P.xlabel('Rat Density (' + '$ha^{-1}$)', fontsize = 14)
        P.ylabel('Stoat equilibrium density' + ' ($km^{-2}$)', fontsize = 14)
        P.xlim(0, 20)        
        P.title('(A)', loc='left')
        
        self.plotSimPop()
        P.tight_layout()
        FName = 'stoatEquilPop.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()


    def getNeq(self):
        nR = len(self.R)
        self.nEq = np.zeros(nR)
        for i in range(nR):
            eq_i = 2.0
            rat_i = self.R[i]
            # loop years
            for j in range(15):
                pSurv = (np.exp(-eq_i / (rat_i)**self.adultSurvDecay) * 
                    self.adultSurv)
                NStar = eq_i * pSurv
                recRate = (np.exp(-NStar / (rat_i)**self.recruitDecay) * 
                    self.perCapRec)
                eq_i = NStar * (1.0 + recRate)
            self.nEq[i] = eq_i


    def plotSimPop(self):
        nStoats = np.arange(1, 8)
        self.nCells = len(nStoats)
        nYears = 6
        self.pop = np.zeros((nYears, self.nCells))
        self.pop[0] = nStoats
        n = nStoats.copy()
        for i in range(1, nYears):
            ## sim cells
            pSurv = (np.exp(-n / self.Rat**self.adultSurvDecay)) * self.adultSurv
            NStar = n * pSurv
            recRate = np.exp(-NStar / self.Rat**self.recruitDecay) * self.perCapRec
            n = NStar * (1.0 + recRate)
            self.pop[i] = n
        print('last population', self.pop[-1])
        years = np.arange(nYears)
        P.subplot(1,2,2)
        for i in range(self.nCells):
            P.plot(years, self.pop[:, i], color = 'k', linewidth = 2)
        P.xlabel('Years', fontsize = 14)
        P.ylabel('Stoat density (' + '$km^{-2}$)', fontsize = 14)
        P.title('(B)', loc='left')


    def stoatSeries(self):
        rod = np.array([2,4,5,2,7,7,5, 20, 10, 3])

#        rod = np.repeat(5, 20)
        stoat = 2.0
        nRod = len(rod)
        stoatArray = np.zeros(nRod)
        for i in range(nRod):
            pSurv = (np.exp(-stoat / rod[i]**self.adultSurvDecay)) * self.adultSurv
            NStar = stoat * pSurv
            recRate = np.exp(-NStar / rod[i]**self.recruitDecay) * self.perCapRec
            stoat = NStar * (1.0 + recRate)
            stoatArray[i] = stoat
        P.figure()
        years = np.arange(nRod)
        P.plot(years, stoatArray, color = 'k')
        P.show()
        print('stoat', stoatArray)


    def plotSurvYear(self):
        years = np.arange(1,6, 0.01)
        prpSurv = self.adultSurv**years
        P.plot(years, prpSurv, color = 'k', linewidth = 4.0)
        P.xlabel('Age (years)')
        P.ylabel('Mean age-specific survival')
        FName = 'prpStoatAdultSurviving.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()

                
    def simStochastic(self):
        n = 2    
        rat = 5
        surv = -.7
        decayS = 3.6
        r = 1.6
        decayR = .6
        nYears = 60
        years = np.arange(nYears)
        nArray = np.zeros(nYears)
        rat_i = rat
        for i in range(nYears):
            if i % 10 == 0:
                rat_i = 18
            elif i % 10 == 1:
                rat_i = 11.5
            else: 
                rat_i = rat
            surv_i = self.adultSurv * (np.exp(-n**2 / 
                rat_i**self.adultSurvDecay))
            NStar = n * surv_i
#            print('n', n, 'NStar', NStar, 'surv_i', surv_i)
            pMaxRec = np.exp(-NStar**2 / rat_i**self.recruitDecay)
            recRate = self.perCapRec * pMaxRec
            mu = NStar * (1 + recRate)
#            n = np.random.poisson(mu)
            n = np.exp(np.random.normal(np.log(mu), 0.2))
            nArray[i] = n
        P.figure()
        P.plot(years, nArray, color = 'k', linewidth = 4)
        P.show()  
                  
               

class SimStoatDynXXX(object):
    def __init__(self):
        """
        stoat dynamics in 1 km2 block
        """
        self.N = np.linspace(1, 12, 6, dtype=int)
        self.nYears = 10
        self.recruitDecay = 0.6      #1.0         #2.0
        self.perCapRec = 2.5             #.8
        self.S = np.arange(1, 12)

#        self.pMaxRec = np.exp(-self.B**2 / self.recruitDecay**2)
#        self.pMaxRec = np.exp(-self.recruitDecay * self.B**self.recruitSuper)
#        self.pMaxRec = np.exp(-self.recruitDecay * self.B**2 / self.K**2)
        self.adultSurv = np.exp(-0.8)        # constant annual survival rate (exponential model)
        self.adultSurvDecay = .8        #.1
#        self.findK()


        self.plotWrapFX()
#        self.plotAdultSurv()
#        self.plotProbRec()
#        self.simPop()

    def plotWrapFX(self):
        self.R = np.linspace(100, 3000, 100)
        self.Rat = 500.
        self.nStoats = np.arange(1,14, 0.1)
        self.getAdultSurvRecruit()
        self.getNeq()        
        self.plotStoats()

    def getAdultSurvRecruit(self):
        pMaxSurv = (np.exp(-self.nStoats**2 / self.Rat**self.adultSurvDecay))
        self.stoatSur_R500 = self.adultSurv * pMaxSurv
        self.pMaxRec = np.exp(-self.nStoats**2 / self.Rat**self.recruitDecay)
        self.recRate_R500 = self.pMaxRec * self.perCapRec 

    def getNeq(self):
        nR = len(self.R)
        self.nEq = np.zeros(nR)
        for i in range(nR):
            eq_i = 2.0
            rat_i = self.R[i]
            # loop years
            for j in range(15):
                pSurv = (np.exp(-eq_i**2 / rat_i**self.adultSurvDecay)) * self.adultSurv
                NStar = eq_i * pSurv
                recRate = np.exp(-NStar**2 / rat_i**self.recruitDecay) * self.perCapRec
                eq_i = NStar * (1.0 + recRate)
            self.nEq[i] = eq_i

    def plotStoats(self):
        P.figure(figsize=(16,10))
        P.subplot(2,2,1)
        P.subplots_adjust(hspace = 0.4, wspace = 0.2) 
        P.plot(self.nStoats, self.stoatSur_R500, color='k', linewidth = 4)
        P.xlabel('Stoat Density (' + '$km^{-2}$)', fontsize = 14)
        P.ylabel('Annual adult survivorship', fontsize = 14)
        P.title('(A)', loc='left')
        P.subplot(2,2,2)
        P.plot(self.nStoats, self.recRate_R500, color='k', linewidth = 4)
        P.xlabel('Stoat Density (' + '$km^{-2}$)', fontsize = 14)
        P.ylabel('Per-capita recruitment rate', fontsize = 14)
        P.title('(B)', loc='left')
        P.subplot(2,2,3)
        P.plot(self.R, self.nEq, color = 'k', linewidth = 4)
        P.xlabel('Rat Density (' + '$km^{-2}$)', fontsize = 14)
        P.ylabel('Stoat equilibrium density' + ' ($km^{-2}$)', fontsize = 14)
        P.title('(C)', loc='left')

        P.subplot(2,2,4)
        self.plotSimPop()
        FName = 'stoatDynamics.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()

    def plotSimPop(self):
        nStoats = np.arange(1,14)


        self.nCells = len(nStoats)
        nYears = 12
        self.pop = np.zeros((nYears, self.nCells))
        n = nStoats.copy()
        for i in range(nYears):
            ## sim cells
            pSurv = (np.exp(-n**2 / self.Rat**self.adultSurvDecay)) * self.adultSurv
            NStar = n * pSurv
            recRate = np.exp(-NStar**2 / self.Rat**self.recruitDecay) * self.perCapRec
            n = NStar * (1.0 + recRate)
            self.pop[i] = n
        print('last population', self.pop[-1])
        years = np.arange(1, nYears + 1)
        for i in range(self.nCells):
            P.plot(years, self.pop[:, i], color = 'k', linewidth = 2)
        P.xlabel('Years', fontsize = 14)
        P.ylabel('Stoat density (' + '$km^{-2}$)', fontsize = 14)
        P.title('(D)', loc='left')


    def plotAdultSurv(self):
        n = np.arange(1, 90.)
        pMaxSurv = (np.exp(-n**2 / self.KArray[0]**self.adultSurvDecay))
        sur = self.adultSurv * pMaxSurv
        P.figure(figsize=(11,9))
        P.subplot(2,2,1)
        P.plot(n, sur, color = 'k', linewidth = 4)
        P.title('Seed Density = ' + str(self.KArray[0]) + ' $m^{-2}$')
        P.xlabel('Rat density')
        P.ylabel('Prop. adult surv')

        n = np.arange(1, 25)
        pMaxSurv = (np.exp(-n**2 / self.KArray[2]**self.adultSurvDecay))
        sur = self.adultSurv * pMaxSurv
        P.subplot(2,2,2)
        P.plot(n, sur, color = 'k', linewidth = 4)
        P.title('Seed Density = ' + str(self.KArray[2]) + ' $m^{-2}$')
        P.xlabel('Rat density')
        P.ylabel('Prop. adult surv')

        n = np.arange(1, 12)
        pMaxSurv = (np.exp(-n**2 / self.KArray[-1]**self.adultSurvDecay))
        sur = self.adultSurv * pMaxSurv
        P.subplot(2,2,3)
        P.plot(n, sur, color = 'k', linewidth = 4)
        P.title('Seed Density = ' + str(self.KArray[-1]) + ' $m^{-2}$')
        P.xlabel('Rat density')
        P.ylabel('Prop. adult surv')

        P.subplot(2,2,4)
        years = np.arange(1,6)
        prpSurv = self.adultSurv**years
        print('daily survivorship', sur, 'prpsurv', prpSurv, 'years', years)
#        years = (age).astype(int)
        P.plot(years, prpSurv, color = 'k', linewidth = 4.0)
        P.title('Ideal conditions')
        P.xlabel('Age (years)')
        P.ylabel('Mean age specific survival')

        FName = 'prpAdultSurviving.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()

    def plotProbRec(self):
        P.figure(figsize = (11,9))
        R = np.arange(1, 90.0)
        self.pMaxRec = np.exp(-R**2 / self.KArray[0]**self.recruitDecay)
        P.subplot(2,2,1)
        P.plot(R, self.pMaxRec * self.perCapRec, color = 'k', linewidth = 4.0)
        P.xlabel('Rat Density')
        P.ylabel('Per Cap Recruit')
        P.title('Seed density = ' + str(self.KArray[0]) + ' $m^{-2}$')

        P.subplot(2,2,2)
        R = np.arange(1, 35.0)
        self.pMaxRec = np.exp(-R**2 / self.KArray[1]**self.recruitDecay)
        P.plot(R, self.pMaxRec * self.perCapRec, color = 'k', linewidth = 4.0)
        P.xlabel('Rat Density')
        P.ylabel('Per Cap Recruit')
        P.title('Seed density = ' + str(self.KArray[1]) + ' $m^{-2}$')

        P.subplot(2,2,3)
        R = np.arange(1, 22.0)
        self.pMaxRec = np.exp(-R**2 / self.KArray[3]**self.recruitDecay)
        P.plot(R, self.pMaxRec * self.perCapRec, color = 'k', linewidth = 4.0)
        P.xlabel('Rat Density')
        P.ylabel('Per Cap Recruit')
        P.title('Seed density = ' + str(self.KArray[3]) + ' $m^{-2}$')

        P.subplot(2,2,4)
        R = np.arange(1, 12.0)
        self.pMaxRec = np.exp(-R**2 / self.KArray[-1]**self.recruitDecay)
        P.plot(R, self.pMaxRec * self.perCapRec, color = 'k', linewidth = 4.0)
        P.xlabel('Rat Density')
        P.ylabel('Per Cap Recruit')
        P.title('Seed density = ' + str(self.KArray[-1]) + ' $m^{-2}$')

        FName = 'probabilityRecruit.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()


#@jit
def numbaLoop(a, c, n):
    for i in range(n):
        a[i] = i**2
        print(i, a[i])
    for j in range(2):
        for k in range(3):
            print('j', j, 'k', c[j][k] * 1, np.prod(c[j][k]))
    nx = [20, 40, 60]
    for k in range(3):
        ch = np.int(nx[k] * .5)
        nr = np.random.choice(nx[k], ch)
        print('nr', nr)


class NumbaTest(object):
    def __init__(self):
        """
        test numba functionality in class
        """
        self.n = 25
        self.a = np.zeros(self.n)
        b = [[1,2,3,4], [5,6,7], [8,9]], [[-1,-2,-3,-4], [-5,-6,-7], [-8,-9]]
        c = np.array(b)
        numbaLoop(self.a, c, self.n)
        print('self.a', self.a)








#def findK_General(20, .5, 1.05, 1, .97):
#    ## (n, S, sDecay, R, rDecay)
#    S = np.exp(-S)
#    for i in range(1,50):
#        NStar = n * S * (np.exp(-n**2 / sDecay**2))
#        recruits = NStar * R * np.exp(-NStar**2 / rDecay**2)
#        N = NStar + recruits
#        print('i', i, 'N', N)
#    K = np.round(n, 2)
#    print('K', K)




def errorCost():
    cost = np.array([100, 50, 25, 12.5, 10])
    perr = np.array([.025, .05, .1, .2, .25])
    n = 200
    nErr = len(perr)
    realCost = np.zeros(nErr)
    for i in range(nErr):
        for j in range(n):
            infect = np.random.binomial(1, perr[i])
            while infect == 1:
                realCost[i] += cost[i]
                infect = np.random.binomial(1, perr[i])
        print('err rate =', perr[i], 'real cost =', realCost[i] / n)


def getK(t, surv, survDecay, N, seed, maxRec, recDecay):
    for i in range(t):
        surv_i = surv * (np.exp(-N**2 / 
                seed**survDecay))
        NStar = N * surv_i
        pMaxRec = np.exp(-NStar**2 / seed**recDecay)
        recRate = maxRec * pMaxRec
        nRecruit = NStar * recRate
        N = (1 + recRate) * NStar
        print('t', i, 'N', N)

def getKiwiK():
    t = 15
    prp = .25
    surv = .9
    survDecay = 110
    N = 10
    maxRec = .6
    recDecay = 14.5

    for i in range(t):
        surv_i = surv * (np.exp(-N**2 / survDecay**2))
        NStar = N * surv_i
        pMaxRec = np.exp(-NStar**2 / recDecay**2)
        recRate = maxRec * pMaxRec
        nRecruit = NStar * recRate
        N = (1 + recRate) * NStar
        print('t', i, 'N', N)

    N = 5
    for i in range(t):
        surv_i = surv * (np.exp(-N**2 / (survDecay*prp)**2))
        NStar = N * surv_i
        pMaxRec = np.exp(-NStar**2 / (recDecay*prp)**2)
        recRate = maxRec * pMaxRec
        nRecruit = NStar * recRate
        N = (1 + recRate) * NStar
        print('t', i, 'N', N, 'N/prp', N/prp)


def incidence():
    r = 0.27725887
    n0 = 166
    n = 209
    t = 1.0
    poss = np.arange(170, 250, dtype = int)
    mu = n0 * np.exp(r * t)
    poisPMF = stats.poisson.logpmf(poss, mu)
    pmfObs = poisPMF[poss == n]
    maxLL = np.max(poisPMF)
    nMaxPMF = poss[poisPMF == maxLL][0]
    print('nMaxPMF', nMaxPMF)
    P.figure()

#    ax1 = P.gca()


    P.plot(poss, poisPMF, color = 'k', linewidth = 3, 
            label = '$n=n_0e^{(rt)}$' )
    P.vlines(x = n, ymin=np.min(poisPMF), ymax = pmfObs, 
            linestyles = 'dashed', colors='k', label = 'LL when n = ' + str(n))
    P.vlines(x = nMaxPMF, ymin=np.min(poisPMF), ymax = maxLL, 
            linestyles = 'dashed', colors='r', 
            label = 'Most likely n = ' + str(nMaxPMF))
#    lns = lns0 + lns1
#    labs = [l.get_label() for l in lns0]
    P.legend(loc = 'upper left')
    P.xlabel('Possible cases')
    P.ylabel('Log likelihood')
    FName = 'CovidLL_25March.png'
    P.savefig(FName, format='png', dpi = 500)
    P.show()


def testNoneFX(a, b, c = None, e = None):
    if c is None:
        d = a + b
    else:
        d = (a+b)/c
        d *= e
    return(d)


def covidSim():
    days = np.arange(1, 15)
    n = len(days)
    se = .20
    prior = .65
    poa = np.zeros(n)
    for i in range(n):
        p_i = prior / (1.0 - se * ( 1. - prior))
        prior = p_i
        poa[i] = p_i
        print(p_i)

    P.figure()
    P.plot(days, poa, linewidth = 3, color='k', label='Probability of absence')
    P.hlines(y = 0.95, xmin = 0, xmax = 14, linestyles = 'dashed', color='k',
            label = 'Target PoA')
    P.xlabel('Days')
    P.ylabel('Probability of absence')
    P.legend(loc='lower right')
    P.xlim(1, 14)
    FName = 'PoAExample.png'
    P.savefig(FName, format='png', dpi = 500)
    P.show()

def percentChange():
    annP = 0.05
    pop0 = 10
    pop = pop0
    n = 15
    popArr = np.zeros(n)
    percGrow = np.zeros(n)
    for i in range(n):
        popInc = pop * annP
        popArr[i] = pop + popInc
        percGrow[i] = (popArr[i] - pop) / pop
        pop = popArr[i]
        print('i', i, 'percGrow', percGrow[i], 'NewPop', pop)
    print('totalPChange', (pop - pop0) / pop0, 'finalPop', pop)



def PoA_Abundance(Y, n, se, targetProb):
    NArray = np.arange(n, Y+1)
    pPredArray = np.zeros(Y - n + 1)
    productCompliment = 1.0
    cc = 0
    for i in range(n, Y+1):
        p_i = (1 - (np.exp(gammaln(i+1) - gammaln(n+1) - gammaln(i - n + 1)) *
            (se**n) * ((1-se)**(i - n))))
        if cc == 0:
            pPredArray[cc] = 1.0 - p_i
            productCompliment = p_i
        else:
            productCompliment *= p_i
            pPredArray[cc] = 1.0 - productCompliment
        print('P*', i, 'probPred', pPredArray[cc])
        cc += 1
    P.Figure()
    P.plot(NArray, pPredArray, linewidth = 3, color = 'k')
    P.hlines(y = targetProb, xmin = n, xmax = NArray[-1], 
        linestyles = 'dashed', color='k')  
    P.xlabel('Population abundance', fontsize = 16)
    P.ylabel('$P(N \leq Y|n)$', fontsize = 16)
    P.savefig("probPopLessThan.png", format='png', dpi = 500)
    P.show()

def AR_FX():
    wkArr = np.arange(156, dtype = int)
    wk = np.tile(np.arange(52), 3)
    radWk = 2.0 * wk * np.pi / 52.0
    sinWk = np.sin(radWk)
    cosWk = np.cos(radWk)
    dataMask = np.arange(156) >= 25
    nDat = np.sum(dataMask)
    wkDat = wkArr[dataMask]
    lnD0 = np.log(2.0)
#    mast = np.zeros(156, dtype = bool)
#    mast[11] = True
    wkSinceMast = np.zeros(156)
    for i in range(12, 156):
        wkSinceMast[i] = i - 11

    th = 1.2
    foc = 3.62 
    betas = np.array([-.12, .07, -.02, 1.8])
    radDat = radWk[dataMask]
    dwrpC = dwrpcauchy(radDat, th, foc) 
    xDat = np.ones((nDat, 4))
    xDat[:, 1] = np.log(wkSinceMast[dataMask])
    xDat[:, 2] = (np.log(wkSinceMast[dataMask]))**2
    xDat[:, 3] = dwrpC
    lnr = np.dot(xDat, betas)

    P.figure()
    P.subplot(1,2,1)
    P.plot(wkDat, lnr)
    P.subplot(1,2,2)
    P.plot(wkDat, dwrpC)
    P.show()
    print('rad', radWk, 'mast', mast, 'wkmast', wkSinceMast)


def simAlabastor():
    wk = np.tile(np.arange(1,53),3)
    wkMast = np.arange(1,157)
    para = np.array([-0.2, 7.5, 0.06, -0.02])
    sinCosPara = np.array([-.04, .01])
    r = 0.11
    tmPara = np.array([10.0, 4.0])
    lnD0 = np.log(1.2)
    lnD = lnD0
    ndat = len(wk)
    dArr = np.zeros((ndat, 3))
    mastArr = np.zeros(ndat)
    sinCosArr = np.zeros(ndat)
    sinCosArr2 = np.zeros(ndat)
    
    for i in range(ndat):
        sin_i = para[-2] * np.sin(wk[i] * 2.0 * np.pi / 52.0)
        cos_i = para[-1] * np.cos(wk[i] * 2.0 * np.pi / 52.0)
        tm_i = para[-3] * gamma_pdf(wkMast[i], tmPara[0], tmPara[1])
        b_i = para[0] + tm_i + sin_i + cos_i
        lnD = lnD * (1.0 + b_i) + r
        dArr[i, 0] = lnD
        mastArr[i] = tm_i
        sinCosArr[i] = sin_i + cos_i
        ## second sin cos graph
        sin2_i = sinCosPara[-2] * np.sin(wk[i] * 2.0 * np.pi / 52.0)
        cos2_i = sinCosPara[-1] * np.cos(wk[i] * 2.0 * np.pi / 52.0)
        sinCosArr2[i] = sin2_i + cos2_i
    P.figure()
    P.subplot(2,2,1)
    P.plot(wkMast, np.exp(dArr))
    P.subplot(2,2,2)
    P.plot(wkMast, mastArr)
    P.subplot(2,2,3)
    P.plot(wkMast, sinCosArr)
    P.subplot(2,2,4)
    P.plot(wkMast, sinCosArr2)

    P.show()

def simAR2():
    t = 38
    c = 0.9
    a = .6
    b = -.12
    print('condition less than 0:', (a**2 + (4.0 * b)) < 0)
    print('Period', (2.0 * np.pi / np.arccos(a / 2.0 / np.sqrt(-b))))
    print('Equilibrium', np.exp(c / (1 - a - b)))
    timeArr = np.arange(t)
    nArray = np.zeros(t)
    nArray[:2] = np.log(np.array([5.5, 5.6]))
    for i in range(2, t):
        err = np.random.normal(0, .0000017)
        nArray[i] = c + (a * nArray[i-1]) + (b * nArray[i-2]) + err
    P.figure()
    P.plot(timeArr, np.exp(nArray))
    P.show()        


def genGammaTest():
    x = np.arange(1, 105, 1)
    a = [130.0, 130.0, 130.0, 130.0]
    d = 1
    p = [3., 5.0, 7, 9]
#    ggPDF = genGammPDF(x, a, d, p)
#    print('ggPDF', ggPDF)
    col = ['k', 'b', 'r', 'g']
    P.figure()
    P.subplot(1,2,1)
    for i in range(4):
        ggpdf = genGammPDF(x, a[i], d, p[i])
#        P.subplot(2,2,(i+1))    
        P.plot(x, ggpdf, color = col[i])
    P.xlabel('Months after mast (March)')
    P.ylabel('Density dependence effect')
    P.subplot(1,2,2)
    for i in range(4):
        ggpdf = gengamma.pdf(x, a[i], p[i]) * 4.5 - .4
        P.plot(x, ggpdf, color = col[i])
    P.xlabel('Months after mast (March)')
    P.ylabel('Density dependence effect')
    
    P.savefig("generalizedGamma.png", format='png', dpi = 100)
    P.show()

def ratGenGamma():
    x = 1
    maxDen = 18.0
    x = np.arange(1, 26, .1)
    a = [16, 10, 12, 13]
    d = 1.7
    p = [3., 6.0, 5.0, 12.0]
#    ggPDF = genGammPDF(x, a, d, p)
#    print('ggPDF', ggPDF)
    col = ['k', 'b', 'r', 'g']
    P.figure()
    ggpdf = genGammPDF(x, a[0], d, p[0])
    gg = (ggpdf / np.max(ggpdf)) * maxDen

    tarPop = ((maxDen - gg[0]) * 0.25) + gg[0]
    print('target pop', tarPop)
#    for i in range(4):
#        ggpdf = genGammPDF(x, a[i], d, p[i]) * 4.5 - .4
    P.plot(x, gg, color = col[0], linewidth = 3)
    P.xlabel('Months after mast (March)', fontsize = 14)
    P.ylabel('Rat Density ($ha^{-1}$)', fontsize = 14)
    P.savefig("ratDenMast.png", format='png', dpi = 100)
    P.show()



def simRatGenGamma():
    ## ARRAY OF WEEKS SINCE MAST
    wkMast = np.arange(1,105)
    ## MEAN DENSITY DENPENDENCE PARAMETER AND TIME SINCE MAST COVARIATE
    para = np.array([-.15, 780.0])
    ## MAXIMUM POPULATION GROWTH RATE
    r = 0.1 #0.25
    ## PARAMETERS FOR THE GENERALISED GAMMA DISTRIBUTION FOR TIME SINCE MAST
    tmPara = np.array([135.0, 1.0, 5.0])
#    tmPara = np.array([120.0, 1.0, 3.5])
    ## ELEVATION EFFECT FOR LOW, MID AND HIGH
    elevEff = np.array([0.0, .003, -.007 ])
    ## INITIAL DENSITY OF RATS PER HA FOR HIGH, MID AND LOW ELEVATION
    ## ADJUSTED TO RATS PER KM SQUARED FOR CALCULATIONS  
    lnD0 = np.log(np.array([(6.0*100), (8.0 * 100), (0.5 * 100)]))  #.00001)
    lnD = lnD0.copy()
    ndat = len(wkMast)
    dArr = np.zeros((ndat, 3))
    bArr = np.zeros((ndat, 3))
    mastArr = np.zeros(ndat)
    for j in range(3):
        for i in range(ndat):
            tm_i = genGammPDF(wkMast[i], tmPara[0], tmPara[1], tmPara[2])
            b_i = para[0] + (para[-1]*tm_i) + elevEff[j]
            lnD_j = (lnD[j] * (1.0 + b_i)) + r
            dArr[i, j] = lnD_j
            bArr[i, j] = b_i
            if j == 0:
                mastArr[i] = tm_i
#            if i < 40:
#                print('site', j, 'wk', i, 'D_t-1', np.round(np.exp(lnD[j]),4), 
#                    'den', np.round(np.exp(lnD_j),4), 
#                    'b', np.round(b_i,4), 
#                    'tm_i', np.round(tm_i,3))
            lnD[j] = lnD_j
    P.figure(figsize=(13,9))
    P.subplot(2,1,1)
    popDen = np.exp(dArr) / 100.0
    P.plot(wkMast, popDen[:,0], color = 'k', label='Low')
    P.plot(wkMast, popDen[:,1], color = 'b', label='Mid')
    P.plot(wkMast, popDen[:,2], color = 'r', label='High')
    P.vlines(x = 15, ymin = np.min(popDen[:,2]), ymax = np.max(popDen[:,1]), color = 'k',
        linestyle = 'dashed')
    P.ylabel('Rat density ($ha^{-1}$)')
    P.xlabel('Weeks since mast')
    P.legend(loc= 'upper right')
    P.subplot(2,2,3)
    P.plot(wkMast, mastArr)
    P.ylabel('Mast effect (GM PDF)')
    P.subplot(2,2,4)
    expBArr = np.exp(bArr)
    P.plot(wkMast, expBArr[:,0], color = 'k', label='Low')
    P.plot(wkMast, expBArr[:,1], color = 'b', label='Mid')
    P.plot(wkMast, expBArr[:,2], color = 'r', label='High')
    P.legend(loc= 'upper right')
    P.ylabel('Density dependence')
    P.show()


def simGompertz():
    a = .889362
    b = -0.19
    c = b + 1
    k = a /(1-c)
    
    x0 = np.log(50)
    x = x0
    t = 50
    tt = np.arange(1, t+1)
    xArr = np.zeros(t)
    for i in range(t):
        x = a + x * (1.0 + b)
#        print('i', i, 'x', x)
        xArr[i] = x
    P.figure()
    P.plot(tt, np.exp(xArr))
    P.hlines(y = np.exp(k), xmin = np.min(tt), xmax = np.max(tt), linestyle='dashed')
    c = b+1
    K = a/(1 - c)
    print('c', c, 'K', K, 'last x', xArr[-1]) 
    P.show()


def explorePopGrowth():
    t = 9
    n0 = 77
    lam = 1.5
    r = np.log(lam)
    Nt = n0 * np.exp(r * t)
    n = n0
    for i in range(t):
        n = n*lam
        print('n', n)
    print('Nt', Nt)


def listTest():
    a = []
    a.append(np.arange(4))
    a.append(np.arange(14, 20))
    a.append(np.arange(33, 37))
    print('a before', a)
    b = a[-1]
    listFX(b)
    print('a after', a)
    

def listFX(b):
    n = len(b)
    for i in range(n):
        b[i] = b[i] * 2


########            Main function
#######
def main():

    print('Run Main')


    G0Test()
#    g0SigmaTest()
    BetaPlot()
#    findBetaPert(.4, .5, .6, shape=4)
    betaBinomFX()

    pIntro()

#    gamtest = gammaTest()
#    invGam = invGamma()
#    wrpCTest()
#    normalBeta()
#    normalFX()
#    CDFTest()
#    Poisson()
#    halfNorm()
#    multiVNorm()
#    circHR()
#    TruncNormFX()
#    TruncNormIR()
#    WeibullPCapt()
#    Rescale()
#    encounterFX()
#    Emigration()
#    EmigrationSurvive()
#    EmigrationStoat()
#    EmigrationKiwi()
#    errorCost()

#    listTest()

#    d = testNoneFX(a = 2, b = 3, c = 5, e = 2)
#    print(d)

#    AlleeEffect()

#    FunctionalResp()
#    genLogisticFX()
#    KiwiGrowth()
#    CVTest()
#    PredationSurvival()
#    RickerGrowth()
#    AlleeEffect()
#    binomTest()
#    SurvivorPrp()
#    percentChange()

#    LatHypCubeFX()
#    PoA_Abundance(Y=15, n=2, se=0.33, targetProb=0.95)

#    simAlabastor()
#    simAR2()

###    genGammaTest()
#    ratGenGamma()
#    simGompertz()
###    simRatGenGamma()

#    explorePopGrowth()

########    SimKiwiDyn()

    # getK(t, surv, survDecay, N, seed, maxRec, recDecay)
#    getK(20, 0.5, 1.05, 1, 75, 2.5, 0.97)
#    getKiwiK()
 
#    SimKiwiDyn2()
#    SimRatDyn()
#    SimStoatDyn()
#    SimStoatDynXXX()
#    findK_General(XX, S, sDecay, R, rDecay)

#    incidence()
#    covidSim()
#    NumbaTest()

#    AR_FX()

if __name__ == '__main__':
    main()

