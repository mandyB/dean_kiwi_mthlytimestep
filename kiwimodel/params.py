import numpy as np

class KiwiParams(object):
    """
    The parameters to the Kiwi model
    """
    def __init__(self):
        self.extentShp = None
        self.kClasses = None
        self.islands = None
        self.islandK = 1.3
        self.DEM = None
        # resolution for rodent, stoats, and kiwi, respectively.
        self.resolutions = (200.0, 1000.0, 1000.0)
        self.controlFile = None
        self.controlPathPrefix = None
        self.years = []
        self.burnin = 0
        self.iter = 3

        self.mastCellParams = (0.001, 1000.0)
        self.mastWindowSize = self.resolutions[0] * 150 # in metres
        self.mastRho = 16000.0
        self.mastPrEvent = 1.0 / 5.1  # p(mast) = 1 out of 5.1 years
        self.mastProportionParams = findBeta(0.5, 0.4)
        self.mastSpatialSD = 2.1  

        self.islandK = 2.0
        self.initialRodentN = 10
        self.rodentLambda = 2.0 # rmax
        self.rodentSurv = 0.61
        self.rodentSurvDecay = 1.1
        self.rodentRecDecay = 0.97

        self.prpGrowRateControl = 1.0  # proportion of rodent growth before control is applied
        self.rodentProbEatBait = 0.7 # pT

        self.pRodentPres = 0.95
        self.rodentInitialMultiplier = 0.8        
        
        ######## TRACKING TUNNEL PARAMETERS
        self.threshold_TT = 1.0             # Thres prop of TT with detections
#        self.prpGrowAssessRodent = 1.0      # prop of rat growth before apply control
        self.g0_TT = 0.02                   # Tracking tunnel g0
        self.sigma_TT = 22.0                # Rat sigma
        self.nights_TT = 4                  # Tracking tunnel nights
        self.nTunnels = 120                 # number of tracking tunnels


        self.gammaProbEmigrate = np.array([0.1, 0.2, 0.1])   # gamma for rodent, stoats, 
                                                    # Eqn 18, 20 and others 
        self.deltaImmigrate = np.array([0.8, 0.4, 0.6])    # delta (rodents, stoats, kiwis)         
                                                        # Eqn 20 and others
        self.tauImmigrate = np.array([0.001, 0.25, 0.0])      # Eqn 18, 20 and others
                                                 # rate parameter Imm (rodent, stoat, kiwi)
        self.emigrationWindowSize = (self.resolutions[0] * 13, 
                    self.resolutions[1] * 20, self.resolutions[2] * 11) # in metres

        self.rodentMaxAltitude = 1000.0  # metres
        self.kiwiMaxAltitude = 1340.0  # metres
        self.stoatMaxAltitude = 1100.0
        ############
        ############    Stoat parameters
    
        self.stoatLambda = 1.2  #4.0
        self.stoatRecDecay = 1.0
        self.stoatSurv = 0.50
        self.stoatSurvDecay = 1.2

        self.stoatPopSD = 0.22
        self.pEncToxic = 0.004          # operates at stoat scale
        self.pEatEncToxic = 0.8          # operates at stoat scale
        self.stoatInitialMultiplier = .85
        self.pStoatPres = 0.75
        self.initialStoatN = 3.0
        ############
        ############    Kiwi parameters

        self.kiwiK = 20.0
        self.pKiwiPres = 0.68
        self.initialkiwiN = 5.0
        self.kiwiInitialMultiplier = 0.5
        self.kiwiPsi = 2.0  # Eqn 32
        self.competEffect = 0.0
        self.kiwiPopSD = .12

        self.kiwiSurv = 0.90
        self.kiwiSurvDecay = 110.0
        self.kiwiLambda = 0.6
        self.kiwiRecDecay = 14.5
        
        # model for control that is reactive to masting
        self.reactivePropMgmtMasting = 0.5 # set > 0 to enable

        ## MODEL FOR CONTROL REACTIVE TO RODENT TRACKING TUNNEL RATE
        
        ## NUMBER OF YEARS OVER WHICH CALC KIWI ANN GROWTH RATE
        self.annGrowthYears = 5


    def setExtentShapeFile(self, shpFile):
        self.extentShp = shpFile

###    def setKClasses(self, preBurninK, postBurninK):
###        self.kClassPreBurnin = preBurninK
###        self.kClassPostBurnin = postBurninK


    def setKClasses(self, kClasses):
        self.kClasses = kClasses

    def setIslands(self, islands):
        self.islands = islands

    def setIslandK(self, islandK):
        self.islandK = islandK

    def setDEM(self, dem):
        self.DEM = dem

    def setResolutions(self, resolutions):
        """
        Resolutions for rodent, stoats, and kiwi, respectively.
        """
        self.resolutions = resolutions

    def setControlFile(self, controlFile):
        self.controlFile = controlFile

    def setControlPathPrefix(self, prefix):
        self.controlPathPrefix = prefix

    def setBurnin(self, burnin):
        self.burnin = burnin

    def setYears(self, years):
        self.years = years

    def setIterations(self, iter):
        self.iter = iter

    def setMastCellParams(self, a, b):
        self.mastCellParams = (a, b)

    def setMastWindowSize(self, winsize):
        "in metres"
        self.mastWindowSize = winsize

    def setMastRho(self, rho):
        self.mastRho = rho

    def setMastSpatialSD(self, mastSpatialSD):
        """
        ## Spatial masting
        """
        self.mastSpatialSD = mastSpatialSD 

    def setMastPrEvent(self, pr):
        self.mastPrEvent = pr

    def setMastProportionParams(self, mu, sdev):
        self.mastProportionParams = findBeta(mu, sdev)

    def setPRodentPresence(self, pRodentPres):
        self.pRodentPres = pRodentPres

    def setRodentInitialMultiplier(self, rodentInitialMultiplier):
        self.rodentInitialMultiplier = rodentInitialMultiplier

    def setRodentGrowthRate(self, rate):
        self.rodentGrowthRate = rate

    def setPrpGrowRateControl(self, prpGrowRateControl):
        self.prpGrowRateControl = prpGrowRateControl

    def setRodentProbEatBait(self, probEat):
        self.rodentProbEatBait = probEat

    def setRodentEmigrationWindowSize(self, winsize):
        "in metres"
        self.rodentEmigrationWindowSize = winsize

    ### NEW FUNCTIONS FOR RODENT GROWTH
    def setRodentT(self, rodentT):
        self.rodentT = rodentT

    def setRodentSurv(self, rodentSurv):
        self.rodentSurv = rodentSurv

    def setRodentSurvDecay(self, rodentSurvDecay):
        self.rodentSurvDecay = rodentSurvDecay

    def setInitialRodentN(self, initialRodentN):
        self.initialRodentN = initialRodentN

    def setRodentLambda(self, rodentLambda):
        self.rodentLambda = rodentLambda

    def setRodentRecDecay(self, rodentRecDecay):
        self.rodentRecDecay = rodentRecDecay 









    def setThreshold_TT(self, threshold_TT):
        """
        Threshold proportion of tracking tunnels with detections
        """
        self.threshold_TT = threshold_TT

    def setG0_TT(self, g0_TT):
        """
        Tracking tunnel g0
        """
        self.g0_TT = g0_TT

    def setSigma_TT(self, sigma_TT):
        """
        rodent Sigma
        """
        self.sigma_TT = sigma_TT

    def setNights_TT(self, nights_TT):
        """
        Tracking tunnel nights
        """
        self.nights_TT = nights_TT

    def setNTunnels(self, nTunnels):
        """
        number of tracking tunnels
        """
        self.nTunnels = nTunnels


    ### STOAT FUNCTIONS AND PARAMETERS
    def setPStoatPresence(self, pStoatPres):
        self.pStoatPres = pStoatPres

    def setStoatInitialMultiplier(self, stoatInitialMultiplier):
        self.stoatInitialMultiplier = stoatInitialMultiplier

    def setStoatGrowthRate(self, stoatGrowthRate):
        self.stoatGrowthRate = stoatGrowthRate

    def setStoatPopSD(self, stoatPopSD):
        self.stoatPopSD = stoatPopSD

    def setStoatKAsymptotes(self, loAsym, hiAsym):
        self.kAsymptotes = (loAsym, hiAsym)

    def setPEncToxic(self, pEncToxic):
        self.pEncToxic = pEncToxic

    def setPEatEncToxic(self, pEatEncToxic):
        self.pEatEncToxic = pEatEncToxic

    ### NEW FUNCTIONS FOR STOAT GROWTH
    def setStoatT(self, stoatT):
        self.stoatT = stoatT

    def setStoatSurv(self, stoatSurv):
        self.stoatSurv = stoatSurv

    def setStoatSurvDecay(self, stoatSurvDecay):
        self.stoatSurvDecay = stoatSurvDecay

    def setInitialStoatN(self, initialStoatN):
        self.initialStoatN = initialStoatN

    def setStoatLambda(self, stoatLambda):
        self.stoatLambda = stoatLambda

    def setStoatRecDecay(self, stoatRecDecay):
        self.stoatRecDecay = stoatRecDecay 









    ### KIWI FUNCTIONS AND PARAMETERS
    def setPKiwiPresence(self, pKiwiPres):
        self.pKiwiPres = pKiwiPres

    def setKiwiInitialMultiplier(self, kiwiInitialMultiplier):
        self.kiwiInitialMultiplier = kiwiInitialMultiplier

    def setKiwiPsi(self, kiwiPsi):
        self.kiwiPsi = kiwiPsi

#    def setKiwiGrowthRate(self, kiwiGrowthRate):
#        self.kiwiGrowthRate = kiwiGrowthRate

    def setKiwiK(self, kiwiK):
        self.kiwiK = kiwiK

    def setKiwiPopSD(self, kiwiPopSD):
        self.kiwiPopSD = kiwiPopSD

#    def setMinPredationSurv(self, minPredationSurv):
#        self.minPredationSurv = minPredationSurv

    def setCompetitionEffect(self, competEffect):
        self.competEffect = competEffect

    ### NEW FUNCTIONS FOR KIWI GROWTH
    def setKiwiSurv(self, kiwiSurv):
        self.kiwiSurv = kiwiSurv

    def setKiwiSurvDecay(self, kiwiSurvDecay):
        self.kiwiSurvDecay = kiwiSurvDecay

    def setInitialKiwiN(self, initialkiwiN):
        self.initialkiwiN = initialkiwiN

    def setKiwiLambda(self, kiwiLambda):
        self.kiwiLambda = kiwiLambda

    def setKiwiRecDecay(self, kiwiRecDecay):
        self.kiwiRecDecay = kiwiRecDecay 



    def setReactiveMode(self, prop):
        """
        Put into reactive mode by specifying the proportion
        of pixels in a control area that mast before the whole
        control area is controlled in a given year.
        """
        self.reactivePropMgmtMasting = prop

def findBeta(mu, sdev):
    """
    Find a and b of a Beta distribution given mean and standard deviation
    """
    sdevsq = sdev * sdev;
    a = mu * ( ( mu * (1.0 - mu) ) / sdevsq - 1.0)
    b = ( 1.0 - mu ) * ( ( mu * ( 1.0 - mu ) ) / sdevsq - 1.0)

    return a, b
