#!/usr/bin/env python

import os
from scipy import stats
from scipy.special import gammaln
from scipy.special import gamma
import numpy as np
from numba import jit
import pickle
import datetime
import basicsModule



class MCMC(object):
    def __init__(self, params, predatordata, basicdata, checkingdata=None):

        self.basicdata = basicdata
        self.predatordata = predatordata
        self.params = params
        ## If use checking data
        if checkingdata is None:
            # storage arrays for parameters
            self.startingStep = 0
            self.bgibbs = np.zeros([self.params.ngibbs, self.params.nbcov])             # beta parameters
            self.rgibbs = np.zeros(self.params.ngibbs)                                  # pop growth
            self.rparagibbs = np.zeros([self.params.ngibbs, 2])                         # wrapped cauchy para for recruitment 
            self.igibbs = np.zeros(self.params.ngibbs)                                  # immigration
            self.siggibbs = np.zeros(self.params.ngibbs)                                # home range decay
            self.g0gibbs = np.zeros([self.params.ngibbs, self.basicdata.nTrapBait])     # beta parameters
            self.Ngibbs = np.zeros([self.params.ngibbs, self.basicdata.nsession])       
            self.g0Multigibbs = np.zeros([self.params.ngibbs, 3])                       # seasonal g0 multiplier 
            self.initialRepPopgibbs = np.zeros(self.params.ngibbs)
            ### g0 update sampling arrays
            self.g0_sample0 = np.arange(self.basicdata.nTrapBait, step=2)
            self.g0_sample1 = np.arange(1, self.basicdata.nTrapBait, step=2)
            self.g0_sampleAll = np.arange(self.basicdata.nTrapBait)
            self.sampleIndx = np.zeros(len(self.g0_sample0))
            # storage array for prob of trapping data
            self.pTrappingData = np.empty(self.basicdata.nsession)
            self.pTrappingData_s = np.empty(self.basicdata.nsession)
            # Index into array of Gibbs steps to save
            self.cc = 0

        else:
            self.startingStep = checkingdata.startingStep
            self.bgibbs = checkingdata.bgibbs
            self.rgibbs = checkingdata.rgibbs                                  # pop growth
            self.rparagibbs = checkingdata.rparagibbs                          # wrapped cauchy para for recruitment 
            self.igibbs = checkingdata.igibbs                                  # immigration
            self.siggibbs = checkingdata.siggibbs                              # home range decay
            self.g0gibbs = checkingdata.g0gibbs                                # beta parameters
            self.Ngibbs = checkingdata.Ngibbs                                  # population size
            self.g0Multigibbs = checkingdata.g0Multigibbs                      # seasonal g0 multiplier 
            self.initialRepPopgibbs = checkingdata.initialRepPopgibbs
            ### g0 update sampling arrays
            self.g0_sample0 = np.arange(self.basicdata.nTrapBait, step=2)
            self.g0_sample1 = np.arange(1, self.basicdata.nTrapBait, step=2)
            self.g0_sampleAll = np.arange(self.basicdata.nTrapBait)
            self.sampleIndx = np.zeros(len(self.g0_sample0))
            # storage array for prob of trapping data
            self.pTrappingData = np.empty(self.basicdata.nsession)
            self.pTrappingData_s = np.empty(self.basicdata.nsession)
            # Index into array of Gibbs steps to save
            self.cc = checkingdata.cc


        ## run mcmcFX - gibbs loop
        print('cc', self.cc)

        self.mcmcFX()

#            print('checking and cc', self.startingStep, self.cc)

##################
#################
################

    @staticmethod
    def PPredatorTrapCaptFX(availTrapNights, eTermMat, g0Sess, debug = False):      
        """
        prob that stoat at specifid locs was capt in specified traps
        """
        availTrapNights = availTrapNights.flatten()
#        eTermMat = eTermMat.flatten()
        g0Sess = g0Sess.flatten()
        pNoCapt = 1.0 - (g0Sess * eTermMat)
        pNoCaptNights = pNoCapt**(availTrapNights)
        pNoCaptNights[pNoCaptNights >= .9999] = .9999
        pcapt = 1.0 - pNoCaptNights
        pcapt[pcapt > .97] = 0.97
        if debug == True:
            print('(g0Sess * eTermMat)', (g0Sess * eTermMat).shape)
            print('pNoCapt', pNoCapt)
            print('tn',availTrapNights.shape)
            print('eterm', eTermMat.shape)
            print('g0', g0Sess.shape)
            print('pcapt.shp', pcapt.shape)
        return pcapt        # 1.0 - pNoCaptNights

    def getBetaFX(self):
        a = self.basicdata.g0 * ((self.basicdata.g0 * (1.0 - self.basicdata.g0)) / self.params.g0Sd**2.0 - 1.0)
        b = (1.0 - self.basicdata.g0) * ((self.basicdata.g0 * (1.0 - self.basicdata.g0)) / self.params.g0Sd**2.0 - 1.0)
        return np.random.beta(a, b, size = None)





########
########       Block of functions for updating N
########

    def N_predatordata_updateFX(self):
#        print('totrec', self.basicdata.totalRecruits)
#        print('reproPop', self.basicdata.reproPop)
#        self.basicdata.Npred_s = self.basicdata.Npred.copy()
        for i in range(self.basicdata.nsession):
                                                ####### Update N
                                                 # get proposed Ns and predatordata_s and llik
            (Ns, sessionMask, predatorLoc_s, predatorPres_s, predatorPCapt_s, llik, llik_s,
                availTrapNights, presOnlyMask, presOnlyMask_s, trapSessionMask, 
                g0Session) = self.proposeNFX(i)

#            print('i, n, npred, sessrec', i, self.basicdata.N[i], self.basicdata.Npred[i], self.basicdata.sessionRecruits[i])
#            print('npred', self.basicdata.Npred)
#            print('totRec', self.basicdata.totalRecruits)
#           print('sessrecruits', self.basicdata.sessionRecruits)


                                                # pnow pnew Npred
            presOnlyMask = self.N_PnowPnewFX(Ns, i, llik,
                llik_s, presOnlyMask, presOnlyMask_s, predatorLoc_s, predatorPres_s,
                predatorPCapt_s, sessionMask, debug = False)
                                                # returns predatorloc
                                                # returns predatorPres
                                                # returns predatorPCapt


                                                ########### update predator location
                                                # propose new predatorloc and pcapt
            (predatorLocSession, predatorPCaptSession, predatorRemoveSession, predatorPresSession,
                predatorTrapIDSession, predatorTrapPCaptSession, remMask,
                nPredatorCellSession) = self.updatePredatorLocFX(i,
                sessionMask, availTrapNights, presOnlyMask, g0Session)

#                                                ########## update predators that are removed
#                                                # return predatorloc and predatorPcapt
#            if self.basicdata.removeDat[i] > 0:
#                (predatorRemoveSession, predatorTrapIDSession,
#                    predatorTrapPCaptSession,remMask) = self.updatePredatorRemoveFX(i, sessionMask,
#                    presOnlyMask, predatorLocSession, predatorPCaptSession,
#                    predatorRemoveSession, predatorPresSession, predatorTrapIDSession,
#                    predatorTrapPCaptSession, remMask, availTrapNights, g0Session)




                                                ######### update trapid of traps that catch predators
                                                #########
            if self.basicdata.removeDat[i] > 1:
                self.updateTrapIDFX(i, sessionMask, predatorLocSession, predatorPresSession,
                    predatorTrapIDSession, predatorTrapPCaptSession, remMask,
                    availTrapNights, g0Session)

                                                 ######## calc th likelihoods by session
            self.thetaLikelihoodFX(i, predatorLocSession, presOnlyMask, sessionMask)


#                                                ######## get llik for g0 and g0_s
#            self.g0SigLLikFX(i, availTrapNights, predatorLocSession, predatorTrapIDSession,
#                predatorPCaptSession, predatorTrapPCaptSession, sessionMask, presOnlyMask,
#                predatorRemoveSession, remMask, g0Session, g0Session_s)

                


#########
########  End updating N and predatordata
########

    def proposeNFX(self, i):    # get proposed Ns and assoc predator data
                                                                        # use FX in NupdateFX
        rDat = self.basicdata.removeDat[i]
        Ns = self.basicdata.N[i] + np.random.permutation(self.basicdata.nsample)[0]
        Ns = np.where(Ns <= 0, 1, Ns)
        Ns = np.where(Ns <= rDat, rDat + 1, Ns)
        Ns = np.where(Ns == self.basicdata.N[i], self.basicdata.N[i] + 1, Ns)
        Ns = np.where(Ns >  self.params.maxN, self.basicdata.N[i] - 5, Ns)
        Ns = np.where(Ns == self.basicdata.N[i], self.basicdata.N[i] - 1, Ns)

        sessionMask = self.predatordata.predatorSession == i
        predatorRemoveSession = self.predatordata.predatorRemove[sessionMask]
        predatorPresSession = self.predatordata.predatorPres[sessionMask]
        predatorPCaptSession = self.predatordata.predatorPCapt[sessionMask]
        presOnlyMask = predatorPresSession == 1                    #mask of pres only
        predatorLoc_s = self.predatordata.predatorLoc[sessionMask]
        predatorPres_s = predatorPresSession.copy()
        predatorPCapt_s = predatorPCaptSession.copy()
        # mask for trap data (nsession * ntrap)
        trapSessionMask = self.basicdata.trapSession == i
        # g0 for session i
        g0Session = self.basicdata.g0All[trapSessionMask]
        availTrapNights = self.basicdata.trapNightsAvail_2D[trapSessionMask]
        # get proposed predator data for Ns
        (presOnlyMask_s, predatorLoc_s, predatorPres_s, predatorPCapt_s) = self.proposePredatorFX(Ns, i, predatorPres_s, predatorLoc_s,
            predatorPCapt_s, availTrapNights, predatorRemoveSession,
            predatorPresSession, presOnlyMask, g0Session)
        # get binomial log likelihood for N and Ns
        llik = self.NLogLikFX(self.basicdata.N[i], rDat, predatorPCaptSession[presOnlyMask],
            predatorRemoveSession[presOnlyMask])        #(i > 11)
        llik_s = self.NLogLikFX(Ns, rDat, predatorPCapt_s[presOnlyMask_s],
            predatorRemoveSession[presOnlyMask_s])

        return (Ns, sessionMask, predatorLoc_s, predatorPres_s, predatorPCapt_s, llik, llik_s,
            availTrapNights, presOnlyMask, presOnlyMask_s, trapSessionMask, g0Session)


    def proposePredatorFX(self, Ns, i, predatorPres_s, predatorLoc_s,
        predatorPCapt_s, availTrapNights, predatorRemoveSession,
        predatorPresSession, presOnlyMask, g0Session):
        """
        determine loc, presence and pcapt for new predators
        Or, remove existing predators
        FX nested in proposeNFX
        """
        # in this block need to add individuals to proposed pop
        if Ns > self.basicdata.N[i]:
            potentialAddIndx = self.basicdata.datseq[presOnlyMask == False]
            nadd = Ns - self.basicdata.N[i]            # number predators added
            randindx = np.random.randint(0, self.basicdata.ncell, nadd)  # cell to add individual
            add_id = np.random.choice(potentialAddIndx, nadd, replace = False)  # individual id added to present list
            predatorPres_s[add_id] = 1
            presOnlyMask_s = predatorPres_s == 1
            predatorLoc_s[add_id] = randindx

#            print('i', i, 'randindx', randindx, 'add_id', add_id, 
#                'endid', randindx +1,
#                'shp', np.shape(self.basicdata.expTermMat),
#                'N', self.basicdata.N[i], 'Ns', Ns) 
            et_1 = self.basicdata.expTermMat[:, randindx]

###            if nadd == 1:
###                endid = randindx + 1
###                et_1 = self.basicdata.expTermMat[:, randindx]
###            else:
###                et_1 = self.basicdata.expTermMat[:, randindx]
            pCaptNew = self.PCaptPredatorFX(availTrapNights, et_1, g0Session)
            predatorPCapt_s[add_id] = pCaptNew

        # in this block need to remove individuals from pop
        if Ns < self.basicdata.N[i]:
            nrem = self.basicdata.N[i] - Ns
            removeSessionMask = predatorRemoveSession == 0
            predatorPresMask = predatorPresSession == 1
            potentialRemoveMask = predatorPresMask & removeSessionMask
            potentialRemove = self.basicdata.datseq[potentialRemoveMask]        # select from 0:maxN
            remIndx = np.random.choice(potentialRemove, nrem, replace = False)
            predatorLoc_s[remIndx] = 0
            predatorPres_s[remIndx] = 0
            predatorPCapt_s[remIndx] = 0
            presOnlyMask_s = predatorPres_s == 1
            randindx = 0
        return (presOnlyMask_s, predatorLoc_s, predatorPres_s, predatorPCapt_s)



    def N_PnowPnewFX(self, Ns, i, llik, llik_s, presOnlyMask, presOnlyMask_s,
        predatorLoc_s, predatorPres_s, predatorPCapt_s, sessionMask, debug = False):
        """
        calc Importance ratio for proposed N in session i
        use binomial for capture probabilities
        use poisson for predicted N against latent real N
        Have to calc Npred_s values for poisson probabilities
        """
#        printMask = (self.basicdata.uSession>41) & (self.basicdata.uSession<46)

        # not last session  #########################################
        if (i < self.basicdata.uSession[-1]):
            recruit = self.basicdata.sessionRecruits[i+1]
            Npred2_s = self.nPredFX(i, Ns, recruit)
            self.basicdata.Npred_s[i+1] = Npred2_s


#            if printMask[i]:
#                print('iiiiii', i, 'first Npred_s', self.basicdata.Npred_s[printMask])
               

            # get repro pop if Ns is in repro period (Nov) for the given year
            if self.basicdata.reproMask[i]:
                # get proposed repro pop by integrating the Ns value
                # recruitYearMask  is mask of repro period (Nov) of given year
                # yrIndx is index of repro period (Nov) for given year
                # reproPop_s is tot pop in repro period (Nov) in given year
                # totRecruit and yearRecruits are total recruits and number distributed to each session
                # get recruit info 
#                print('iiiiiiiii', i, '   In repro up one Npred', Npred2_s)

                (recruitYearMask, pseudoRecruitYearMask, pseudoSessRecruitYear, yrIndx, 
                    reproPop_s, totRecruits_s, yearRecruits_s) = self.recruit_NsInRepro(i, Ns)

#                print('iiiiiiiiii', i, 'repPeriod', self.basicdata.reproPeriodIndx[i], 
#                        '   repro up one Npred Diff', Npred2_s,  self.basicdata.Npred_s[i+1])


#                if printMask[i]:
#                    print('iiiii', i, 'second Npred_s', self.basicdata.Npred_s[printMask])
               
                # mask of sessions where recruits will fall
#                nTestMask = self.basicdata.recruitWindowMask & recruitYearMask 
                # N data where recruits fall
                nInYear = self.basicdata.N[recruitYearMask]
                dpoisRec = np.sum(stats.poisson.logpmf(nInYear, self.basicdata.Npred[recruitYearMask]))
                dpoisN1 =  stats.poisson.logpmf(self.basicdata.N[i], self.basicdata.Npred[i])
                dpoisN2 =  stats.poisson.logpmf(self.basicdata.N[i + 1], self.basicdata.Npred[i + 1])
                # llik for current N
                pnow = llik + dpoisRec + dpoisN1 + dpoisN2

                # get llik for proposed N
                dpoisRec_s = np.sum(stats.poisson.logpmf(nInYear, self.basicdata.Npred_s[recruitYearMask]))
                dpoisN1_s = stats.poisson.logpmf(Ns, self.basicdata.Npred[i])
                dpoisN2_s = stats.poisson.logpmf(self.basicdata.N[i + 1], self.basicdata.Npred_s[i+1])
                pnew = llik_s + dpoisRec_s  + dpoisN1_s + dpoisN2_s

#                if i == 153:
#                    print('repromask i', self.basicdata.reproMask[i])
#                    print('recruitYearMask', recruitYearMask)
#                    print('self.basicdata.recruitWindowMask', self.basicdata.recruitWindowMask)
#                    print('recruitYearMask', nTestMask)

#                if (i>152) & (i<155):
#                    print('iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii', i)
#                    yearIndx = self.basicdata.reproPeriodIndx[i]
#                    print('N and Ns', self.basicdata.N[i], Ns)
#                    print('npred and _s', self.basicdata.
#                    print('nInYear', nInYear)
#                    print('reproPop and reproPop_s', self.basicdata.reproPop[yearIndx], reproPop_s)
#                    print('llik + dpoisRec + dpoisN1 + dpoisN2', llik, dpoisRec, dpoisN1, dpoisN2)
#                    print('llik_s + dpoisRec_s + dpoisN1_s + dpoisN2_s', llik_s, dpoisRec_s, dpoisN1_s, dpoisN2_s)
#                    print('pnow and pnew', pnow, pnew)


            # if Ns not in repro period and not last session
            if (self.basicdata.reproMask[i] == 0):
                # poisson for current N and then N[i+1]
                dpoisN1 =  stats.poisson.logpmf(self.basicdata.N[i], self.basicdata.Npred[i])
                dpoisN2 =  stats.poisson.logpmf(self.basicdata.N[i + 1], self.basicdata.Npred[i + 1])
                pnow = llik + dpoisN1 + dpoisN2
                dpoisN1_s = stats.poisson.logpmf(Ns, self.basicdata.Npred[i])
                dpoisN2_s = stats.poisson.logpmf(self.basicdata.N[i + 1], self.basicdata.Npred_s[i+1])
                pnew = llik_s + dpoisN1_s + dpoisN2_s
            #################################################
            ####################################################
        # Last session
        if i == self.basicdata.uSession[-1]:
            pnow = llik + stats.poisson.logpmf(self.basicdata.N[i], self.basicdata.Npred[i])
            pnew = llik_s + stats.poisson.logpmf(Ns, self.basicdata.Npred[i])
        # Calc IR

        pdiff = pnew - pnow
        if pdiff > 10.0:
            rValue = 2.0
            zValue = 0.01
        elif pdiff < -5.0:
            rValue = 0.01
            zValue = 2.0
        else:
            rValue =  np.exp(pdiff)       # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        # update if new parameters are better
#        rValue = np.exp(pnew - pnow)        # calc importance ratio
#        zValue = np.random.uniform(0,1, size = None)
        if rValue > zValue:
            # if before last session, update next npred
            if i < self.basicdata.uSession[-1]:
                self.basicdata.Npred[i + 1] = self.basicdata.Npred_s[i + 1]     # Npred2_s
            # if i is in repro period, update recruitment details:
            if self.basicdata.reproMask[i]:
                self.basicdata.reproPop[yrIndx] = reproPop_s    # mean repro pop
                self.basicdata.totalRecruits[yrIndx] = totRecruits_s    # total recruits
                self.basicdata.sessionRecruits[recruitYearMask] = yearRecruits_s    # recruits in sess
                self.basicdata.Npred[recruitYearMask] = self.basicdata.Npred_s[recruitYearMask]
                self.basicdata.pseudoSessRecruits[pseudoRecruitYearMask] = pseudoSessRecruitYear
                self.basicdata.pseudoTotRecruitArr[pseudoRecruitYearMask] = totRecruits_s

            # update if r > z
            self.basicdata.N[i] = Ns
            self.predatordata.predatorLoc[sessionMask] = predatorLoc_s
            self.predatordata.predatorPres[sessionMask] = predatorPres_s
            self.predatordata.predatorPCapt[sessionMask] = predatorPCapt_s
            
            presOnlyMask = presOnlyMask_s.copy()
#        if i == 0:
#            print('r -z', rValue - zValue)
#            pcaptSess = self.predatordata.predatorPCapt[sessionMask]
#            print('Nupdate predatorPCapt[presOnlyMask]', self.predatordata.predatorPCapt[presOnlyMask])
#            print('Nupdate loc', self.predatordata.predatorLoc[presOnlyMask]) 

#            if i == 43:
#                print('43  r - z', rValue - zValue, 'N', self.basicdata.N[i], 'Ns', Ns)

#        if i == self.basicdata.uSession[-1]:
#            print('after npred', self.basicdata.Npred[printMask])
#            print('after npred_s', self.basicdata.Npred_s[printMask])
#            npredTest = self.NpredAllMCMC(self.basicdata.sessionRecruits)
#            print('pred npred', npredTest[printMask])
        return presOnlyMask


    def recruit_NsInRepro(self, i, Ns):
        """
        Get Npred across sessions in a year when update Ns session is in repro period
        """
        yrIndx = self.basicdata.reproPeriodIndx[i]                                  # reproductive year
        recruitYearMask = self.basicdata.yearRecruitIndx == yrIndx                  # mask of sessions for year
        reproPop_s = self.getProposedReproPop(i, Ns, yrIndx)                        # repro pop in given year

        totRecruits_s = (reproPop_s * self.basicdata.rg) + self.basicdata.ig        # total recruits in year
        nPseudoSessYear_i = self.basicdata.nPseudoSessInYear[yrIndx]
        pseudoRecruitYearMask = self.basicdata.pseudoYearRecruitIndx == yrIndx 
        pseudoSessRecruitYear = self.proposedRecruitYear(totRecruits_s, pseudoRecruitYearMask)
        julPseudoYear = self.basicdata.pseudoJulYear[pseudoRecruitYearMask]
        sessionRecruits = self.basicdata.sessionRecruits[recruitYearMask]
        sessionJul = self.basicdata.startJulSess[recruitYearMask]
        nSess_i = self.basicdata.nSessInYear[yrIndx]
        # numba fx to get recruits for each session in the recruitment year
        yearRecruits_s = basicsModule.getSessRecruitsOneYear(nPseudoSessYear_i, pseudoSessRecruitYear, 
            sessionRecruits, sessionJul, julPseudoYear, nSess_i)
        # populate arrays
        self.basicdata.reproPop_s[yrIndx] = reproPop_s
        self.basicdata.totalRecruits_s[yrIndx] = totRecruits_s
        self.basicdata.sessionRecruits_s[recruitYearMask] = yearRecruits_s
        # get nPred_s for year with updated number of recruits
        self.basicdata.Npred_s[recruitYearMask] = self.NpredYear_NsInRepro(i, Ns, recruitYearMask)
        return (recruitYearMask, pseudoRecruitYearMask, pseudoSessRecruitYear, yrIndx, 
            reproPop_s, totRecruits_s, yearRecruits_s)

    def getProposedReproPop(self, i, Ns, yrIndx):
        """
        Get reproductive population for a single year
        """
        ntmp_s1 = np.sum(self.basicdata.N[self.basicdata.reproPeriodIndx == yrIndx])
#        print('yrindx', yrIndx, 'nsessinrepro', self.basicdata.nSessInRepro)
        nSess = self.basicdata.nSessInRepro[yrIndx]
        rPop = (ntmp_s1 - self.basicdata.N[i] + Ns)/nSess 
        return rPop

    def proposedRecruitYear(self, totRecruits, pseudoRecruitYearMask):
        """
        Calculate predicted number of recruits in each week of a given year
        """
        relWrpCauchy = self.basicdata.relWrpCauchy[pseudoRecruitYearMask]
        recruitYearPseudo = totRecruits * relWrpCauchy
        return recruitYearPseudo


    def NpredYear_NsInRepro(self, i, Ns, recruitYearMask):
        """
        get Npred or Npred.s for new N for a given year when change N in repro period
        """
        datseq = self.basicdata.uSession[recruitYearMask] - 1
        Ntmp = self.basicdata.N.copy()
        Ntmp[i] = Ns
        Nstart = Ntmp[datseq]
        remTmp = self.basicdata.removeDat[datseq]
        Ntmp2 = Nstart - remTmp
        npredout = Ntmp2 + self.basicdata.sessionRecruits_s[recruitYearMask]

#        if i == 43:
#            print('usession', self.basicdata.uSession)
#            print('self.basicdata.yearRecruitIndx ==0 ', 
#                self.basicdata.uSession[self.basicdata.yearRecruitIndx == 1])
#            print('reproPeriodIndx', self.basicdata.reproPeriodIndx)
#            print('self.basicdata.repPeriodIndx == ', 
#                self.basicdata.uSession[self.basicdata.reproPeriodIndx == 1])
           
#            print('dataseq', datseq)
#            print('Nstart', Nstart[:5])
#            print('remTmp', remTmp[:5])
#            print('Ntmp2', Ntmp2[:5])
#            print('sessionRecruits_s[recruitYearMask]', (self.basicdata.sessionRecruits_s[recruitYearMask])[:5])
#            print('sessionRecruits[recruitYearMask]', (self.basicdata.sessionRecruits[recruitYearMask])[:5])
#            print('Ntmp2 + sessionRecruits_s', (Ntmp2 + self.basicdata.sessionRecruits_s[recruitYearMask])[:5])
            
        return npredout


    def nPredFX(self, i, nn, recruit):
        """
        Get npred for up one session
        """
        nStart = nn - self.basicdata.removeDat[i]
        npred1 = nStart + recruit
#        if i == 43:
#            print('XXXX NpredFX   iii', i, 'nstart', nStart, 'npred1', npred1)

        return npred1


    def PCaptPredatorFX(self, availTrapNights,  eTermMat, g0Sess):  
        """
         # prob stoat capt in 1 of many traps in a given session
        """
        pNoCapt = 1.0 - (g0Sess * eTermMat)
        pNoCaptNights = pNoCapt**(availTrapNights)
        pNoCaptNights[pNoCaptNights >= .9999] = .9999
#        sortPNoCapt = np.sort(pNoCaptNights, axis=0)
        pNoCaptNightsTraps = np.prod(pNoCaptNights, axis = 0)
#        pNoCaptNightsTraps = np.prod(sortPNoCapt[:self.params.nTopTraps], axis = 0)
        pcapt = 1.0 - pNoCaptNightsTraps
        pcapt[pcapt > 0.97] = .97
        return pcapt


    @staticmethod
    def NLogLikFX(n, remd, captp, sessrem, debug=False):    
        """
         # FX get binomial log lik
         # FX nested in ProposeNFX
        """
        comboTerm = gammaln(n + 1) - gammaln(n - remd + 1)
        first = captp * sessrem
        second = (1.0 - captp) * (1.0 - sessrem)
        binProball = first + second
        lProb = np.sum(np.log(binProball))
        llik_term = comboTerm + lProb
        return llik_term

#########
#########           End block of functions for updating N


#######
#######            Begin functions for updating predatorLoc
#######
    def updatePredatorLocFX(self, i, sessionMask, availTrapNights,
        presOnlyMask, g0Session):
        
#        print('iiii        ', i, self.basicdata.N[i])
        # predator loc, pres, capt and pcapt latent data
        predatorPresSession = self.predatordata.predatorPres[sessionMask]
        predatorLocSession = self.predatordata.predatorLoc[sessionMask]

#        ################### Tmp
#        self.basicdata.g0All = g0AllFX(self.basicdata.g0, self.basicdata.g0All, self.basicdata.trapBaitIndx,
#                self.basicdata.nsession, self.basicdata.trapSession, self.basicdata.g0MultiplierAll)
#        ################### End Tmp
#        if i == 0:
#            self.g0Sess_Loc = g0Session.copy()

        predatorPCaptSession = self.predatordata.predatorPCapt[sessionMask]
        predatorRemoveSession = self.predatordata.predatorRemove[sessionMask]
        remMask = predatorRemoveSession == 1               # mask only predators removed
        predatorPCapt_s = predatorPCaptSession.copy()
        predatorLoc_s = predatorLocSession.copy()
        predatorTrapIDSession = self.predatordata.predatorTrapID[sessionMask]
        predatorTrapPCaptSession = self.predatordata.predatorTrapPCapt[sessionMask]
        # propose to change 20% of predator locations - not all
        nChangeLoc = int(np.ceil(self.basicdata.N[i] * .80))
        potentialChangeID = self.basicdata.datseq[presOnlyMask]
        changeID =  np.random.choice(potentialChangeID, nChangeLoc, replace = False)

        changeMask = np.in1d(self.basicdata.datseq, changeID)
        Loc_s = np.random.randint(self.basicdata.ncell,size = nChangeLoc)
        predatorLoc_s[changeMask] = Loc_s
        # Prob capt of new loc by all traps     #### corrected for limited number of traps
###        if nChangeLoc == 1:
###            endID = Loc_s + 1
###            et_2 = self.basicdata.expTermMat[:, Loc_s : endID]
###        else:
###            et_2 = self.basicdata.expTermMat[:, Loc_s]

        et_2 = self.basicdata.expTermMat[:, Loc_s]

#        if self.basicdata.N[i] == 1:
#            et_2 = np.expand_dims(et_2, 1)
        pCaptNewLoc = self.PCaptPredatorFX(availTrapNights, et_2, g0Session)
        predatorPCapt_s[changeMask] = pCaptNewLoc

#        pCaptNew = self.PCaptPredatorFX(availTrapNights, et_2, g0Session)

        # index of traps with top prob of capt
#        topPCaptIndx = np.argsort(tmpPTrapCapt)[-params.nTopTraps:]
        # array of pcapt of only top traps
#        topPCapt = tmpPTrapCapt[topPCaptIndx]
        # total prob capture of pred j across all top traps
#        tmpPCapt[j] = 1. - np.prod(1. - topPCapt)

#        print(changeMask.shape)
#        print(Loc_s.shape)
#        print(pCaptNew.shape)
#        print(predatorPCapt_s.shape)

#        predatorPCapt_s[changeMask] = pCaptNew
        # Likelihood of capture data given locations of predator 
        Z_llik = self.NLogLikFX(1, 1, predatorPCaptSession[changeMask],
            predatorRemoveSession[changeMask], debug = False)             # llik binom capt data (z)
        Z_llik_s = self.NLogLikFX(1, 1, predatorPCapt_s[changeMask],
            predatorRemoveSession[changeMask])
        # Multinomial likelihood of location of predators
        (Loc_llik, nPredatorCellSession) = self.locMultiNom(predatorLocSession, 
            presOnlyMask, self.basicdata.thMultiNom, debug = False)
        (Loc_llik_s, nPredatorCellSession_s) = self.locMultiNom(predatorLoc_s, 
            presOnlyMask, self.basicdata.thMultiNom, debug = False)
        # mask of predator with changed locations that were captured
        changeRemoveMask = remMask & changeMask
        # prob that the trap capt predator in its location
        predatorTrapPCaptChangeRemove = predatorTrapPCaptSession[changeRemoveMask]
        # New proposed location of trapped pred that changed locations 
        Loc_sChangeRemove = predatorLoc_s[changeRemoveMask]
        tid = predatorTrapIDSession[changeRemoveMask]
        #proposed exp term for prob capture for proposed location
        etermsess = self.basicdata.expTermMat[tid, Loc_sChangeRemove]
        # trap nights
        tnightsavail = availTrapNights[tid] 
        g0tid = g0Session[tid]
        # prob capt in the given trap for captured predators in proposed location
        predatorTrapPCaptChangeRemove_s = self.PPredatorTrapCaptFX(tnightsavail, etermsess, g0tid)

#            print('g0 location', self.basicdata.g0)
#            print('sum availtrap locatation update', np.sum(availTrapNights))
#            print('availtrapnights', availTrapNights[presOnlyMask])
#            print('Location update ###################')
#            print('pCapt_s', predatorPCapt_s[presOnlyMask])
#            print('pcaptold', predatorPCaptSession[presOnlyMask])

#        if i == 0:
#            test = self.PPredatorTrapCaptFX(tnightsavail, etermsess, g0tid, debug = True)
#            print('iiiiiiiiiiiiiiii = 174')
#            print('predatorTrapPCaptChangeRemove_s', predatorTrapPCaptChangeRemove_s)
#            print('tn shp, Location', tnightsavail.shape)
#            print('etermsess', etermsess, etermsess.shape)
#            print('g0tid', g0tid, g0tid.shape)
#            pc = 1 - (g0tid * etermsess)
#            pcn = 1- (pc**tnightsavail)
#            print('pc pcn and predtrapcapt', pc, pcn, predatorTrapPCaptChangeRemove_s)
#            print('tid', tid, tid.shape)
#            print('Loc_sChangeRemove', Loc_sChangeRemove, Loc_sChangeRemove.shape)
              
#        predatorTrapPCaptChangeRemove_s = pCaptTrapNew
        # likelihood of trap that captured predators captured them in present and proposed locations
        Trap_llik = np.sum(np.log(predatorTrapPCaptChangeRemove))
        Trap_llik_s = np.sum(np.log(predatorTrapPCaptChangeRemove_s))
        # total probability of data given location and proposed location latent variables 
        pnow = Z_llik + Loc_llik + Trap_llik
        pnew = Z_llik_s + Loc_llik_s + Trap_llik_s
        # importance ratio
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0,1, size = None)
        # update latent variables if accept proposed locations
 
#        if i == 0:
#            print('Loc start pres id', self.basicdata.datseq[presOnlyMask])
#            print('LOC pred Loc_s', predatorLoc_s[presOnlyMask])
#            print('LOC pred Loc', predatorLocSession[presOnlyMask])
#            print('LOC r - v', rValue - zValue)


        if rValue > zValue:
            self.predatordata.predatorLoc[sessionMask] = predatorLoc_s          # don't need to return ???
            self.predatordata.predatorPCapt[sessionMask] = predatorPCapt_s
            predatorPCaptSession = predatorPCapt_s.copy()
            predatorLocSession = predatorLoc_s.copy()
            predatorTrapPCaptSession[changeRemoveMask] = predatorTrapPCaptChangeRemove_s
            self.predatordata.predatorTrapPCapt[sessionMask] = predatorTrapPCaptSession
            nPredatorCellSession = nPredatorCellSession_s
        # return updated latent variables
#####!                      
#        if i == 0:
#            print('loc proposed pcapt_s', predatorPCapt_s[presOnlyMask])
#            pc = self.predatordata.predatorPCapt[sessionMask]
#            print('Loc pcaptold', predatorPCaptSession[presOnlyMask])
#            print('loc change pcapt', pc[presOnlyMask])
#            locTmp = predatorLocSession[presOnlyMask]
#            et_s = self.basicdata.expTermMat[:, locTmp]
#            sumeterm = np.sum(et_s, axis = 0)
#            print('eterm Loc', sumeterm)

        return (predatorLocSession, predatorPCaptSession, predatorRemoveSession, predatorPresSession,
            predatorTrapIDSession, predatorTrapPCaptSession,remMask, nPredatorCellSession)

    def locMultiNom(self, predatorLocTest, presOnlyMask, thmn, debug = False):
        """
        Calc multinom density function for habitat
        """
        nPredatorCell =  np.bincount(predatorLocTest[presOnlyMask], minlength = 
                        self.basicdata.ncell)
        log_mn = basicsModule.multinomial_pmf(thmn, nPredatorCell)
        return (log_mn, nPredatorCell)

#######
######      End function for updating predator location

#####
#######    Begin functions for updating predator remove
#####
    def updatePredatorRemoveFX(self, i, sessionMask,
        presOnlyMask, predatorLocSession, predatorPCaptSession, predatorRemoveSession,
        predatorPresSession, predatorTrapIDSession,
        predatorTrapPCaptSession, remMask, availTrapNights, g0Session):
        """
        update predators that are captured one by one.
        """
        tmpNotRemMask = predatorRemoveSession == 0                  # mask of predators not removed
        presNotRemMask = presOnlyMask & tmpNotRemMask               # mask of removed predators
        nrem = self.basicdata.removeDat[i]                          # number predators removed
        remID = self.basicdata.datseq[remMask]                         # id indx of removed predators
        notRemID = self.basicdata.datseq[presNotRemMask]               # present individ not removed

        # cycle through all removed predators, propose alternative
        for j in range(nrem):                                       
            currentID = remID[j]
            currentTrapID = predatorTrapIDSession[currentID]
            predatorID_s = np.random.choice(notRemID, size = None, replace = True)
            predatorLoc_s = predatorLocSession[predatorID_s]

            zLLik = np.log(predatorPCaptSession[currentID])             # prob current predator capt
            zLLik_s = np.log(predatorPCaptSession[predatorID_s])        # prob proposed is captured

            eterm2 = self.basicdata.expTermMat[currentTrapID, predatorLoc_s]
            tnightsavail = availTrapNights[currentTrapID]
            g0Trap = g0Session[currentTrapID]
            predatorTrapPCapt_s = self.PPredatorTrapCaptFX(tnightsavail, eterm2, g0Trap, debug = False)

            TrapLLik = np.log(predatorTrapPCaptSession[currentID])
            TrapLLik_s = np.log(predatorTrapPCapt_s)
            
            pnow = zLLik + TrapLLik
            pnew = zLLik_s + TrapLLik_s

            rValue = np.exp(pnew - pnow)        # calc importance ratio
            zValue = np.random.uniform(0, 1.0, size = None)


#                print('r and z #################', rValue, zValue)
#                print('zLLik and TrapLLik', zLLik, TrapLLik)
#                print('zLLik_s and TrapLLik_s', zLLik_s, TrapLLik_s)

            if rValue > zValue:
                predatorRemoveSession[currentID] = 0
                predatorRemoveSession[predatorID_s] = 1
                predatorTrapIDSession[predatorID_s] = predatorTrapIDSession[currentID]
                predatorTrapIDSession[currentID] = 0

                predatorTrapPCaptSession[predatorID_s] = predatorTrapPCapt_s
                predatorTrapPCaptSession[currentID] = 0
                remID[j] = predatorID_s
                notRemID[notRemID == predatorID_s] = currentID

            currentID = remID[j]                                      # id of current removed predator [j]
            predatorID_s = np.random.choice(notRemID, size = None, replace = True)

        remMask = predatorRemoveSession == 1
        self.predatordata.predatorRemove[sessionMask] = predatorRemoveSession
        self.predatordata.predatorTrapID[sessionMask] = predatorTrapIDSession
        self.predatordata.predatorTrapPCapt[sessionMask] = predatorTrapPCaptSession

#        if i == 0:
#            self.g0Sess_Remove = g0Session.copy()

###        if i == 0:
###            print("pcapt remove update", predatorPCaptSession[presOnlyMask])

        return (predatorRemoveSession, predatorTrapIDSession, predatorTrapPCaptSession,
            remMask)

#######
######      End function for updating predator removal

#####
#######    Begin functions for updating trap id:
#####
    def updateTrapIDFX(self, i, sessionMask, predatorLocSession, predatorPresSession,
        predatorTrapIDSession, predatorTrapPCaptSession, remMask, availTrapNights,
        g0Session):
        """
        update the trap that captures an individual predator.
        """
        nrem = self.basicdata.removeDat[i]                          # number predators removed
        remIndx = self.basicdata.datseq[remMask]                    # id indx of removed predators
        # loop through predator-traps that had captures
        for j in range(nrem):
            predatorIndx = remIndx[j]
            indxPool = remIndx[remIndx != predatorIndx]
            # cycle through all removed predators, propose alterna
            predatorIndx_s = np.random.choice(indxPool, size = None, replace = True) 
            # this block propose new trap for current predator
            # loc of current predator
            predatorLoc = predatorLocSession[predatorIndx]   
            # location of second predator 
            predatorLoc_s = predatorLocSession[predatorIndx_s]
            # trap of first predator
            trapID_now = predatorTrapIDSession[predatorIndx]    # trapID[j]
            # trap of second predator
            trapID_s = predatorTrapIDSession[predatorIndx_s]
            # pCapt first predator with its original
            pCapt = predatorTrapPCaptSession[predatorIndx]
            # pCapt of second predator with its original trap
            pCapt_s = predatorTrapPCaptSession[predatorIndx_s]
            # prob of capture of first predator with second trap - switched
            pCaptSwitch = self.PPredatorTrapCaptFX(availTrapNights[trapID_s],
                self.basicdata.expTermMat[trapID_s, predatorLoc], g0Session[trapID_s])
            # pCapt of second predator with first trap - switched
            pCaptSwitch_s = self.PPredatorTrapCaptFX(availTrapNights[trapID_now],
                self.basicdata.expTermMat[trapID_now, predatorLoc_s], g0Session[trapID_now])

#            if (i ==174) & (j<5):
#                print('iiiiiiiii', i, j)
#                print('pCaptSwitch', pCaptSwitch)
#                print('pCaptSwitch_s', pCaptSwitch_s)

            # prob of original trap/pred and second trap/pred
            pnow = np.log(pCapt) + np.log(pCapt_s)
            # prob of switched preds and traps
            pnew = np.log(pCaptSwitch) + np.log(pCaptSwitch_s)
            # calc importance ratio
            rValue = np.exp(pnew - pnow)      
            zValue = np.random.uniform(0, 1.0, size = None)
            if rValue > zValue:
                # update if switched trap/predator is better
                predatorTrapIDSession[predatorIndx] = trapID_s
                predatorTrapIDSession[predatorIndx_s] = trapID_now
                predatorTrapPCaptSession[predatorIndx] = pCaptSwitch
                predatorTrapPCaptSession[predatorIndx_s] = pCaptSwitch_s
        # update latent trapid and and pcapt
        self.predatordata.predatorTrapID[sessionMask] = predatorTrapIDSession
        self.predatordata.predatorTrapPCapt[sessionMask] = predatorTrapPCaptSession

#        if i == 0:
#            self.g0Sess_trapid = g0Session.copy()

#######
###########         begin Beta - theta update ##########
#####
    def thetaLikelihoodFX(self, i, predatorLocSession, presOnlyMask, sessionMask):
        """
        get multinomial llik for each session
        """
        # number of predators in each cell
        nPredatorCell = np.bincount(predatorLocSession[presOnlyMask], minlength = 
                        self.basicdata.ncell)
        # multinomial LL for distribution of predators in cells with varying habitat quality
        self.basicdata.llikTh[i] = basicsModule.multinomial_pmf(self.basicdata.thMultiNom, 
                                nPredatorCell)
        self.basicdata.llikTh_s[i] = basicsModule.multinomial_pmf(
                                self.basicdata.thMultiNom_s, nPredatorCell)

    def betaUpdateFX(self):
        """
        updata betas all at same time
        """
        # likelihood of parameters given priors
        prior_pdf = stats.norm.logpdf(self.basicdata.b, self.params.bPrior, self.params.bPriorSD)
        prior_pdf_s = stats.norm.logpdf(self.basicdata.bs, self.params.bPrior, self.params.bPriorSD)
        # probability of data given current and proposed betas
        pnow = np.sum(self.basicdata.llikTh) + np.sum(prior_pdf)
        pnew = np.sum(self.basicdata.llikTh_s) + np.sum(prior_pdf_s)
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0, 1.0, size = None)
        if rValue > zValue:
            self.basicdata.b = self.basicdata.bs.copy()
            self.basicdata.lth = self.basicdata.lth_s.copy()
            self.basicdata.llikTh = self.basicdata.llikTh_s.copy()
            self.basicdata.thMultiNom = self.basicdata.thMultiNom_s.copy()
        self.basicdata.bs = np.random.normal(self.basicdata.b, .01)
        self.basicdata.lth_s = np.dot(self.basicdata.xdat, self.basicdata.bs)
        self.basicdata.thMultiNom_s = basicsModule.thProbFX(self.basicdata.lth_s)

#######                 NEW 
###########         begin g0 update ##########
###########
    def propose_g0_ONCE(self):
        """
        propose new g0 and sigma values
        do this here, at end of multiplier update because the new multiplier will affect g0All_s
        """
        # propose 8 new values 
        self.basicdata.g0_s = self.basicdata.g0.copy()
        self.sampleIndx = np.random.choice(self.g0_sampleAll, 8, replace = False)
        logit_g0 = basicsModule.logit(self.basicdata.g0[self.sampleIndx])
        self.basicdata.g0_s[self.sampleIndx] =  basicsModule.inv_logit(np.random.normal(logit_g0, 
            self.params.g0Sd))
        self.basicdata.g0All_s = basicsModule.g0AllFX(self.basicdata.g0_s, self.basicdata.g0All_s, self.basicdata.trapBaitIndx,
                self.basicdata.nsession, self.basicdata.trapSession, self.basicdata.g0MultiplierAll)

#    def propose_g0(self, i):
#        """
#        propose new g0 and sigma values
#        do this here, at end of multiplier update because the new multiplier will affect g0All_s
#        """
#        # propose 4 new values 
#        self.basicdata.g0_s = self.basicdata.g0.copy()
#        if i == 0:
#            self.sampleIndx = np.random.choice(self.g0_sample0, 4, replace = False)
#        else:
#            self.sampleIndx = np.random.choice(self.g0_sample1, 4, replace = False)
#        logit_g0 = logit(self.basicdata.g0[self.sampleIndx])
#        self.basicdata.g0_s[self.sampleIndx] =  inv_logit(np.random.normal(logit_g0, 0.02))
#        self.basicdata.g0All_s = g0AllFX(self.basicdata.g0_s, self.basicdata.g0All_s, self.basicdata.trapBaitIndx,
#                self.basicdata.nsession, self.basicdata.trapSession, self.basicdata.g0MultiplierAll)


#    def g0Sig_PPredatorCaptFX(self, availTrapNights, eterm, g0Param, debug = False):     # prob that predator was capt in trap
#        """
#        # prob that predator was not captured at trap levle
#        # returns a matrix
#        """
#        pNoCapt = 1.0 - (g0Param * eterm)
#        pNoCaptNights = (pNoCapt**availTrapNights)
#        pNoCaptNights[pNoCaptNights >= .9999] = .9999
#        return pNoCaptNights

    @staticmethod
    def g0Sig_ProbsFX(captp, sessrem, debug=False):     
        """
        # FX get binomial log lik
        """
        first = captp * sessrem
        second = (1.0 - captp) * (1.0 - sessrem)
        binProball = first + second
        llik_g0Sig = np.sum(np.log(binProball))
        return llik_g0Sig


    def getPTrappingData(self, k, availTrapNights, eterm_s, g0Session, pNoCaptAll_s):
        """
        Get probabilities of trapping data given parameters and location
        The prob that traps catch the predators that they did - without predator id
        """
        ######  use proposed parameters
        pCaptAll_s = 1 - np.prod(pNoCaptAll_s, axis = 1)      # p Capt at unique locs in all traps
        pCaptAllSumOne_s = pCaptAll_s / np.sum(pCaptAll_s)
        trapsessmask = self.basicdata.trapSession == k
        trapTrappedSession = self.basicdata.trapTrapped[trapsessmask]
        self.pTrappingData_s[k] = basicsModule.multinomial_pmf(pCaptAllSumOne_s, trapTrappedSession)
        ######  use current parameters        
        pCaptAllPredators = self.PCaptAllPreds_FX(availTrapNights,  eterm_s, g0Session)
        pCaptAllSumOne = pCaptAllPredators / np.sum(pCaptAllPredators)
        self.pTrappingData[k] = basicsModule.multinomial_pmf(pCaptAllSumOne, trapTrappedSession)


    def g0SigLLikFX_ONCE(self, j, availTrapNights, predatorLocSession, predatorTrapIDSession,
        predatorPCaptSession, predatorTrapPCaptSession, sessionMask, presOnlyMask,
        predatorRemoveSession, remMask, g0Session, g0Session_s, eTermMat):
        """
        get llik for g0 and g0_s
        """
        presLoc = predatorLocSession[presOnlyMask]
#        (uLoc, indxLoc) = np.unique(presLoc, return_inverse = True) #unique Loc so limit matrix computation
                                                                    #get indices to feedback pCapt to predator data
#        eterm_s = eTermMat[:, uLoc]                 # exp term of unique locs
#        l_presloc = len(presLoc)
#        if l_presloc == 1:
        if np.isscalar(presLoc):
            endPresID = presLoc + 1
            eterm_s = eTermMat[:, presLoc : endPresID]
        else:
            eterm_s = eTermMat[:, presLoc]

        (pNoCaptAll_s, pCapt_s) = self.PCapt_g0SigFX(availTrapNights, eterm_s, g0Session_s)

        predatorPCaptLoc_sSession = predatorPCaptSession.copy()
        predatorPCaptLoc_sSession[presOnlyMask] = pCapt_s
        self.predatordata.predatorPCapt_s[sessionMask] = predatorPCaptLoc_sSession #keep this in case keep new g0 and sig
        # the following gets info only for capt predators
        remPresMask = predatorRemoveSession[presOnlyMask] == 1         # mask of removed predators in sess i

        matIndx = np.arange(self.basicdata.N[j])
        remIndx = matIndx[remPresMask]

        remtid = predatorTrapIDSession[remMask]                        # trap id of captured predators
        predatorTrapPNoCapt_s = pNoCaptAll_s[remtid, remIndx]          # p No Capt of captured predators only
        predatorTrapPCapt_sSession = predatorTrapPCaptSession.copy()      # template
        predatorTrapPCapt_sSession[remMask] = 1.0 - predatorTrapPNoCapt_s # p capt fill in template
        self.predatordata.predatorTrapPCapt_s[sessionMask] = predatorTrapPCapt_sSession # fill in class data in case keep new params.
        # binomial LL for current g0 and sigma - prob of preds being captured
        self.basicdata.llikg0Sig[j] = self.g0Sig_ProbsFX(predatorPCaptSession[presOnlyMask],
            predatorRemoveSession[presOnlyMask])
        # binomial LL for proposed g0_s and sigma_s - prob of preds being captured
        self.basicdata.llikg0Sig_s[j] = self.g0Sig_ProbsFX(pCapt_s,
            predatorRemoveSession[presOnlyMask])
        #############
        # multinomial prob of traps catching preds that they caught or did not catch.
        self.getPTrappingData(j, availTrapNights, eterm_s, g0Session, pNoCaptAll_s)
        #############


    def g0SigLLikFX(self, i, j, availTrapNights, predatorLocSession, predatorTrapIDSession,
        predatorPCaptSession, predatorTrapPCaptSession, sessionMask, presOnlyMask,
        predatorRemoveSession, remMask, g0Session, g0Session_s, eTermMat):
        """
        get llik for g0 and g0_s
        """
        presLoc = predatorLocSession[presOnlyMask]
#        (uLoc, indxLoc) = np.unique(presLoc, return_inverse = True) #unique Loc so limit matrix computation
                                                                    #get indices to feedback pCapt to predator data
#        eterm_s = eTermMat[:, uLoc]                 # exp term of unique locs
        l_presloc = len(presLoc)
        if l_presloc == 1:
            endPresID = presLoc + 1
            eterm_s = eTermMat[:, presLoc : endPresID]
        else:
            eterm_s = eTermMat[:, presLoc]

        (pNoCaptAll_s, pCapt_s) = self.PCapt_g0SigFX(availTrapNights, eterm_s, g0Session_s)

        predatorPCaptLoc_sSession = predatorPCaptSession.copy()
        predatorPCaptLoc_sSession[presOnlyMask] = pCapt_s
        self.predatordata.predatorPCapt_s[sessionMask] = predatorPCaptLoc_sSession #keep this in case keep new g0 and sig
        # the following gets info only for capt predators
        remPresMask = predatorRemoveSession[presOnlyMask] == 1         # mask of removed predators in sess i

        matIndx = np.arange(self.basicdata.N[j])
        remIndx = matIndx[remPresMask]

        remtid = predatorTrapIDSession[remMask]                        # trap id of captured predators
        predatorTrapPNoCapt_s = pNoCaptAll_s[remtid, remIndx]          # p No Capt of captured predators only
        predatorTrapPCapt_sSession = predatorTrapPCaptSession.copy()      # template
        predatorTrapPCapt_sSession[remMask] = 1.0 - predatorTrapPNoCapt_s # p capt fill in template
        self.predatordata.predatorTrapPCapt_s[sessionMask] = predatorTrapPCapt_sSession # fill in class data in case keep new params.
        # binomial LL for current g0 and sigma - prob of preds being captured
        self.basicdata.llikg0Sig[j] = self.g0Sig_ProbsFX(predatorPCaptSession[presOnlyMask],
            predatorRemoveSession[presOnlyMask])
        # binomial LL for proposed g0_s and sigma_s - prob of preds being captured
        self.basicdata.llikg0Sig_s[j] = self.g0Sig_ProbsFX(pCapt_s,
            predatorRemoveSession[presOnlyMask])
        #############
        # multinomial prob of traps catching preds that they caught or did not catch.
        self.getPTrappingData(j, availTrapNights, eterm_s, g0Session, pNoCaptAll_s)
        #############


    def PCapt_g0SigFX(self, availTrapNights,  eTermMat, g0Sess):
        """
         # prob stoat capt in 1 of many traps in a given session
        """
        pNoCapt = 1.0 - (g0Sess * eTermMat)
        pNoCaptNights = pNoCapt**(availTrapNights)
        pNoCaptNights[pNoCaptNights >= .9999] = .9999
#        sortPNoCapt = np.sort(pNoCaptNights, axis=0)
        pNoCaptNightsTraps = np.prod(pNoCaptNights, axis = 0)
#        pNoCaptNightsTraps = np.prod(sortPNoCapt[:self.params.nTopTraps], axis = 0)
        pcapt = 1.0 - pNoCaptNightsTraps
        pcapt[pcapt > 0.97] = .97
        return (pNoCaptNights, pcapt)


    def PCaptAllPreds_FX(self, availTrapNights,  eTermMat, g0Sess):
        """
         # prob stoat capt in 1 of many traps in a given session
        """
        pNoCapt = 1.0 - (g0Sess * eTermMat)
        pNoCaptNights = pNoCapt**(availTrapNights)
        pNoCaptNights[pNoCaptNights >= .9999] = .9999
        pNoCaptNightsAll = np.prod(pNoCaptNights, axis = 1)
        pCaptAllPredators = 1.0 - pNoCaptNightsAll
        return (pCaptAllPredators)


    def g0UpdateFX_ONCE(self):
        """
        update g0 in mcmc function
        """
        # random selection of 8 types for each mcmc iteration
        self.propose_g0_ONCE()
        # loop through sessions
        for j in range(self.basicdata.nsession):
            sessionMask = self.predatordata.predatorSession == j
            predatorRemoveSession = self.predatordata.predatorRemove[sessionMask]
            predatorPresSession = self.predatordata.predatorPres[sessionMask]
            predatorPCaptSession = self.predatordata.predatorPCapt[sessionMask]
            presOnlyMask = predatorPresSession == 1                    #mask of pres only
            predatorLocSession = self.predatordata.predatorLoc[sessionMask]
            remMask = predatorRemoveSession == 1               # mask only predators removed
            predatorTrapIDSession = self.predatordata.predatorTrapID[sessionMask]
            predatorTrapPCaptSession = self.predatordata.predatorTrapPCapt[sessionMask]
            # mask for trap data (nsession * ntrap)
            trapSessionMask = self.basicdata.trapSession == j
            # g0 for session j
            g0Session = self.basicdata.g0All[trapSessionMask]
            g0Session_s = self.basicdata.g0All_s[trapSessionMask]

            # avail trap nights for session i
            availTrapNights = self.basicdata.trapNightsAvail_2D[trapSessionMask]
            ######## get llik for g0 and g0_s
            self.g0SigLLikFX_ONCE(j, availTrapNights, predatorLocSession, predatorTrapIDSession,
                predatorPCaptSession, predatorTrapPCaptSession, sessionMask, presOnlyMask,
                predatorRemoveSession, remMask, g0Session, g0Session_s, self.basicdata.expTermMat)
        self.g0_PnowPnew()

    def g0UpdateFX(self):
        """
        update g0 in mcmc function
        """
        # loop through 2 random selection of 4 trap types = 8 types for each mcmc iteration
        for i in range(2):
            self.propose_g0(i)
            # loop through sessions
            for j in range(self.basicdata.nsession):
                sessionMask = self.predatordata.predatorSession == j
                predatorRemoveSession = self.predatordata.predatorRemove[sessionMask]
                predatorPresSession = self.predatordata.predatorPres[sessionMask]
                predatorPCaptSession = self.predatordata.predatorPCapt[sessionMask]
                presOnlyMask = predatorPresSession == 1                    #mask of pres only
                predatorLocSession = self.predatordata.predatorLoc[sessionMask]
                remMask = predatorRemoveSession == 1               # mask only predators removed
                predatorTrapIDSession = self.predatordata.predatorTrapID[sessionMask]
                predatorTrapPCaptSession = self.predatordata.predatorTrapPCapt[sessionMask]
                # mask for trap data (nsession * ntrap)
                trapSessionMask = self.basicdata.trapSession == j
                # g0 for session j
                g0Session = self.basicdata.g0All[trapSessionMask]
                g0Session_s = self.basicdata.g0All_s[trapSessionMask]

#                if (i == 0) & (j == 0):
#                    print('g0 diff g0 update', np.sum(g0Session - g0Session_s))
#                    ldat = len(g0Session)
#                    mat = np.zeros([ldat, 2])
#                    mat[:, 0] = g0Session.flatten()
#                    mat[:, 1] = g0Session_s.flatten()
#                    print('g0 mat', mat[0:200])

                # avail trap nights for session i
                availTrapNights = self.basicdata.trapNightsAvail_2D[trapSessionMask]
                 ######## get llik for g0 and g0_s
                self.g0SigLLikFX(i, j, availTrapNights, predatorLocSession, predatorTrapIDSession,
                            predatorPCaptSession, predatorTrapPCaptSession, sessionMask, presOnlyMask,
                            predatorRemoveSession, remMask, g0Session, g0Session_s, self.basicdata.expTermMat)
            self.g0_PnowPnew()

    def g0_PnowPnew(self):
        """
        calc pnow and pnew for g0 and update
        """        
        sumPTrapData = np.sum(self.pTrappingData)
        sumPTrapData_s = np.sum(self.pTrappingData_s)
#        sigmaPriorNow = stats.norm.logpdf(self.basicdata.sigma, self.params.sigma_mean, self.params.sigma_sd)
#        sigmaPriorNew = stats.norm.logpdf(self.basicdata.sigma_s, self.params.sigma_mean, self.params.sigma_sd)       
        pnow = np.sum(self.basicdata.llikg0Sig) + np.sum(np.log(stats.beta.pdf(self.basicdata.g0[self.sampleIndx],
            self.params.g0_alpha, self.params.g0_beta))) + sumPTrapData
        pnew = np.sum(self.basicdata.llikg0Sig_s) + np.sum(np.log(stats.beta.pdf(self.basicdata.g0_s[self.sampleIndx],
            self.params.g0_alpha, self.params.g0_beta))) + sumPTrapData_s
        pdiff = pnew - pnow
        if pdiff > 10.0:
            rValue = 2.0
            zValue = 0.01
        elif pdiff < -5.0:
            rValue = 0.01
            zValue = 2.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        # update if new parameters are better

#        print('###############  g0 update')
#        print('g0 and g0s', self.basicdata.g0, self.basicdata.g0_s)
###        print('pnow, pnew', pnow, pnew)
#        print('llik, llik_s', np.sum(self.basicdata.llikg0Sig), np.sum(self.basicdata.llikg0Sig_s))
#        print('sumPTrapData, sumPTrapData_s', sumPTrapData, sumPTrapData_s)
#        print('prior, prior_s', np.log(stats.beta.pdf(self.basicdata.g0[i],
#            self.params.g0_alpha, self.params.g0_beta)), np.log(stats.beta.pdf(self.basicdata.g0_s[i],
#            self.params.g0_alpha, self.params.g0_beta)))

        if rValue > zValue:
            self.basicdata.g0[self.sampleIndx] = self.basicdata.g0_s[self.sampleIndx]
            self.basicdata.g0All = self.basicdata.g0All_s.copy()
            self.predatordata.predatorPCapt = self.predatordata.predatorPCapt_s.copy()
            self.predatordata.predatorTrapPCapt = self.predatordata.predatorTrapPCapt_s.copy()
            self.pTrappingData = self.pTrappingData_s.copy()
#        if (i == 0):
#            sessMask = self.predatordata.predatorSession == 0
#            predPresSess = self.predatordata.predatorPres[sessMask]
#            pPCaptSess = self.predatordata.predatorPCapt[sessMask]
#            presOnly = predPresSess == 1                    #mask of pres only
#            print('r-v', rValue - zValue)
#            print('pPCaptSession', pPCaptSess[presOnly])


###########
################  End g0 update
#######


#######                 NEW 
###########         begin sigma update ##########
###########
    def propose_sigma(self):
        """
        propose new sigma values
        """
        # propose new values for next iteration
        self.basicdata.sigma_s = np.random.normal(self.basicdata.sigma, self.params.sigma_search_sd, size = None)
        self.basicdata.var2_s = 2.0 * (self.basicdata.sigma_s**2.0)
        self.basicdata.expTermMat_s =  np.exp(-(self.basicdata.distTrapToCell2) / self.basicdata.var2_s)

    def sigLLikFX(self, j, availTrapNights, predatorLocSession, predatorTrapIDSession,
        predatorPCaptSession, predatorTrapPCaptSession, sessionMask, presOnlyMask,
        predatorRemoveSession, remMask, g0Session, eTermMat):
        """
        get llik for g0 and g0_s
        """
#        presLoc = predatorLocSession[presOnlyMask]
#        (uLoc, indxLoc) = np.unique(presLoc, return_inverse = True) #unique Loc so limit matrix computation
                                                                    #get indices to feedback pCapt to predator data
#        eterm_s = eTermMat[:, uLoc]                 # exp term of unique locs
#        # pCapt with proposed sigma and g0 parameters (all of them at once)
#        pNoCaptAll_s = self.g0Sig_PPredatorCaptFX(availTrapNights, eterm_s, g0Session)
#        sortPNoCapt = np.sort(pNoCaptAll_s, axis=0)
#        pNoCapt_s = np.prod(sortPNoCapt[:self.params.nTopTraps], axis = 0)
#        pNoCapt_s = pNoCaptAll_s.prod(axis = 0)                     # p not capt in all traps
#        pCapt_s = 1.0 - pNoCapt_s                                   # p Capt at unique locs in all traps
#        pCapt_s[pCapt_s > 0.97] = 0.97
#        pCaptLoc_s = pCapt_s[indxLoc]                               # feedback new pCapt to predator data
#        predatorPCaptLoc_sSession = predatorPCaptSession.copy()

        presLoc = predatorLocSession[presOnlyMask]

###        l_presloc = len(presLoc)
###        if l_presloc == 1:
###            endPresID = presLoc + 1
###            eterm_s = self.basicdata.expTermMat_s[:, presLoc : endPresID]
###        else:
###            eterm_s = self.basicdata.expTermMat_s[:, presLoc]
        eterm_s = self.basicdata.expTermMat_s[:, presLoc]

#              eterm = self.basicdata.expTermMat[:, presLoc]

        (pNoCaptAll_s, pCapt_s) = self.PCapt_g0SigFX(availTrapNights, eterm_s, g0Session)

        pCaptAll_s = 1 - np.prod(pNoCaptAll_s, axis = 1)                                   # p Capt at unique locs in all traps
        pCaptAllSumOne_s = pCaptAll_s / np.sum(pCaptAll_s)
        trapsessmask = self.basicdata.trapSession == j
        trapTrappedSession = self.basicdata.trapTrapped[trapsessmask]
        self.pTrappingData_s[j] = basicsModule.multinomial_pmf(pCaptAllSumOne_s, trapTrappedSession)

        predatorPCaptLoc_sSession = predatorPCaptSession.copy()
        predatorPCaptLoc_sSession[presOnlyMask] = pCapt_s
#        predatorPCaptLoc_sSession[presOnlyMask] = pCaptLoc_s
        self.predatordata.predatorPCapt_s[sessionMask] = predatorPCaptLoc_sSession #keep this in case keep new g0 and sig
        # the following gets info only for capt predators
        remPresMask = predatorRemoveSession[presOnlyMask] == 1         # mask of removed predators in sess i
        matIndx = np.arange(self.basicdata.N[j])
        remIndx = matIndx[remPresMask]
        remtid = predatorTrapIDSession[remMask]                        # trap id of captured predators
        predatorTrapPNoCapt_s = pNoCaptAll_s[remtid, remIndx]          # p No Capt of captured predators only
#        remPresMask = predatorRemoveSession[presOnlyMask] == 1         # mask of removed predators in sess i
#        remIndx = indxLoc[remPresMask]                              # locations only of captured predators
#        remtid = predatorTrapIDSession[remMask]                        # trap id of captured predators
#        predatorTrapPNoCapt_s = pNoCaptAll_s[remtid, remIndx]          # p No Capt of captured predators only
        predatorTrapPCapt_sSession = predatorTrapPCaptSession.copy()      # template
        predatorTrapPCapt_sSession[remMask] = 1.0 - predatorTrapPNoCapt_s # p capt fill in template
        self.predatordata.predatorTrapPCapt_s[sessionMask] = predatorTrapPCapt_sSession # fill in class data in case keep new params.
###        if (j == 0):
#            print('eterm and g0 shp', eterm_s.shape, g0Session.shape)
#            print('predatorPCaptSession', predatorPCaptSession[presOnlyMask])
###            print('pCapt_s sig update', pCapt_s)
#            print('predatorRemoveSession', predatorRemoveSession[presOnlyMask])
        # binomial LL for current sigma 
        self.basicdata.llikg0Sig[j] = self.g0Sig_ProbsFX(predatorPCaptSession[presOnlyMask],
            predatorRemoveSession[presOnlyMask])
        # binomial LL for proposed sigma_s
        self.basicdata.llikg0Sig_s[j] = self.g0Sig_ProbsFX(pCapt_s,
            predatorRemoveSession[presOnlyMask])

#        #############
#        # multinomial prob of traps catching preds that they caught or did not catch.
#        self.getPTrappingData(j, availTrapNights, eterm, g0Session, pNoCaptAll_s)
#        #############



    def sigUpdateFX(self):
        """
        update sigma in mcmc function
        """
        self.propose_sigma()
            # loop through sessions
        for j in range(self.basicdata.nsession):
            sessionMask = self.predatordata.predatorSession == j
            predatorRemoveSession = self.predatordata.predatorRemove[sessionMask]
            predatorPresSession = self.predatordata.predatorPres[sessionMask]
            predatorPCaptSession = self.predatordata.predatorPCapt[sessionMask]
            presOnlyMask = predatorPresSession == 1                    #mask of pres only
            predatorLocSession = self.predatordata.predatorLoc[sessionMask]
            remMask = predatorRemoveSession == 1               # mask only predators removed
            predatorTrapIDSession = self.predatordata.predatorTrapID[sessionMask]
            predatorTrapPCaptSession = self.predatordata.predatorTrapPCapt[sessionMask]
            # mask for trap data (nsession * ntrap)
            trapSessionMask = self.basicdata.trapSession == j
            # g0 for session j
            g0Session = self.basicdata.g0All[trapSessionMask]
#            g0Session_s = self.basicdata.g0All_s[trapSessionMask]
            # avail trap nights for session i
#            if j == 0:
#                print('g0 shape', g0Session.shape)
#                print('g0Sess sig Diff', np.sum(self.g0Sess_g0 - g0Session))

#            availTrapNights = np.expand_dims(self.basicdata.trapNightsAvail[trapSessionMask], 1)
            availTrapNights = self.basicdata.trapNightsAvail_2D[trapSessionMask]
            ######## get llik for g0 and g0_s
            self.sigLLikFX(j, availTrapNights, predatorLocSession, predatorTrapIDSession,
                        predatorPCaptSession, predatorTrapPCaptSession, sessionMask, presOnlyMask,
                        predatorRemoveSession, remMask, g0Session, self.basicdata.expTermMat_s)
        self.sigma_PnowPnew()

    def sigma_PnowPnew(self):
        """
        calc pnow and pnew for sigma and update
        """        
        sumPTrapData = np.sum(self.pTrappingData)
        sumPTrapData_s = np.sum(self.pTrappingData_s)
        sigmaPriorNow = stats.norm.logpdf(self.basicdata.sigma, self.params.sigma_mean, self.params.sigma_sd)
        sigmaPriorNew = stats.norm.logpdf(self.basicdata.sigma_s, self.params.sigma_mean, self.params.sigma_sd)       
        pnow = np.sum(self.basicdata.llikg0Sig) + sigmaPriorNow + sumPTrapData
        pnew = np.sum(self.basicdata.llikg0Sig_s) + sigmaPriorNew + sumPTrapData_s
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0.0, 1.0, size = None)

#        print('pnow and new', pnow, pnew)
#        print('ll and ll_s', np.sum(self.basicdata.llikg0Sig), np.sum(self.basicdata.llikg0Sig_s))


        # update if new parameters are better
        if rValue > zValue:
            self.basicdata.sigma = self.basicdata.sigma_s
            self.predatordata.predatorPCapt = self.predatordata.predatorPCapt_s.copy()
            self.predatordata.predatorTrapPCapt = self.predatordata.predatorTrapPCapt_s.copy()
            self.basicdata.expTermMat = self.basicdata.expTermMat_s.copy()    
            self.pTrappingData = self.pTrappingData_s.copy()


#        print('Sigma###############')
#        print('LL and LL_s', np.sum(self.basicdata.llikg0Sig), np.sum(self.basicdata.llikg0Sig_s))
#        print('ptrap and ptrap_s', sumPTrapData, sumPTrapData_s)


###
################  End sigma update



################  Begin function to update g0Multiplier
#######

    def proposeg0Multi(self):
        """
        propose new parameters for g0Multiplier for updating
        """
        # proposed values
        self.basicdata.g0Multiplier_s[1:] = np.random.normal(self.basicdata.g0Multiplier[1:],
            self.params.g0MultiSearch)
        # make array length nSession of multiplier by season
        self.basicdata.g0MultiplierAll_s = self.basicdata.g0Multiplier_s[self.basicdata.g0_Indx_Multiplier]        
        self.basicdata.g0All_s = basicsModule.g0AllFX(self.basicdata.g0, self.basicdata.g0All_s, self.basicdata.trapBaitIndx, 
                    self.basicdata.nsession, self.basicdata.trapSession, self.basicdata.g0MultiplierAll_s)

    def g0MultiplierLLikFX(self):
        """
        get llik for g0Multiplier parameters
        """
        self.proposeg0Multi()
        for i in range(self.basicdata.nsession):
            sessionMask = self.predatordata.predatorSession == i
            predatorRemoveSession = self.predatordata.predatorRemove[sessionMask]
            remMask = predatorRemoveSession == 1
            predatorPresSession = self.predatordata.predatorPres[sessionMask]
            predatorPCaptSession = self.predatordata.predatorPCapt[sessionMask]
            predatorLocSession = self.predatordata.predatorLoc[sessionMask]
            predatorTrapIDSession = self.predatordata.predatorTrapID[sessionMask]
            predatorTrapPCaptSession = self.predatordata.predatorTrapPCapt[sessionMask]
            presOnlyMask = predatorPresSession == 1                    #mask of pres only
            # mask for trap data (nsession * ntrap)
            trapSessionMask = self.basicdata.trapSession == i
            # g0 for session i
            g0Session_s = self.basicdata.g0All_s[trapSessionMask]
            # avail trap nights for session i
#            availTrapNights = np.expand_dims(self.basicdata.trapNightsAvail[trapSessionMask], 1)
            availTrapNights = self.basicdata.trapNightsAvail_2D[trapSessionMask]

            presLoc = predatorLocSession[presOnlyMask]
###            l_presloc = len(presLoc)
###            if l_presloc == 1:
###                endPresID = presLoc + 1
###                eterm_s = self.basicdata.expTermMat[:, presLoc : endPresID]
###            else:
###                eterm_s = self.basicdata.expTermMat[:, presLoc]

            eterm_s = self.basicdata.expTermMat[:, presLoc]

            (pNoCaptAll_s, pCapt_s) = self.PCapt_g0SigFX(availTrapNights, eterm_s, g0Session_s)

            pCaptAll_s = 1 - np.prod(pNoCaptAll_s, axis = 1)                                   # p Capt at unique locs in all traps
            pCaptAllSumOne_s = pCaptAll_s / np.sum(pCaptAll_s)
            trapsessmask = self.basicdata.trapSession == i
            trapTrappedSession = self.basicdata.trapTrapped[trapsessmask]
            self.pTrappingData_s[i] = basicsModule.multinomial_pmf(pCaptAllSumOne_s, trapTrappedSession)


            # pCapt with proposed sigma and g0 parameters (all of them at once)
            predatorPCaptLoc_sSession = predatorPCaptSession.copy()
            predatorPCaptLoc_sSession[presOnlyMask] = pCapt_s
#           predatorPCaptLoc_sSession[presOnlyMask] = pCaptLoc_s
            self.predatordata.predatorPCapt_s[sessionMask] = predatorPCaptLoc_sSession #keep this in case keep new g0 and sig
            # the following gets info only for capt predators
            remPresMask = predatorRemoveSession[presOnlyMask] == 1         # mask of removed predators in sess i
            matIndx = np.arange(self.basicdata.N[i])
            remIndx = matIndx[remPresMask]
            remtid = predatorTrapIDSession[remMask]                        # trap id of captured predators
            predatorTrapPNoCapt_s = pNoCaptAll_s[remtid, remIndx]          # p No Capt of captured predators only


#            presLoc = predatorLocSession[presOnlyMask]
#            (uLoc, indxLoc) = np.unique(presLoc, return_inverse = True) #unique Loc so limit matrix computation
                                                                    #get indices to feedback pCapt to predator data
#            eterm = self.basicdata.expTermMat[:, uLoc]                 # exp term of unique locs
            # pCapt with proposed sigma and g0 parameters (all of them at once)
#            pNoCaptAll_s = self.g0Sig_PPredatorCaptFX(availTrapNights, eterm, g0Session_s)
#            sortPNoCapt = np.sort(pNoCaptAll_s, axis=0)
#            pNoCapt_s = np.prod(sortPNoCapt[:self.params.nTopTraps], axis = 0)
#            pCapt_s = 1.0 - pNoCapt_s                                   # p Capt at unique locs in all traps
#            pCapt_s[pCapt_s > 0.97] = 0.97

#            pCaptLoc_s = pCapt_s[indxLoc]                               # feedback new pCapt to predator data
#            predatorPCaptLoc_sSession = predatorPCaptSession.copy()
#            predatorPCaptLoc_sSession[presOnlyMask] = pCaptLoc_s
#            self.predatordata.predatorPCapt_s[sessionMask] = predatorPCaptLoc_sSession #keep this in case keep new g0 and sig
#            # the following gets info only for capt predators
#            remPresMask = predatorRemoveSession[presOnlyMask] == 1         # mask of removed predators in sess i
#            remIndx = indxLoc[remPresMask]                              # locations only of captured predators
#            remtid = predatorTrapIDSession[remMask]                        # trap id of captured predators
#            predatorTrapPNoCapt_s = pNoCaptAll_s[remtid, remIndx]          # p No Capt of captured predators only

            predatorTrapPCapt_sSession = predatorTrapPCaptSession.copy()      # template
            predatorTrapPCapt_sSession[remMask] = 1.0 - predatorTrapPNoCapt_s # p capt fill in template
            self.predatordata.predatorTrapPCapt_s[sessionMask] = predatorTrapPCapt_sSession # fill in class data in case keep new params.
            # binomial LL for current g0 and sigma
            self.basicdata.llikg0Sig[i] = self.g0Sig_ProbsFX(predatorPCaptSession[presOnlyMask],
                predatorRemoveSession[presOnlyMask])
            # binomial LL for proposed g0_s and sigma_s
            self.basicdata.llikg0Sig_s[i] = self.g0Sig_ProbsFX(pCapt_s,
                predatorRemoveSession[presOnlyMask])
###            if i == 0:
###                print('predatorPCaptSession ###### g0Multi update', predatorPCaptSession[presOnlyMask])
###                print('pCaptLoc_s ###### g0Multi update', pCapt_s)
###                print('self.basicdata.g0All diff', np.sum(self.basicdata.g0All_s - self.basicdata.g0All))
#                print('LL and LL_s', self.basicdata.llikg0Sig[i], self.basicdata.llikg0Sig_s[i])
        
    def g0MultiplierUpdateFX(self):
        """
        update g0 in mcmc function
        """
        self.g0MultiplierLLikFX()
        sumPTrapData = np.sum(self.pTrappingData)
        sumPTrapData_s = np.sum(self.pTrappingData_s)
        priorNow = stats.norm.logpdf(self.basicdata.g0Multiplier[1:],self.params.g0MultiPrior[0],
                                    self.params.g0MultiPrior[1])
        priorNew = stats.norm.logpdf(self.basicdata.g0Multiplier_s[1:], self.params.g0MultiPrior[0],
                                    self.params.g0MultiPrior[1])
        pnow = np.sum(self.basicdata.llikg0Sig) + np.sum(priorNow) + sumPTrapData
        pnew = np.sum(self.basicdata.llikg0Sig_s) + np.sum(priorNew) + sumPTrapData_s
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0.0, 1.0, size = None)
        # update if new parameters are better
        if rValue > zValue:
            self.basicdata.g0Multiplier = self.basicdata.g0Multiplier_s.copy()
            self.basicdata.g0MultiplierAll = self.basicdata.g0MultiplierAll_s.copy()            
            self.basicdata.g0All = self.basicdata.g0All_s.copy()
            self.predatordata.predatorPCapt = self.predatordata.predatorPCapt_s.copy()
            self.predatordata.predatorTrapPCapt = self.predatordata.predatorTrapPCapt_s.copy()

#        print('#######################   g0 multiplier')
#        print('LL and LL_s', np.sum(self.basicdata.llikg0Sig), np.sum(self.basicdata.llikg0Sig_s))
#        print('ptrap and ptrap_s', sumPTrapData, sumPTrapData_s)


#######
###########         begin r update ##########
#####

    def reproDataAllYears(self, growthrate):
        """
        get number of recruits across years and session
        with specified growth parameter
        """
        # get proposed total recruits for each year
        self.basicdata.totalRecruits_s = (self.basicdata.reproPop * growthrate) + self.basicdata.ig
        # make pseudo arrays for updating
        self.basicdata.pseudoTotRecruitArr_s = self.basicdata.totalRecruits_s[self.basicdata.pseudoYearRecruitIndx]
        self.basicdata.pseudoSessRecruits_s = (self.basicdata.pseudoTotRecruitArr_s *
            self.basicdata.relWrpCauchy)
        # get recruits across all sessions
        self.basicdata.sessionRecruits_s = basicsModule.getSessRecruitsOneYear(self.basicdata.nPseudoJulYear,
            self.basicdata.pseudoSessRecruits_s, self.basicdata.sessionRecruits_s,
            self.basicdata.startJulSess, self.basicdata.pseudoJulYear,
            self.basicdata.nsession)

#        tmpArr = np.zeros((self.basicdata.nPseudoJulYear, 3))
#        tmpArr[:,0] = self.basicdata.pseudoJulYear.copy()
#        pseudoReproMask = np.in1d(self.basicdata.pseudoJulYear, self.params.reproDays)
#        tmpArr[:,1] = pseudoReproMask * 1.0
#        tmpArr[:,2] = self.basicdata.pseudoYearRecruitIndx.copy()
#        tmpArr[:,1] = (self.basicdata.pseudoTotRecruitArr)
#        tmpArr[:,2] = self.basicdata.pseudoTotRecruitArr_s
#        tmpArr[:,4] = self.basicdata.pseudoSessRecruits 
#        tmpArr[:,3] = self.basicdata.pseudoSessRecruits_s
#        for i in range(self.basicdata.nPseudoJulYear):
#            print('iii', i, tmpArr[i])



    def NpredAllMCMC(self, recruits):
        """
        calc Npred for all sessions
        """
        Npred = np.zeros(self.basicdata.nsession)
        Npred[0] = self.basicdata.reproPop[0] + recruits[0]
        NpredTmp = self.basicdata.N[:-1] - self.basicdata.removeDat[:-1]
        Npred[1:] = NpredTmp + recruits[1:]
        Npred[Npred < 0] = .1
        return(Npred)

    def rLLikFX(self):
        """
        get llik for population growth parameter and proposed
        """
        # get recruits for each session with proposed rs
        self.reproDataAllYears(self.basicdata.rs)
        # get Npred_s
        self.basicdata.Npred_s = self.NpredAllMCMC(self.basicdata.sessionRecruits_s)
        self.basicdata.llikR = np.sum(np.log(stats.poisson.pmf(self.basicdata.N, self.basicdata.Npred)))
        self.basicdata.llikR_s = np.sum(np.log(stats.poisson.pmf(self.basicdata.N, self.basicdata.Npred_s)))
#        print('llikR and llikR_s',self.basicdata.rg, self.basicdata.rs, self.basicdata.llikR, self.basicdata.llikR_s)
#        print('r prior',  np.log(gamma_pdf(self.basicdata.rg,
#            self.params.r_shape, (1.0 / self.params.r_scale))))
#        print('rs prior',  np.log(gamma_pdf(self.basicdata.rs,
#            self.params.r_shape, (1.0 / self.params.r_scale))))
#        print('Npred', self.basicdata.Npred[22:28])     
#        print('Npred_s', self.basicdata.Npred_s[22:28])
#        print('rg', self.basicdata.rg, 'rs', self.basicdata.rs, 'totRec_s', self.basicdata.totalRecruits_s,
#            'totRec', self.basicdata.totalRecruits) 
#        print('npred Diff', (self.basicdata.Npred - self.basicdata.Npred_s))
#        print('relwrpCauchy', self.basicdata.relWrpCauchy[0:600])
#        print('self.basicdata.sessionRecruits', self.basicdata.sessionRecruits[0:600] - self.basicdata.sessionRecruits_s[0:600])

#        NpredTest = self.NpredAllMCMC(self.basicdata.sessionRecruits)
#        tmpArr = np.zeros((self.basicdata.nsession, 5))
#        tmpArr[:,0] = self.basicdata.reproPeriodIndx
#        tmpArr[:,1] = self.basicdata.yearRecruitIndx
#        tmpArr[:,2] = self.basicdata.Npred - self.basicdata.Npred_s
#        tmpArr[:,3] = self.basicdata.uSession
#        tmpArr[:,4] = self.basicdata.Npred_s
#        print('tmpArr', tmpArr[:46, :4])
#        print('npred[42:46]', self.basicdata.Npred[42:46])
#        print('npred_s[42:46]', self.basicdata.Npred_s[42:46])
#        print('totRec Diff', self.basicdata.totalRecruits_s - self.basicdata.totalRecruits)
#        print('sessRec Diff', self.basicdata.sessionRecruits - self.basicdata.sessionRecruits_s)

#        print('r npred[42:46]', self.basicdata.Npred[42:46])
#        print('r npred_s[42:46]', self.basicdata.Npred_s[42:46])

#        print('rg', self.basicdata.rg, 'r_s', self.basicdata.rs, 'totRec_s - totRec', 
#            self.basicdata.totalRecruits_s - self.basicdata.totalRecruits) 
#        print('npred Diff', np.sum(self.basicdata.Npred - self.basicdata.Npred_s))
#        print('relwrpCauchy', self.basicdata.relWrpCauchy[0:600])
#        print('self.basicdata.sessionRecruits', np.sum(self.basicdata.sessionRecruits - 
#            self.basicdata.sessionRecruits_s))




    def rUpdateFX(self):
        """
        update rg in mcmc function
        """
        self.basicdata.rs = np.exp(np.random.normal(np.log(self.basicdata.rg), 
            self.params.reproSearch, size = None))

#        self.basicdata.rs = self.basicdata.rg

        self.rLLikFX()
        pnow = self.basicdata.llikR + np.log(basicsModule.gamma_pdf(self.basicdata.rg,
            self.params.r_shape, (self.params.r_scale)))
        pnew = self.basicdata.llikR_s + np.log(basicsModule.gamma_pdf(self.basicdata.rs,
            self.params.r_shape, (self.params.r_scale)))
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0, 1.0, size = None)
        if rValue > zValue:
            self.basicdata.rg = self.basicdata.rs
            self.basicdata.Npred = self.basicdata.Npred_s.copy()
            self.basicdata.sessionRecruits = self.basicdata.sessionRecruits_s.copy()
            self.basicdata.totalRecruits = self.basicdata.totalRecruits_s.copy()
            self.basicdata.pseudoTotRecruitArr = self.basicdata.pseudoTotRecruitArr_s.copy()
            self.basicdata.pseudoSessRecruits = self.basicdata.pseudoSessRecruits_s.copy()

#        print('rg', self.basicdata.rg, 'rs', self.basicdata.rs, ' pnew', pnew, 'pnow', pnow)

######
################### End update for rg
#####

######
################### Update for rpara
###############     wrapped cauchy parameters for distributing recruits
#####

    def getSessRecWrpCauchy(self):
        """
        get number of recruits across years and session
        with specified growth parameter
        """
        self.basicdata.pseudoSessRecruits_s = (self.basicdata.pseudoTotRecruitArr * 
            self.basicdata.relWrpCauchy_s)
        # get recruits across all sessions
        self.basicdata.sessionRecruits_s = basicsModule.getSessRecruitsOneYear(self.basicdata.nPseudoJulYear, 
            self.basicdata.pseudoSessRecruits_s, self.basicdata.sessionRecruits_s, 
            self.basicdata.startJulSess, self.basicdata.pseudoJulYear,
            self.basicdata.nsession)

    def wrpCauchyLLikFX(self):
        """
        get llik for population growth parameter and proposed
        """
        # get recruits for each session with proposed rs
        self.getSessRecWrpCauchy()
        # get Npred_s
        self.basicdata.Npred_s = self.NpredAllMCMC(self.basicdata.sessionRecruits_s)
        self.basicdata.llikWrpC = np.sum(np.log(stats.poisson.pmf(self.basicdata.N, self.basicdata.Npred)))
        self.basicdata.llikWrpC_s = np.sum(np.log(stats.poisson.pmf(self.basicdata.N, self.basicdata.Npred_s)))


    def proposeNewRpara(self):
        """
        propose new rpara values for next iteration
        """
#        self.basicdata.rpara_s = self.basicdata.rpara.copy()
        self.basicdata.rpara_s[0] = np.random.normal(self.basicdata.rpara[0], 
            self.params.rparaSearch[0], size = None)
        self.basicdata.rpara_s[1] = np.exp(np.random.normal(np.log(self.basicdata.rpara[1]), 
            self.params.rparaSearch[1] , size = None))
        self.basicdata.relWrpCauchy_s = basicsModule.calcRelWrapCauchy(self.basicdata.rpara_s,
            self.basicdata.nPseudoJulYear, self.basicdata.uYearIndx, 
            self.basicdata.pseudoYearRecruitIndx, self.basicdata.daypiRecruit)

    def rparaUpdateFX(self):
        """
        update rg in mcmc function
        """
        self.proposeNewRpara()
        self.wrpCauchyLLikFX()
        pnow = (self.basicdata.llikWrpC + np.log(basicsModule.gamma_pdf(self.basicdata.rpara[1],
            self.params.rho_wrpc_Priors[0], (self.params.rho_wrpc_Priors[1]))) +
            np.log(stats.norm.pdf(self.basicdata.rpara[0], self.params.mu_wrpc_Priors[0], self.params.mu_wrpc_Priors[1])))
        pnew = (self.basicdata.llikWrpC_s + np.log(basicsModule.gamma_pdf(self.basicdata.rpara_s[1],
            self.params.rho_wrpc_Priors[0], (self.params.rho_wrpc_Priors[1]))) +
            np.log(stats.norm.pdf(self.basicdata.rpara_s[0], self.params.mu_wrpc_Priors[0], self.params.mu_wrpc_Priors[1])))
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0, 1.0, size = None)
#        print('self.basicdata.llikWrpC and _s', self.basicdata.llikWrpC, self.basicdata.llikWrpC_s)
#        print('pnow and pnew', pnow, pnew)
#        print('recruit diff', self.basicdata.sessionRecruits - self.basicdata.sessionRecruits_s)

        if rValue > zValue:
            self.basicdata.rpara = self.basicdata.rpara_s.copy()
            self.basicdata.relWrpCauchy = self.basicdata.relWrpCauchy_s.copy()
            self.basicdata.Npred = self.basicdata.Npred_s.copy()
            self.basicdata.sessionRecruits = self.basicdata.sessionRecruits_s.copy()
            self.basicdata.pseudoSessRecruits = self.basicdata.pseudoSessRecruits_s.copy()
#        print('RPARA.....pnow: ', pnow, 'pnew', pnew)



#######
##############      Begin initial Repro pop 
#######

    def proposeRecruitYr0(self):
        """
        get number of recruits across all sessions in year 0
        with proposed intial repro pop
        """
#        for i in self.basicdata.uYearIndx:
        rp = np.random.normal(self.basicdata.reproPop[0], self.params.initialReproSearch,
             size = None)
        if rp < self.params.IRR_priors[0]:
            rp = self.params.IRR_priors[0] + 5
        if rp > self.params.IRR_priors[1]:
            rp = self.params.IRR_priors[1] - 5
        self.basicdata.reproPop_s[0] = rp
        self.basicdata.totalRecruits_s[0] = (self.basicdata.reproPop_s[0] * self.basicdata.rg) + self.basicdata.ig
        # sequence to get recruits per session in first year
        relWrpCauchy = self.basicdata.relWrpCauchy[self.basicdata.pseudoRecruitYearMask_0]
        self.basicdata.pseudoSessRecruitYear_0  = self.basicdata.totalRecruits_s[0] * relWrpCauchy
        # get recruits for all sessions in first recruitment year
        # numba fx to get recruits for each session in the recruitment year
        self.basicdata.sessionRecruits_0 = basicsModule.getSessRecruitsOneYear(self.basicdata.nPseudoSessYear_0, 
            self.basicdata.pseudoSessRecruitYear_0, self.basicdata.sessionRecruits_0, 
            self.basicdata.sessionJul_0, self.basicdata.julPseudoYear_0,
            self.basicdata.nSession_0)

    def NpredYear0(self):
        """
        calc Npred for all sessions
        """
        Npred = np.zeros(self.basicdata.nSessYr0)
        Npred[0] = self.basicdata.reproPop_s[0] + self.basicdata.sessionRecruits_0[0]
        Npred[1:] = (self.basicdata.N[: self.basicdata.nSessYr0 - 1] - 
                    self.basicdata.removeDat[: self.basicdata.nSessYr0 - 1])
        Npred[1:] = (Npred[1:] + self.basicdata.sessionRecruits_0[1 : self.basicdata.nSessYr0])
        Npred[Npred < 0] = 0
        self.basicdata.Npred_s[: self.basicdata.nSessYr0] = Npred

    def initialReproLLik(self):
        """
        get llik for initial repro pop and proposed
        """
        self.basicdata.Npred_s = self.basicdata.Npred.copy()
        # get recruits for each session in year 0
        self.basicdata.pseudoSessRecruitYear_0 = self.proposeRecruitYr0()
        # get Npred_s
        self.NpredYear0()
        llik = np.sum(np.log(stats.poisson.pmf(self.basicdata.N[0 : self.basicdata.nSessYr0], 
                                self.basicdata.Npred[0 : self.basicdata.nSessYr0])))
        llik_s = np.sum(np.log(stats.poisson.pmf(self.basicdata.N[0 : self.basicdata.nSessYr0], 
                                self.basicdata.Npred_s[0 : self.basicdata.nSessYr0])))
        return(llik, llik_s)


    def initialReproUpdateFX(self):
        """
        update inital repro pop in mcmc function
        """
        (pnow, pnew) = self.initialReproLLik()
#        pnow = llik + stats.norm.logpdf(np.log(self.basicdata.reproPop[0]), self.params.IRR_priors[0],
#                        self.params.IRR_priors[1])
#        pnew = llik_s + stats.norm.logpdf(np.log(self.basicdata.reproPop_s[0]), self.params.IRR_priors[0],
#                        self.params.IRR_priors[1])
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0, 1.0, size = None)
        if rValue > zValue:
            self.basicdata.reproPop[0] = self.basicdata.reproPop_s[0]
            self.basicdata.Npred[self.basicdata.recruitYearMask_0] = (
                self.basicdata.Npred_s[self.basicdata.recruitYearMask_0])
            self.basicdata.sessionRecruits[self.basicdata.recruitYearMask_0] = (
                self.basicdata.sessionRecruits_0)
            self.basicdata.totalRecruits[0] = self.basicdata.totalRecruits_s[0]
            self.basicdata.pseudoSessRecruits[self.basicdata.pseudoRecruitYearMask_0] = (
                self.basicdata.pseudoSessRecruitYear_0)
            self.basicdata.pseudoTotRecruitArr[self.basicdata.pseudoRecruitYearMask_0] = (
                self.basicdata.totalRecruits[0])


#######
###########         begin I update ##########
#####
    def immLLikFX(self):
        """
        get llik for ig and is
        i in [0, nsession - 1]
        """
#        self.basicdata.i_s = self.basicdata.ig
        self.basicdata.i_s = np.random.normal(self.basicdata.ig, self.params.immSearch, size = None)
        # make uniform prior within bounds
        if (self.basicdata.i_s > self.params.immUniform[1]):
            self.basicdata.i_s = self.params.immUniform[1] - 20.0
        elif (self.basicdata.i_s < self.params.immUniform[0]):
            self.basicdata.i_s = self.params.immUniform[0] + 4.0
        # get proposed total number of recruits per year, and sesion recruits
        self.RecruitsAllYears() 
        self.basicdata.Npred_s = self.NpredAllMCMC(self.basicdata.sessionRecruits_s)
#        print('n', self.basicdata.N) 
#        print('npred', self.basicdata.Npred_s)
#        print('sessrecru', self.basicdata.sessionRecruits_s)
#        print('totrecru', self.basicdata.totalRecruits_s)
        self.basicdata.llikImm = np.log(stats.poisson.pmf(self.basicdata.N, self.basicdata.Npred))
        self.basicdata.llikImm_s = np.log(stats.poisson.pmf(self.basicdata.N, self.basicdata.Npred_s))

#        print('Imm npred[42:46]', self.basicdata.Npred[42:46])
#        print('Imm npred_s[42:46]', self.basicdata.Npred_s[42:46])

#        print('ig', self.basicdata.ig, 'i_s', self.basicdata.i_s, 'totRec_s - totRec', 
#            self.basicdata.totalRecruits_s - self.basicdata.totalRecruits) 
#        print('npred Diff', np.sum(self.basicdata.Npred - self.basicdata.Npred_s))
#        print('relwrpCauchy', self.basicdata.relWrpCauchy[0:600])
#        print('self.basicdata.sessionRecruits', np.sum(self.basicdata.sessionRecruits - 
#            self.basicdata.sessionRecruits_s))



    def RecruitsAllYears(self):
        """
        Calculate total number of recruits for all years when updating 
        recruitment for reproduction parameter
        """
        # get proposed total recruits for each year
        self.basicdata.totalRecruits_s = ((self.basicdata.reproPop * self.basicdata.rg) + 
            self.basicdata.i_s)
        # make pseudo arrays for updating
        self.basicdata.pseudoTotRecruitArr_s = self.basicdata.totalRecruits_s[self.basicdata.pseudoYearRecruitIndx]
        self.basicdata.pseudoSessRecruits_s = (self.basicdata.pseudoTotRecruitArr_s *
            self.basicdata.relWrpCauchy)
        # get recruits across all sessions
        self.basicdata.sessionRecruits_s = basicsModule.getSessRecruitsOneYear(self.basicdata.nPseudoJulYear,
            self.basicdata.pseudoSessRecruits_s, self.basicdata.sessionRecruits_s,
            self.basicdata.startJulSess, self.basicdata.pseudoJulYear,
            self.basicdata.nsession)
#        print('ig', self.basicdata.ig, 'i_s', self.basicdata.i_s, 'totRec_s', self.basicdata.totalRecruits_s,
#            'totRec', self.basicdata.totalRecruits) 
#        print('npred Diff', np.sum(self.basicdata.Npred - self.basicdata.Npred_s))
#        print('relwrpCauchy', self.basicdata.relWrpCauchy[0:600])
#        print('self.basicdata.sessionRecruits', self.basicdata.sessionRecruits[0:600] - self.basicdata.sessionRecruits_s[0:600])

    def ccIncrementFX(self, increment):
        """
        Increment the storage array index by the requested number
        """
        self.cc += increment 


    def immUpdateFX(self):
        """
        update ig in mcmc function
        """
        self.immLLikFX()
        if self.params.immDistr == 'uni':
            priorNow = 0.0
            priorNew = 0.0
        else:
            priorNow = stats.norm.logpdf(self.basicdata.ig,
                self.params.imm_mean, self.params.imm_sd)
            priorNew = stats.norm.logpdf(self.basicdata.i_s,
                self.params.imm_mean, self.params.imm_sd)
        # calc IR Ratio
        pnow = np.sum(self.basicdata.llikImm) + priorNow 
        pnew = np.sum(self.basicdata.llikImm_s) + priorNew
        pdiff = pnew - pnow
        if pdiff > 10.0:
            rValue = 2.0
            zValue = 0.01
        elif pdiff < -5.0:
            rValue = 0.01
            zValue = 2.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        # update if new parameters are better

#        rValue = np.exp(pnew - pnow)        # calc importance ratio
#        zValue = np.random.uniform(0.0, 1.0, size = None)
        if rValue > zValue:
            self.basicdata.ig = self.basicdata.i_s
            self.basicdata.Npred = self.basicdata.Npred_s.copy()
            self.basicdata.sessionRecruits = self.basicdata.sessionRecruits_s.copy()
            self.basicdata.totalRecruits = self.basicdata.totalRecruits_s.copy()
            self.basicdata.pseudoTotRecruitArr = self.basicdata.pseudoTotRecruitArr_s.copy()
            self.basicdata.pseudoSessRecruits = self.basicdata.pseudoSessRecruits_s.copy()
#        print('Imm pnow', pnow, 'pnew', pnew)


########            Main mcmc function
########
    def mcmcFX(self):
        # total steps including all checkpoints
        self.maxsteps = ((self.params.ngibbs * self.params.thinrate) + self.params.burnin)
        # stopping step for this run or checkpoint
        self.stoppingStep = np.min([(self.startingStep + self.params.interval), self.maxsteps])

        print('startstep =', self.startingStep, 'stopstep =', self.stoppingStep)
        print('ngibbs =', self.params.ngibbs, 'thinrate =', self.params.thinrate, 
            'burnin =', self.params.burnin, 'interval =', self.params.interval)

        for self.g in range(self.startingStep, self.stoppingStep):

#            print('iter', self.g)

            self.N_predatordata_updateFX()

            self.betaUpdateFX()

            self.g0UpdateFX_ONCE()

            self.sigUpdateFX()

            self.g0MultiplierUpdateFX()

            self.rUpdateFX()

            self.rparaUpdateFX()

            self.initialReproUpdateFX()

            self.immUpdateFX()

            if self.g in self.params.keepseq:
                self.Ngibbs[self.cc] = self.basicdata.N
                self.bgibbs[self.cc] = self.basicdata.b
                self.rgibbs[self.cc] = self.basicdata.rg
                self.igibbs[self.cc] = self.basicdata.ig
                self.g0gibbs[self.cc] = self.basicdata.g0.T
                self.siggibbs[self.cc] = self.basicdata.sigma
                self.rparagibbs[self.cc] = self.basicdata.rpara                         # wrapped cauchy para for recruitment
                self.g0Multigibbs[self.cc] = self.basicdata.g0Multiplier                       # seasonal g0 multiplier
                self.initialRepPopgibbs[self.cc] = self.basicdata.reproPop[0]

                self.ccIncrementFX(1)

        print('End cc', self.cc)
