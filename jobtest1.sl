#!/bin/bash
#SBATCH -J kiwitest
#SBATCH -A landcare00045
#SBATCH --mail-type=end
#SBATCH --mail-user=andersond@landcareresearch.co.nz
#SBATCH --time=00:40:00

#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=10000  

export RIOS_DFLT_JOBMGRTYPE=slurm
export RIOS_SLURMJOBMGR_SBATCHOPTIONS="-J kiwiTest -A landcare00045 --time=00:30:00 --mem-per-cpu=10000"
export RIOS_SLURMJOBMGR_INITCMDS="export PYTHONPATH=$PWD;module load Python-Geo"

#SBATCH -C wm
#SBATCH -o job-%j.%N.out
#SBATCH -e job-%j.%N.err

module load Python-Geo

srun testcalcmulti.py




